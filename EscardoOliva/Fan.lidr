> module Fan

> import Data.Fin
> import Control.Isomorphism
> import Effects
> import Effect.Exception
> import Effect.StdIO

> import Finite.Predicates
> import Sigma.Sigma

> %default total
> %access public export
> %auto_implicits off

Consider extensional equality 

> ExtEq : {A, B : Type} -> (f : A -> B) -> (g : A -> B) -> Type
> ExtEq {A} f g = (a : A) -> f a = g a 

I have not tried to do it, but I think that it should not be very
difficult to show that

> finiteDecidableExtEq : {A, B : Type} -> 
>                        (f : A -> B) -> (g : A -> B) -> 
>                        Finite A -> DecEq B -> Dec (ExtEq f g)

Also, if two function |f, g : (Nat -> A) -> B| are representable by |f'
: (Fin m -> A) -> B| and |g' : (Fin n -> A) -> B| (that is, |f as| and
|g as| depend on a finite number of values of |as|), then it should be
possible to show that, if |A| is finite, |ExtEq f g| is decidable.

Still, it seems to me that the property of being representable 

> head : {A : Type} -> (Nat -> A) -> A
> head as = as Z

> tail : {A : Type} -> (Nat -> A) -> (Nat -> A)
> tail as = as . S

> cons : {A : Type} -> A -> (Nat -> A) -> (Nat -> A)
> cons a as  Z    = a
> cons a as (S m) = as m 

> trim : {A : Type} -> (n : Nat) -> (Nat -> A) -> (Fin n -> A)
> trim n as = as . finToNat 

> SigmaNatFinRepr : Type -> Type -> Type
> SigmaNatFinRepr A B = Sigma Nat (\ n => (Fin n -> A) -> B)

> Represents : {A, B : Type} -> SigmaNatFinRepr A B -> ((Nat -> A) -> B) -> Type
> Represents {A} (MkSigma n f') f = (as : Nat -> A) -> f' (trim n as) = f as

> Representable : {A, B : Type} -> ((Nat -> A) -> B) -> Type
> Representable {A} {B} f = Sigma (SigmaNatFinRepr A B) ( \ nf' => Represents nf' f )

is non-decidable even for |B = Bool|. What makes |fan| in section 5.3 of
the Escardó + Oliva paper "work" is the interplay of two factors:

1) the fact that |least| (and thus |fan|) is partial

> partial
> least : (Nat -> Bool) -> Nat
> least p = if p Z then Z else 1 + least (tail p)

2) the fact that the evaluation of |f a| and |f b| in |fan|

> J : Type -> Type -> Type
> J R X = (X -> R) -> X

> K : Type -> Type -> Type
> K R X = (X -> R) -> R

> findBool : J Bool Bool
> findBool p = p True

> partial
> bigotimes : {X, R : Type} -> (Nat -> J R X) -> ((Nat -> X) -> R) -> (Nat -> X)

< bigotimes  Nil      p = Nil
< bigotimes (e :: es) p = x0 :: bigotimes es (p . (x0 ::)) where
<   x0 = e (\ x => p (x :: bigotimes es (p . (x ::))))

> bigotimes {X} {R} es p = cons x0 (bigotimes {X} {R} es' (p . (cons x0))) where
>   e   : J R X
>   e   = head es
>   es' : Nat -> J R X
>   es' = tail es
>   partial x0  : X 
>   x0  = e (\ x => p (cons x (bigotimes es' (p . (cons x)))))

> repeat : {X : Type} -> X -> (Nat -> X)
> repeat x n = x

> partial
> findCantor : J Bool (Nat -> Bool)
> findCantor = bigotimes (repeat findBool)

> overline : {X, R : Type} -> J R X -> K R X
> overline e = \ p => p (e p)

> partial
> forsome : ((Nat -> Bool) -> Bool) -> Bool
> forsome = overline findCantor

> partial
> forevery : ((Nat -> Bool) -> Bool) -> Bool
> forevery p = not (forsome (not . p))

> agree : {A : Type} -> (Eq A) => Nat -> (Nat -> A) -> (Nat -> A) -> Bool
> agree  Z    as bs = True
> agree (S m) as bs = if as Z == bs Z
>                     then agree m (tail as) (tail bs)
>                     else False

> implies : Bool -> Bool -> Bool
> implies b a = b || (not a)

> partial
> fan : {B : Type} -> (Eq B) => ((Nat -> Bool) -> B) -> Nat
> fan f = least (\ n => forevery (\ as => forevery (\ bs => implies (f as == f bs) (agree n as bs)) ))

is lazy. This is crucial because it effectively reduces representability
to termination

< |fan f| terminates <=> f is represented by a vector-based f'

In this context, the only requirement on |B| which matters is that it is
equality decidable. 

From this angle, I am not sure that the excursion to topology land that
they propose is very helpful. But this might be because I do not know
anything about topology.

What I think that it would be interesting to understand is how these
functions behave in a language like Idris. For instance

> partial
> main : IO ()
> main = 
>   do putStr ("fan (lambda bs => True) "
>              ++
>              show (fan (\ bs => True)) ++ "\n"
>              ++
>              "fan (lambda bs => bs 2) "
>              ++
>              show (fan (\ bs => bs 2)) ++ "\n"
>              )

takes quite some time to execute and I guess that replacing 2 with
something bigger would cause the executable to stall!

Ciao,
Nicola


> {-

> ---}


