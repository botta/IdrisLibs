> module Num.Implementations

> %default total
> %access public export
> %auto_implicits on


> implementation (Num t) => Num (Maybe t) where

>   (+)  Nothing  _       = Nothing
>   (+)  _        Nothing = Nothing
>   (+) (Just x) (Just y) = Just (x + y)

>   (*)  Nothing  _       = Nothing
>   (*)  _        Nothing = Nothing
>   (*) (Just x) (Just y) = Just (x * y)

>   fromInteger = Just . fromInteger
