> module Main

> import Data.Fin
> import Data.Vect
> import Data.Matrix
> import Data.Matrix.Numeric

> import Fun.MathExpr
> import Double.Properties
> import Double.Operations
> import Interfaces.Math

> %default total
> %access public export
> %auto_implicits off


* Preliminaries

This is a re-implementation of

  https://gist.github.com/mrkgnao/a45059869590d59f05100f4120595623 

which, in turn, is an Idris implementation of

  https://blog.jle.im/entry/practical-dependent-types-in-haskell-1.html


* Layers

A layer of type |X| with |m| inputs and |n| outputs is a tuple
consisting of a function of type |X -> X|, a vector of |n| elements of
type |X| (the biases) and a |n| x |m| matrix of elements of type |X|
(the weights):

> Layer : (X : Type) -> (m : Nat) -> (n : Nat) -> Type
> Layer X m n = (MathExpr X, Vect n X, Matrix n m X)

> evalLayer : {X : Type} -> {m, n : Nat} -> 
>             (Num X, Fractional X, Neg X, Math X) => 
>             Layer X m n -> Vect m X -> Vect n X
> evalLayer (s, b, w) x = map (eval s) (b + (w </> x))


* Sequential neural networks

A sequential neural network of type |X| with |m| inputs, |n| outputs and
a sequence of intermediate layers is a sequence of layers of type
|X|:

> infixr 5 :>:

> data Net : (X : Type) -> (m, n, k : Nat) -> Type where
>   Just   : {X : Type} -> {m, n : Nat} -> 
>            Layer X m n -> Net X m n 1
>   (:>:)  : {X : Type} -> {m, m', n, d : Nat} -> 
>            Layer X m m' -> Net X m' n d -> Net X m n (S d)


* Feed-forward

The idea is that, given a sequential neural network of type |X| with |m|
inputs and |n| outputs and a vector of |m| inputs of matching type, one
can compute a vector of |n| outputs. The computation is called
feed-forward.

> ff : {X : Type} -> {m, n, k : Nat} -> 
>      (Num X, Fractional X, Neg X, Math X) => 
>      Net X m n k -> Vect m X -> Vect n X
> ff   (Just l) = evalLayer l
> ff (l :>: ls) = ff ls . (evalLayer l)


* Error

Given an error measure |μ : Vect n X -> Vect n X -> Y|, the error of
a training pair |(x, y) : Vect m X × Vect n X|, is 

> err : {X : Type} -> {m, n, d : Nat} -> 
>       (Num X, Fractional X, Neg X, Math X) => 
>       (Vect n X -> Vect n X -> X) ->
>       Net X m n d -> (Vect m X, Vect n X) -> X
> err μ net (x, y) = μ y (ff net x)

Typical error measures are

> dist : {n : Nat} -> Vect n Double -> Vect n Double -> Double
> dist x y = let d = x - y
>            in  sqrt (d <:> d)

> distso2 : {n : Nat} -> Vect n Double -> Vect n Double -> Double
> distso2 x y = let d = x - y 
>               in  (d <:> d) / 2.0

> -- the gradient of |distso2| with respect to its second argument
> grad_y_distso2 : {n : Nat} -> Vect n Double -> Vect n Double -> Vect n Double
> grad_y_distso2 x y = y - x


* Back propagation

> backPropagationStep : {X : Type} -> {m, n, d : Nat} -> 
>                       (Num X, Fractional X, Neg X, Math X) => 
>                       X ->
>                       (Vect m X, Vect n X) ->
>                       ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>                       Net X m n d -> Net X m n d
> backPropagationStep {X} rate (input, output) grad_y_μ net = fst (go input output net)
> 
>   where go : {m, n, d : Nat} -> 
>              Vect m X -> Vect n X -> Net X m n d -> (Net X m n d, Vect m X)
>   
>         go {m} {n} input output (Just (s, b, w)) =
>           let y0         : Vect m X
>                          = input
>               x1         : Vect n X
>                          = b + (w </> y0)
>               y1         : Vect n X
>                          = map (eval s) x1
>               grad_x_err : Vect n X
>                          = (map (eval (derivative s)) x1) * (grad_y_μ output y1)
>               b'         : Vect n X
>                          = b - (rate <# grad_x_err)
>               w'         : Matrix n m X
>                          = w - (rate <#> (grad_x_err >< input))
>               grad_y_err : Vect m X
>                          = (transpose w) </> grad_x_err
>           in (Just (s, b', w'), grad_y_err)
> 
>         go {m} {n} input output ((s, b, w) :>: ls) =
>           let y0                 = input
>               x1                 = b + (w </> y0)
>               y1                 = map (eval s) x1
>               (ls', grad_y_err') = go y1 output ls
>               grad_x_err         = (map (eval (derivative s)) x1) * grad_y_err'
>               b'                 = b - (rate <# grad_x_err)
>               w'                 = w - (rate <#> (grad_x_err >< y0))
>               grad_y_err         = (transpose w) </> grad_x_err
>           in ((s, b', w') :>: ls', grad_y_err)           

> backPropagation : {X : Type} -> {m, n, d : Nat} -> 
>                   (Num X, Fractional X, Neg X, Math X) => 
>                   X ->
>                   (Vect m X, Vect n X) ->
>                   ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>                   Nat ->
>                   Net X m n d -> Net X m n d
> backPropagation rate (input, output) grad_y_μ Z net = net 
> backPropagation rate (input, output) grad_y_μ (S n) net = 
>   let net' = backPropagationStep rate (input, output) grad_y_μ net
>   in  backPropagation rate (input, output) grad_y_μ n net'

> testBackPropagation  : {X : Type} -> {m, n, d : Nat} -> 
>                        (Num X, Fractional X, Neg X, Math X, Show X) => 
>                        X -> 
>                        (Vect m X, Vect n X) -> 
>                        ({k : Nat} -> Vect k X -> Vect k X -> X) ->
>                        ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>                        Net X m n d -> 
>                        Nat -> 
>                        Nat ->                         
>                        IO ()
>
> testBackPropagation {X} {m} {n} {d} rate io μ grad_y_μ net isteps osteps = 
>   let step   : (Net X m n d -> Net X m n d)
>              = backPropagation rate io grad_y_μ isteps 
>       err    : (Net X m n d -> X)
>              = \ net => err μ net io 
>       states : Stream (Net X m n d)
>              = iterate step net 
>   in  do putStrLn . unlines $ map (show . err) (take (S osteps) states)   


* Training

> trainTrough : {X : Type} -> 
>               {m, n, d, k : Nat} -> 
>               (Num X, Fractional X, Neg X, Math X) => 
>               X ->
>               Vect (S k) (Vect m X, Vect n X) ->
>               ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>               Nat -> 
>               Nat ->
>               Net X m n d -> Net X m n d
> trainTrough rate (io :: Nil) grad_y_μ steps    Z  net = 
>   net
> trainTrough rate (io :: Nil) grad_y_μ steps (S k) net = 
>   let net'  = backPropagation rate io grad_y_μ steps net
>   in  trainTrough rate (io :: Nil) grad_y_μ steps k net'
> trainTrough rate (io :: io' :: ios) grad_y_μ steps    Z  net =
>   net 
> trainTrough rate (io :: io' :: ios) grad_y_μ steps (S k) net = 
>   let net'  = backPropagation rate io grad_y_μ steps net
>       net'' = trainTrough rate (io' :: ios) grad_y_μ steps (S k) net'
>   in  trainTrough rate (io :: io' :: ios) grad_y_μ steps k net''

> testTrainTrough  : {X : Type} -> 
>                    {m, n, d, k : Nat} -> 
>                    (Num X, Fractional X, Neg X, Math X, Show X) => 
>                    X -> 
>                    Vect (S k) (Vect m X, Vect n X) -> 
>                    ({k : Nat} -> Vect k X -> Vect k X -> X) ->
>                    ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>                    Net X m n d -> 
>                    Nat -> 
>                    Nat ->
>                    Nat ->
>                    IO ()
>
> testTrainTrough {X} {m} {n} {d} {k} rate ios μ grad_y_μ net isteps steps osteps = 
>   let step   : (Net X m n d -> Net X m n d)
>              = trainTrough rate ios grad_y_μ isteps steps in
>   let errors : (Net X m n d -> (X, Vect (S k) X))
>              = \ net => let es = map (err μ net) ios 
>                         in  (es <:> es, es) in
>   let states : Stream (Net X m n d)
>              = iterate step net in
>   do  putStrLn . unlines $ map (show . errors) (take (S osteps) states)   

> select : {X : Type} -> {m, n, d, k : Nat} -> 
>          (Num X, Fractional X, Neg X, Math X, Ord X) => 
>          (Vect n X -> Vect n X -> X) ->
>          Net X m n d -> Vect (S k) (Vect m X, Vect n X) -> (Vect m X, Vect n X)
> select μ net (xy :: Nil) = xy
> select μ net (xy :: xy' :: xys) = 
>   let xy'' = select μ net (xy' :: xys)
>   in  if   err μ net xy > err μ net xy''
>       then xy
>       else xy''

> trainSelect : {X : Type} -> 
>               {m, n, d, k : Nat} -> 
>               (Num X, Fractional X, Neg X, Math X, Ord X) => 
>               X ->
>               Vect (S k) (Vect m X, Vect n X) ->
>               ({k : Nat} -> Vect k X -> Vect k X -> X) ->
>               ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>               Nat -> 
>               Nat ->
>               Net X m n d -> Net X m n d
> trainSelect rate ios μ grad_y_μ steps    Z  net = 
>   net
> trainSelect rate ios μ grad_y_μ steps (S k) net = 
>   let net'  = backPropagation rate (select μ net ios) grad_y_μ steps net
>   in  trainTrough rate ios grad_y_μ steps k net'

> testTrainSelect  : {X : Type} -> 
>                    {m, n, d, k : Nat} -> 
>                    (Num X, Fractional X, Neg X, Math X, Ord X, Show X) => 
>                    X -> 
>                    Vect (S k) (Vect m X, Vect n X) -> 
>                    ({k : Nat} -> Vect k X -> Vect k X -> X) ->
>                    ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>                    Net X m n d -> 
>                    Nat -> 
>                    Nat ->
>                    Nat ->
>                    IO ()
>
> testTrainSelect {X} {m} {n} {d} {k} rate ios μ grad_y_μ net isteps steps osteps = 
>   let step   : (Net X m n d -> Net X m n d)
>              = trainSelect rate ios μ grad_y_μ isteps steps in
>   let errors : (Net X m n d -> (X, Vect (S k) X))
>              = \ net => let es = map (err μ net) ios 
>                         in  (es <:> es, es) in
>   let states : Stream (Net X m n d)
>              = iterate step net in
>   do  putStrLn . unlines $ map (show . errors) (take (S osteps) states)   


> backPropagationVectStep : {X : Type} -> 
>                           {m, n, d, k : Nat} -> 
>                           (Num X, Fractional X, Neg X, Math X) => 
>                           X ->
>                           Vect (S k) (Vect m X, Vect n X) ->
>                           ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>                           Net X m n d -> Net X m n d 
> backPropagationVectStep {X} rate ios grad_y_μ net = fst (go ios net)
> 
>   where go : {m, n, d, k : Nat} -> 
>              Vect (S k) (Vect m X, Vect n X) ->
>              Net X m n d -> (Net X m n d, Vect m X)
>   
>         go {m} {n} {k} ios (Just (s, b, w)) =
>           let inputs      : Vect (S k) (Vect m X)
>                           = map fst ios
>               outputs     : Vect (S k) (Vect n X)
>                           = map snd ios
>               y0s         : Vect (S k) (Vect m X)
>                           = inputs
>               x1s         : Vect (S k) (Vect n X)
>                           = map (\ ξ => b + (w </> ξ)) y0s
>               y1s         : Vect (S k) (Vect n X)
>                           = map (map (eval s)) x1s
>               grad_x_errs : Vect (S k) (Vect n X)
>                           = let x1y1s : Vect (S k) (Vect n X, Vect n X, Vect n X)
>                                       = zip3 x1s y1s outputs
>                             in  map (\ (x1, y1, output) => 
>                                        (map (eval (derivative s)) x1) * (grad_y_μ output y1)) x1y1s
>               grad_x_err  : Vect n X
>                           = sum grad_x_errs
>               lala        : Vect (S k) (Matrix n m X)
>                           = zipWith (><) grad_x_errs inputs  
>               b'          : Vect n X
>                           = b - (rate <# grad_x_err)
>               w'          : Matrix n m X
>                           = w - (rate <#> (sum lala))
>               grad_y_err  : Vect m X
>                           = (transpose w) </> grad_x_err
>           in (Just (s, b', w'), grad_y_err)
> 
>         go {m} {n} {k} ios ((s, b, w) :>: ls) =
>           let inputs             = map (\ (i,o) => i) ios
>               outputs            = map (\ (i,o) => o) ios
>               y0s                = inputs
>               x1s                = map (\ xi => b + (w </> xi)) y0s
>               y1s                = map (map (eval s)) x1s
>               (ls', grad_y_err') = go (zip y1s outputs) ls
>               grad_x_errs        = map (\ xi => (map (eval (derivative s)) xi) * grad_y_err') x1s 
>               grad_x_err         = sum grad_x_errs
>               lala               = zipWith (><) grad_x_errs inputs  
>               b'                 = b - (rate <# grad_x_err)
>               w'                 = w - (rate <#> (sum lala))
>               grad_y_err         = (transpose w) </> grad_x_err
>           in  ((s, b', w') :>: ls', grad_y_err)           

> backPropagationVect : {X : Type} -> 
>                       {m, n, d, k : Nat} -> 
>                       (Num X, Fractional X, Neg X, Math X) => 
>                       X ->
>                       Vect (S k) (Vect m X, Vect n X) ->
>                       ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>                       Nat ->
>                       Net X m n d -> Net X m n d 
> backPropagationVect rate ios grad_y_μ    Z  net = net 
> backPropagationVect rate ios grad_y_μ (S n) net = 
>   let net' = backPropagationVectStep rate ios grad_y_μ net
>   in  backPropagationVect rate ios grad_y_μ n net'

> testBackPropagationVect  : {X : Type} -> 
>                            {m, n, d, k : Nat} -> 
>                            (Num X, Fractional X, Neg X, Math X, Show X) => 
>                            X -> 
>                            Vect (S k) (Vect m X, Vect n X) ->
>                            ({k : Nat} -> Vect k X -> Vect k X -> X) ->
>                            ({k : Nat} -> Vect k X -> Vect k X -> Vect k X) ->
>                            Net X m n d -> 
>                            Nat -> 
>                            Nat ->                         
>                            IO ()
>
> testBackPropagationVect {X} {m} {n} {d} {k} rate ios μ grad_y_μ net isteps osteps = 
>   let step   : (Net X m n d -> Net X m n d)
>              = backPropagationVect rate ios grad_y_μ isteps
>       errors : (Net X m n d -> (X, Vect (S k) X))
>              = \ net => let es = map (err μ net) ios 
>                         in  (es <:> es, es)
>       states : Stream (Net X m n d)
>              = iterate step net
>   in  do putStrLn . unlines $ map (show . errors) (take (S osteps) states)   


* Examples

> s1 : MathExpr Double
> s1 = (Const 1) / (Const 1 + Exp . Neg)

> i1 : Vect 2 Double
> i1 = [0.05, 0.10]

> o1 : Vect 2 Double
> o1 = [0.01, 0.99]

> net1 : Net Double 2 2 1 
> net1 = (Just (s1, [0.60, 0.60], [[0.40, 0.45], [0.50, 0.55]])
>        ) 

> net2 : Net Double 2 2 2 
> net2 = (s1, [0.35, 0.35], [[0.15, 0.20], [0.25, 0.30]])
>        :>:
>        Just (s1, [0.60, 0.60], [[0.40, 0.45], [0.50, 0.55]])         

> ios : (n : Nat) -> Vect (length (enumFromTo 0 (S n))) (Vect 1 Double, Vect 1 Double)
> ios n = [([x], [0.5 * (1.0 + Prelude.Doubles.sin (2.0 * pi * x))]) | x <- xs] where
>   xs : Vect (length (enumFromTo 0 (S n))) Double
>   xs = map (\ x => x / (fromNat (S n))) (map fromNat (fromList [0..(S n)]))

> net3 : (nml : Nat) -> (sml : Nat) -> Net Double 1 1 (nml + 2)
>
> net3 Z _ = (s1, [0.5], [[0.5]]) :>: Just (s1, [0.5], [[0.5]])
>  
> net3 (S n) size = (ls :>: net' n) where
>   ls : Layer Double 1 size
>   ls = (s1, replicate size 0.3, replicate size [0.7])
>   l  : Layer Double size size
>   l  = (s1, replicate size 0.1, replicate size (replicate size 0.25))
>   le : Layer Double size 1 
>   le = (s1, [0.2], [replicate size 0.4])
>   net' : (n : Nat) -> Net Double size 1 (n + 2)
>   net'    Z  = l :>: Just le
>   net' (S m) = l :>: net' m  

> {-

> ---}






 
