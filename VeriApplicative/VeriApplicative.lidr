> module VeriApplicative.VeriApplicative

> import Fun.Predicates
> import VeriFunctor.VeriFunctor

> %default total
> %access public export
> %auto_implicits off


> infixl 3 <*>


> interface (VeriFunctor F, Applicative F) => VeriApplicative (F : Type -> Type) where
>
>   applicativeMap : {A, B : Type} -> (f : A -> B) -> ExtEq {A = F A} (map f) ((pure f) <*>)
>
>   applicativeId : {A : Type} -> ExtEq {A = F A} ((pure id) <*>) id
>
>   applicativeComposition : {A, B, C : Type} -> (Ff : F (A -> B)) -> (Fg : F (B -> C)) ->
>                            ExtEq (((pure (.) <*> Fg) <*> Ff) <*>) ((Fg <*>) . (Ff <*>))
>
>   applicativeHomomorphism : {A, B : Type} -> (f : A -> B) -> 
>                             ExtEq {B = F B} (((<*>) (pure f)) . pure) (pure . f)
>
>   applicativeInterchange : {A, B : Type} -> (Ff : F (A -> B)) -> 
>                            ExtEq ((Ff <*>) . pure) (\ a => pure (\f : (A -> B) => f a) <*> Ff)

> {- 

> ---}

