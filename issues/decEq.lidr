
> interface DecEq t1 t2 where                                                                                                   
>   total decEq : (x1 : t1) -> (x2 : t2) -> Dec (x1 = x2)
