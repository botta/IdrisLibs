> %default total
> %access public export
> %auto_implicits off

> X : Type
> Y : X -> Type
> P : Type
> P = (x : X) -> Y x

> test1 : (f : P) -> (g : P) -> (f = g) -> (x : X) -> f x = g x
> test1 f g prf x = replace {P = \ h : P => f x = h x} prf Refl

> test2 : {f, g : P} -> (f = g) -> (x : X) -> f x = g x
> test2 Refl x = Refl

> data Id : (A : Type) -> (a1 : A) -> (a2 : A) -> Type where
>   R : {A : Type} -> (a : A) -> Id A a a

> leibniz : {A : Type} -> {P : A -> Type} -> (a1 : A) -> (a2 : A) -> Id A a1 a2 -> P a1 -> P a2

> leibniz a a (R a) prf = prf


> {-

> functionality : {A : Type} -> {B : A -> Type} -> 
>                 (f : (a : A) -> B a) -> (g : (a : A) -> B a) -> 
>                 f = g -> (a : A) -> f a = g a
> functionality f g prf x = Refl

> test3 : (f : P) -> (g : P) -> (f = g) -> (x : X) -> f x = g x
> test3 f g prf x = functionality f g prf x 

> ---}

