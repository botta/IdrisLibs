> import Data.Fin

> import Fin.Operations

> %default total
> %access public export
> %auto_implicits on

> fin0Lemma : (i : Fin Z) -> (j : Fin Z) -> i = j

