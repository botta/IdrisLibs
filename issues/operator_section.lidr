> foo : DPair Nat (\ n => n `LTE` 5)

> -- bar : DPair Nat (`LTE` 5)

> xs : List Nat
> xs = [1,2,3]

> ys : List Nat
> ys = map (plus 1) xs
