> module DirectedAcyclicGraphs.BayesNetwork

> import Data.Fin
> import Data.Vect
> import Control.Isomorphism

> import Basic.Predicates
> import Basic.Properties
> import Fin.Operations
> import HList.HList
> import HList.Operations
> import HVect.HVect
> import HVect.Operations
> import Finite.Predicates
> import Finite.Properties
> import Sigma.Sigma
> import Bool.Properties
> import Unit.Properties
> import DirectedAcyclicGraphs.DAG
> import Real.Postulates

> %default total
> %access public export
> %auto_implicits off


We first try to build a full-fledged Bayes network. Each node gets a
type, a value (of that type) and a conditional probability table:

< mutual

<   ||| An mDAG with values and cond. prob. tables in the nodes
<   data BayesNetwork : Nat -> Type where
<     Nil   : BayesNetwork Z
<     Node  : {n : Nat} -> {np : Nat} -> {auto prf : LTE np n} ->
<             (g   : BayesNetwork n) -> 
<             (t   : Type) ->
<             (v   : t) ->
<             (ps  : Vect np (Fin n)) ->
<             (cpt : HVect (map (type g) ps) -> t -> Double) ->
<             BayesNetwork (S n)

<   ||| The type of a node
<   type : {n : Nat} -> BayesNetwork n -> Fin n -> Type
<   type {n = Z}    Nil             k impossible 
<   type {n = S m} (Node g t _ _ _) k with (decEq k last)  
<     | (Yes _) = t
<     | (No  c) = type g (strengthen k {contra = c})

We are specifically interested in networks that fulfill a few additional
requirements:

0) The underlying graph is directed and acyclic.

1) The types associated with the nodes are finite and non empty,

2) There is only one edge between a node and each of its parents.

The first condition is enforced by construction: in a network with |n|
nodes, each node has a unique index and the parents of a node (|ps|)
have indexes which are smaller than the index of that node. We can
explicitely require |t| to be finite and non-empty:


< mutual

<   ||| An mDAG with values and cond. prob. tables in the nodes
<   data BayesNetwork : Nat -> Type where
<     Nil   : BayesNetwork Z
<     Node  : {n : Nat} -> {np : Nat} -> {auto prf : LTE np n} ->
<             (g   : BayesNetwork n) -> 
<             (t   : Type) -> 
<             (ft  : Finite t) ->
<             (net : Not (Empty t)) ->
<             (v   : t) ->
<             (ps  : Vect np (Fin n)) ->
<             (cpt : HVect (map (type g) ps) -> t -> Double) ->
<             BayesNetwork (S n)

<   ||| The type of a node
<   type : {n : Nat} -> BayesNetwork n -> Fin n -> Type
<   type {n = Z}    Nil                 k impossible 
<   type {n = S m} (Node g t _ _ _ _ _) k with (decEq k last)  
<     | (Yes _) = t
<     | (No  c) = type g (strengthen k {contra = c})

We can fulfill 2) by requiring |ps| to have no duplicates or, as
originally suggested by Cezar, by computing the list of the parents of a
node as the inverse image of a function of type |Fin n -> Bool|.

Before we do so, it is useful to first introduce some domain-specific
notions. In Bayesian probability, we have to do with probabilities of
variables or events.

In many applications, variables represent propositions that can be
either |True| or |False|. For example "it rains" or "the grass is wet".

More generally, a Bayesian variable is a finite, not empty type:

> Var : Type
> Var = Sigma Type (\ t => (Finite t, Not (Empty t)))

We will make extensive use of a function that exracts the type
associated with a Bayesian variable:

> typeOf : Var -> Type
> typeOf (MkSigma t (ft, net)) = t

Also, it is useful to explicitly introduce the type of a list of values,
one for every element of an associated list of of variables:

> ListVal : List Var -> Type
> ListVal vars = HList (map typeOf vars) 

Thus, for instance, if we represent "it rains" and "the grass is wet"
with two Boolean variables

> finiteNotEmptyBool : (Finite Bool, Not (Empty Bool))
> finiteNotEmptyBool = (finiteBool, notEmptyBool)

> vars : List Var
> vars = [(MkSigma Bool finiteNotEmptyBool), (MkSigma Bool finiteNotEmptyBool)]

the possible values for |vars| could be

> rw : ListVal vars
> rw = [False, True]

In Bayesian probability, we are mainly concerned with the probability of
values of type |ListVal vars| for a given list of variables |vars: List
Var|. Typically, we want to infer the values of a joint probability
function |jpf: Prob vars|

> Prob : List Var -> Type
> Prob vars = ListVal vars -> Real

from certain given data and under a number of assumption. Is is useful
to distinguish between probability theory axioms

> IsProbabilityAxiom1 : (vars : List Var) -> Prob vars -> Type
> -- IsProbabilityAxiom1 vars jpf = (x : ListVal vars) -> (LTE 0.0 (jpf x), LTE (jpf x) 1.0)  



> mutual

>   ||| An DAG with values and cond. prob. tables in the nodes
>   data BayesNetwork : Nat -> Type where
>     Nil   : BayesNetwork Z
>     Node  : {n : Nat} -> {np : Nat} -> {auto prf : LTE np n} ->
>             (g   : BayesNetwork n) -> 
>             (t   : Type) -> 
>             (ft  : Finite t) ->
>             (net : Not (Empty t)) ->
>             (v   : t) ->
>             (ps  : Fin n -> Bool) ->
>             (cpt : HList (map (type g) (invImage ps True)) -> t -> Double) ->
>             BayesNetwork (S n)

>   ||| The type of a node
>   type : {n : Nat} -> BayesNetwork n -> Fin n -> Type
>   type {n = Z}    Nil                 k impossible 
>   type {n = S m} (Node g t _ _ _ _ _) k with (decEq k last)  
>     | (Yes _) = t
>     | (No  c) = type g (strengthen k {contra = c})

One would actually like to evaluate a network for different values of
the nodes and perhaps also for different probability tables. Thus, the
above implementation of Bayes networks is perhaps too heavyweight.

On the other hand, for full-fledged networks we can implement something
that resembles the chain rule easily: 

> ||| The value of a node
> val : {n : Nat} -> (g : BayesNetwork n) -> (k : Fin n) -> type g k
> val {n = Z}    Nil                 k impossible 
> val {n = S m} (Node g t _ _ v _ _) k with (decEq k last)  
>   | (Yes _) = v
>   | (No  c) = val g (strengthen k {contra = c})

> ||| The values of a list of nodes
> vals : {n : Nat} -> 
>        (g : BayesNetwork n) -> (ks : List (Fin n)) -> HList (map (type g) ks)
> vals g  Nil      = Nil 
> vals g (k :: ks) = (val g k) :: (vals g ks) 

> ||| Is this an implementation of the chain rule? 
> chain : {n : Nat} -> (g : BayesNetwork n) -> Double
> chain  Nil                    = 1
> chain (Node g _ _ _ v ps cpt) = (cpt (vals g (invImage ps True)) v) * (chain g)

By contrast, attempts to disentangle network and values have so far
ended up quite badly, see the commented code below.


Example:

    0    1
     \  /|
      \/ |
      2  |
     /  \|
    3    4

> testBayesNetwork0 : BayesNetwork 0
> testBayesNetwork0 = Nil

> testBayesNetwork1 : BayesNetwork 1
> testBayesNetwork1 = Node testBayesNetwork0 t ft net v ps cpt where
>   t   : Type
>   t   = Bool
>   ft  : Finite t
>   ft  = finiteBool
>   net : Not (Empty t)
>   net = notEmptyBool
>   v   : t
>   v   = False
>   ps  : Fin 0 -> Bool
>   ps  = \ k => absurd k
>   cpt : HList Nil -> Bool -> Double
>   cpt _ False = 0.3
>   cpt _ True  = 0.7

> testBayesNetwork2 : BayesNetwork 2
> testBayesNetwork2 = Node testBayesNetwork1 t ft net v ps cpt where
>   t   : Type
>   t   = Bool
>   ft  : Finite t
>   ft  = finiteBool
>   net : Not (Empty t)
>   net = notEmptyBool
>   v   : t
>   v   = True
>   ps  : Fin 1 -> Bool
>   ps  = \ k => False
>   cpt : HList Nil -> Bool -> Double
>   cpt _ False = 0.8
>   cpt _ True  = 0.2

> testBayesNetwork3 : BayesNetwork 3
> testBayesNetwork3 = Node testBayesNetwork2 t ft net v ps cpt where
>   t   : Type
>   t   = Fin 42
>   ft  : Finite t
>   ft  = finiteFin
>   net : Not (Empty t)
>   net = notEmptyFin
>   v   : t
>   v   = FS FZ
>   ps  : Fin 2 -> Bool
>   ps  = \ k => True
>   cpt : HList [Bool, Bool] -> t -> Double
>   cpt [False, False] k = 0.9
>   cpt [False,  True] k = 0.05
>   cpt [ True, False] k = 0.0
>   cpt [ True,  True] k = 0.0

> testBayesNetwork4 : BayesNetwork 4
> testBayesNetwork4 = Node testBayesNetwork3 t ft net v ps cpt where
>   t    : Type
>   t    = (Bool, Bool)
>   ft   : Finite t
>   ft   = finitePair finiteBool finiteBool
>   net  : Not (Empty t)
>   net  = notEmptyPair notEmptyBool notEmptyBool
>   v    : t
>   v    = (True, False)
>   ps  : Fin 3 -> Bool
>   ps  = \ k => k == 2
>   cpt : HList [Fin 42] -> t -> Double
>   cpt [n] (False, False) = 0.0
>   cpt [n] (False,  True) = 1.0
>   cpt [n] ( True, False) = 0.0
>   cpt [n] ( True,  True) = 1.0

> testBayesNetwork5 : BayesNetwork 5
> testBayesNetwork5 = Node testBayesNetwork4 t ft net v ps cpt where
>   t    : Type
>   t    = Unit
>   ft   : Finite t
>   ft   = finiteUnit
>   net  : Not (Empty t)
>   net  = notEmptyUnit
>   v    : t
>   v    = ()
>   ps  : Fin 4 -> Bool
>   ps  = \ k => k == 1 || k == 2
>   cpt : HList [Bool, Fin 42] -> t -> Double
>   cpt [False, k] () = 0.3
>   cpt [ True, k] () = 0.3

> testType : Type
> testType = type testBayesNetwork5 4

> -- testVal : testType
> -- testVal = val testBayesNetwork5 4

> testChain : Double
> testChain = chain testBayesNetwork5 

This seems to work fine but for some reasons computing |testVal| makes
the type checker go nuts!

If |chain| turns out to be useful (and perhaps we manage to implement
other useful computations on |BayesNetwork|s), we can perhaps think of
building such networks on the basis of simpler data types and for
specific computational tasks. For instance, we could start from a plain
DAG

< ||| A DAG
< data DAG : Nat -> Type where
<   Nil   :  DAG Z
<   Node  :  {n : Nat} -> 
<            (g   : DAG n) -> 
<            (ps  : Fin n -> Bool) ->
<            DAG (S n)

and try to figure out what we would need to implement 

> ||| DAG to BayesNetwork
> mkBayesNetwork : {n : Nat} -> (g : DAG n) -> BayesNetwork n

For the trivial case we do not need any additional data

< mkBayesNetwork {n = Z}    Nil = Nil

For the non-trivial case we first of all need a function that provides
the types associated with the nodes of the network and their proofs of
finiteness and non-emptyness:

> ntype : {n : Nat} -> Fin n -> Type

> fntype : {n : Nat} -> (k : Fin n) -> Finite (ntype k)

> nentype : {n : Nat} -> (k : Fin n) -> Not (Empty (ntype k))

< mkBayesNetwork {n = S m} (Node dag ps) = Node g t ft net ?v ps ?cpt where
<   g   : BayesNetwork m
<   g   = mkBayesNetwork dag
<   t   : Type
<   t   = ntype {n = S m} last
<   ft  : Finite t
<   ft  = fntype {n = S m} last
<   net : Not (Empty t)
<   net = nentype {n = S m} last

Further, we need a function that provides the node values

> nval : {n : Nat} -> (k : Fin n) -> ntype k

< mkBayesNetwork {n = S m} (Node dag ps) = Node g t ft net v ps ?cpt where
<   g   : BayesNetwork m
<   g   = mkBayesNetwork dag
<   t   : Type
<   t   = ntype {n = S m} last
<   ft  : Finite t
<   ft  = fntype {n = S m} last
<   net : Not (Empty t)
<   net = nentype {n = S m} last
<   v   : t
<   v   = nval {n = S m} last

Finally, we need a function that gives us the conditional probability
tables for the nodes.

> ncpt : {n   : Nat} -> 
>        (g   : BayesNetwork n) ->  
>        (ps  : Fin n -> Bool) ->
>        HList (map (type g) (invImage ps True)) -> (ntype {n = S n} last) -> Double

With globals |ntype|, |nval| and |ncpt| in place, we can complete the
implementation of |mkBayesNetwork|:

> mkBayesNetwork {n = Z}    Nil = Nil

> mkBayesNetwork {n = S m} (Node dag ps) = Node g t ft net v ps cpt where
>   g   : BayesNetwork m
>   g   = mkBayesNetwork dag
>   t   : Type
>   t   = ntype {n = S m} last
>   ft  : Finite t
>   ft  = fntype {n = S m} last
>   net : Not (Empty t)
>   net = nentype {n = S m} last
>   v   : t
>   v   = nval {n = S m} last
>   cpt : HList (map (type g) (invImage ps True)) -> t -> Double
>   cpt = ncpt g ps

We can also implement a transformation that does not rely on global
variables

> ||| DAG to BayesNetwork
> mkBayesNetwork' : {n       : Nat} -> 
>                   (g       : DAG n) -> 
>                   (ntype   : (n : Nat) -> Fin n -> Type) ->
>                   (fntype  : (n : Nat) -> (k : Fin n) -> Finite (ntype n k)) ->
>                   (nentype : (n : Nat) -> (k : Fin n) -> Not (Empty (ntype n k))) ->
>                   (nval    : (n : Nat) -> (k : Fin n) -> ntype n k) ->
>                   (ncpt    : (n : Nat) -> 
>                              (g : BayesNetwork n) -> 
>                              (ps : Fin n -> Bool) -> 
>                              HList (map (type g) (invImage ps True)) -> 
>                              (ntype (S n) last) -> 
>                              Double) ->
>                   BayesNetwork n

> mkBayesNetwork' {n = Z}    Nil       _ _ _ _ _ = Nil

> mkBayesNetwork' {n = S m} (Node dag ps) ntype fntype nentype nval ncpt
>   = 
>   Node g t ft net v ps cpt where
>     g   : BayesNetwork m
>     g   = mkBayesNetwork' dag ntype fntype nentype nval ncpt
>     t   : Type
>     t   = ntype (S m) last
>     ft  : Finite t
>     ft  = fntype (S m) last
>     net : Not (Empty t)
>     net = nentype (S m) last  
>     v   : t
>     v   = nval (S m) last
>     cpt : HList (map (type g) (invImage ps True)) -> t -> Double
>     cpt = ncpt m g ps

but perhaps this is not so useful and we should first implement a fold
for DAGs with data instead.

> {-

...

Previous attempts:

> ||| The number of parents of a node
> np : {n : Nat} -> DAG n -> Fin n -> Nat
> np {n = Z}    Null                     k impossible 
> np {n = S m} (Node {np = np'} g _) k with (decEq k last)  
>   | (Yes _) = np'
>   | (No  c) = np g (strengthen k {contra = c})


> ||| The parents of a node
> parents : {n : Nat} -> (g : DAG (S n)) -> (k : Fin (S n)) -> Vect (np g k) (Fin n)
> parents {n = Z}    Null             k impossible
> parents {n = Z}   (Node g Nil) FZ = Nil
> parents {n = Z}   (Node g  ps) (FS k) impossible
> parents {n = S m} (Node g  ps) k with (decEq k last) 
>   | (Yes _) = ps
>   | (No  c) = map weaken (parents g (strengthen k {contra = c}))

> ||| The value of a vector of nodes
> vals : {n : Nat} ->
>        (g : DAG (S n)) ->
>        (type : Fin (S n) -> Type) ->
>        (val  : (k : Fin (S n)) -> type k) ->
>        (k : Fin (S n)) -> 
>        HVect (map (type . weaken) (parents g k))

> chain : {n : Nat} -> 
>         (g : DAG (S n)) -> 
>         (type : Fin (S n) -> Type) ->
>         (cpt  : (k : Fin (S n)) -> HVect (map (type . weaken) (parents g k)) -> type k -> Double) ->
>         (val  : (k : Fin (S n)) -> type k) ->
>         Double
> chain  Null          _   _   _  impossible
> chain {n = S m} (Node {prf = prf'} g ps) type cpt val = 
>   (cpt last (vals (Node {prf = prf'} g ps) type val last) (val last))
>   *
>   (chain g type' cpt' val') where
>     type'  :  (k : Fin (S m)) -> Type
>     type'  =  type . FS
>     cpt'   :  (k : Fin (S m)) -> HVect (map (type' . weaken) (parents g k)) -> type' k -> Double
>     cpt' k =  cpt (FS k)
>     val'   :  (k : Fin (S m)) -> type' k
>     val' k =  val (FS k)

> ---}

...


> {-

> mutual

>   ||| An mDAG with typed nodes
>   data BayesNetwork : Nat -> Type where
>     Nil   : BayesNetwork Z
>     Node  : {n : Nat} -> {np : Nat} -> {auto prf : LTE np n} ->
>             (g   : BayesNetwork n) -> 
>             (t   : Type) ->
>             (ps  : Vect np (Fin n)) ->
>             (tab : Tuple (map (type g) ps) -> t -> Double) ->
>             BayesNetwork (S n)

>   ||| The type of a node
>   type : {n : Nat} -> BayesNetwork n -> Fin n -> Type
>   type {n = Z}    Nil                    k impossible 
>   type {n = S m} (Node g t _ _) k with (decEq k last)  
>     | (Yes _) = t
>     | (No  c) = type g (strengthen k {contra = c})


    0    1
     \  /|
      \/ |
      2  |
     /  \|
    3    4

> testBayesNetwork0 : BayesNetwork 0
> testBayesNetwork0 = Nil

> testBayesNetwork1 : BayesNetwork 1
> testBayesNetwork1 = Node testBayesNetwork0 Bool Nil tab1 where
>   tab1 : Tuple Nil -> Bool -> Double
>   tab1 _ False = 0.3
>   tab1 _ True  = 0.7

> testBayesNetwork2 : BayesNetwork 2
> testBayesNetwork2 = Node testBayesNetwork1 Bool Nil tab2 where
>   tab2 : Tuple Nil -> Bool -> Double
>   tab2 _ False = 0.8
>   tab2 _ True  = 0.2

> testBayesNetwork3 : BayesNetwork 3
> testBayesNetwork3 = Node {np = 2} testBayesNetwork2 Nat [0, 1] tab3 where
>   tab3 : Tuple [Bool, Bool] -> Nat -> Double
>   tab3 (TupleCons False (TupleCons False TupleNil)) n = 0.9
>   tab3 (TupleCons False (TupleCons  True TupleNil)) n = 0.05
>   tab3 (TupleCons  True (TupleCons False TupleNil)) n = 0.0
>   tab3 (TupleCons  True (TupleCons  True TupleNil)) n = 0.0

> testBayesNetwork4 : BayesNetwork 4
> testBayesNetwork4 = Node {np = 1} testBayesNetwork3 (Bool, Bool) [2] tab4 where
>   tab4 : Tuple [Nat] -> (Bool, Bool) -> Double
>   tab4 (TupleCons n TupleNil) (False, False) = 0.0
>   tab4 (TupleCons n TupleNil) (False,  True) = 1.0
>   tab4 (TupleCons n TupleNil) ( True, False) = 0.0
>   tab4 (TupleCons n TupleNil) ( True,  True) = 1.0

> testBayesNetwork5 : BayesNetwork 5
> testBayesNetwork5 = Node {np = 2} testBayesNetwork4 Unit [1, 2] tab5 where
>   tab5 : Tuple [Bool, Nat] -> Unit -> Double
>   tab5 (TupleCons False (TupleCons n TupleNil)) () = 0.3
>   tab5 (TupleCons  True (TupleCons n TupleNil)) () = 0.3

> testtype : Type
> testtype = type testBayesNetwork5 4


> ||| The number of parents of a node
> np : {n : Nat} -> BayesNetwork n -> Fin n -> Nat
> np {n = Z}    Nil                      k impossible 
> np {n = S m} (Node {np = np'} g _ _ _) k with (decEq k last)  
>   | (Yes _) = np'
>   | (No  c) = np g (strengthen k {contra = c})

> testnp : Nat
> testnp = np testBayesNetwork5 4


> ||| The parents of a node
> parents : {n : Nat} -> (g : BayesNetwork (S n)) -> (k : Fin (S n)) -> Vect (np g k) (Fin n)
> parents {n = Z}    Nil           k impossible
> parents {n = Z}   (Node g _ Nil _) FZ = Nil
> parents {n = Z}   (Node g _  ps _) (FS k) impossible
> parents {n = S m} (Node g _  ps _) k with (decEq k last) 
>   | (Yes _) = ps
>   | (No  c) = map weaken (parents g (strengthen k {contra = c}))

> testparents : Vect testnp (Fin 4)
> testparents = parents testBayesNetwork5 4 


> ||| The types of the nodes
> types : {n : Nat} -> (g : BayesNetwork n) -> Vect n Type
> types g = map (type g) (toVect id)


> ||| The types of the parents of a node
> parentTypes : {n : Nat} -> (g : BayesNetwork (S n)) -> (k : Fin (S n)) -> Vect (np g k) Type
> parentTypes g k = map ((type g) . weaken) (parents g k)


...


> typeLemma : {n : Nat} -> {np : Nat} -> {prf : LTE np n} ->
>             (g   : BayesNetwork n) -> 
>             (t   : Type) ->
>             (ps  : Vect np (Fin n)) ->
>             (tab : Tuple (map (type g) ps) -> t -> Double) ->
>             type (Node {n} {np} {prf} g t ps tab) last = t

> lastVal : {n : Nat} -> {np : Nat} -> {prf : LTE np n} ->
>           (g   : BayesNetwork n) -> 
>           (t   : Type) ->
>           (ps  : Vect np (Fin n)) ->
>           (tab : Tuple (map (type g) ps) -> t -> Double) ->
>           (val  :  (k : Fin (S n)) -> type g k) -> 
>           t
> lastVal g          

...

> eval : {n    :  Nat} -> 
>        (g    :  BayesNetwork (S n)) ->
>        (val  :  (k : Fin (S n)) -> type g k) -> 
>        Double
> eval {n = Z}    Nil                _  impossible
> eval {n = Z}   (Node g _ Nil tab) val = tab TupleNil (val FZ)
> eval {n = S m} (Node {n = S m} {np = Z} {prf = prf} g t Nil tab) val = tab TupleNil s2 where
>   s0 : type (Node {n = S m} {np = Z} {prf = prf} g t Nil tab) last
>   s0 = val last
>   s1 : type (Node {n = S m} {np = Z} {prf = prf} g t Nil tab) last = t
>   s1 = typeLemma g t Nil tab
>   s2 : t
>   s2 = replace {P = id} s1 s0
> eval {n = S m} (Node {n = S m} {np = S np'} {prf = prf} g t  (p :: ps) tab) val = tab s3 s2 where
>   s0 : type (Node {n = S m} {np = S np'} {prf = prf} g t (p :: ps) tab) last
>   s0 = val last
>   s1 : type (Node {n = S m} {np = S np'} {prf = prf} g t (p :: ps) tab) last = t
>   s1 = typeLemma g t (p :: ps) tab
>   s2 : t
>   s2 = replace {P = id} s1 s0
>   s3 : Tuple (map (type g) (p :: ps))
>   s3 = TupleCons ?lala ?kika

> {-

> ||| The chain rule? 
> chain : {n    :  Nat} -> 
>         (g    :  BayesNetwork (S n)) ->
>         (val  :  (k : Fin (S n)) -> type g k) -> 
>         Double
> chain {n = Z}    Nil                _  impossible
> chain {n = Z}   (Node g _ Nil tab) val = tab TupleNil (val FZ)
> chain {n = S m} (Node g _  ps tab) val = (tab ?lala (val last)) * (chain g val') where
>   val' :  (k : Fin (S m)) -> type g k
>   val' k = ?kika -- val (weaken k)

> -}

> ---}

> {-

> Empty : Type -> Type
> Empty A = A -> Void


> ||| An mDAG with typed nodes
> data BayesNetwork : Nat -> Type where
>   Nil   : BayesNetwork Z
>   Node  : {n : Nat} -> {np : Nat} -> {auto prf : LTE np n} ->
>           (t : Type) -> -- Finite t -> Not (Empty t) ->  
>           Vect np (Fin n) -> 
>           BayesNetwork n -> BayesNetwork (S n)

    0    1
     \  /|
      \/ |
      2  |
     /  \|
    3    4

> testBayesNetwork0 : BayesNetwork 0
> testBayesNetwork0 = Nil

> testBayesNetwork1 : BayesNetwork 1
> testBayesNetwork1 = Node Bool Nil testBayesNetwork0

> testBayesNetwork2 : BayesNetwork 2
> testBayesNetwork2 = Node Bool Nil testBayesNetwork1

> testBayesNetwork3 : BayesNetwork 3
> testBayesNetwork3 = Node {np = 2} Nat [0, 1] testBayesNetwork2

> testBayesNetwork4 : BayesNetwork 4
> testBayesNetwork4 = Node {np = 1} (Bool, Bool) [2] testBayesNetwork3

> testBayesNetwork5 : BayesNetwork 5
> testBayesNetwork5 = Node {np = 2} Unit [1, 2] testBayesNetwork4


> ||| The number of parents of a node
> np : {n : Nat} -> BayesNetwork n -> Fin n -> Nat
> np {n = Z}    Nil                    k impossible 
> np {n = S m} (Node {np = np'} _ _ g) k with (decEq k last)  
>   | (Yes _) = np'
>   | (No  c) = np g (strengthen k c)

> testnp : Nat
> testnp = np testBayesNetwork5 4


> ||| The type of a node
> type : {n : Nat} -> BayesNetwork n -> Fin n -> Type
> type {n = Z}    Nil                    k impossible 
> type {n = S m} (Node t _ g) k with (decEq k last)  
>   | (Yes _) = t
>   | (No  c) = type g (strengthen k c)

> testtype : Type
> testtype = type testBayesNetwork5 4


> ||| The parents of a node
> parents : {n : Nat} -> (g : BayesNetwork (S n)) -> (k : Fin (S n)) -> Vect (np g k) (Fin n)
> parents {n = Z}    Nil           k impossible
> parents {n = Z}   (Node _ Nil g) FZ = Nil
> parents {n = Z}   (Node _ ps g) (FS k) impossible
> parents {n = S m} (Node _ ps g) k with (decEq k last) 
>   | (Yes _) = ps
>   | (No  c) = map weaken (parents g (strengthen k c))

> testparents : Vect testnp (Fin 4)
> testparents = parents testBayesNetwork5 4 


> ||| The types of the nodes
> types : {n : Nat} -> (g : BayesNetwork n) -> Vect n Type
> types g = map (type g) (toVect id)


> ||| The types of the parents of a node
> parentTypes : {n : Nat} -> (g : BayesNetwork (S n)) -> (k : Fin (S n)) -> Vect (np g k) Type
> parentTypes g k = map ((type g) . weaken) (parents g k)
> -- parentTypes {n = Z}    Nil           k impossible
> -- parentTypes {n = Z}   (Node _ Nil g) FZ = Nil
> -- parentTypes {n = Z}   (Node _ ps g) (FS k) impossible
> -- parentTypes {n = S m} (Node _ ps g) k = map ((type g) . weaken) (parents g k)

> ||| A tuple of values of given types
> data Tuple : {n : Nat} -> Vect n Type -> Type where
>   TupleNil  : Tuple Nil
>   TupleCons : {n : Nat} -> {t : Type} -> {ts : Vect n Type} -> t -> Tuple ts -> Tuple (t :: ts) 


> ||| The chain rule? 
> chain : {n     :  Nat} -> 
>         (g     :  BayesNetwork (S n)) ->
>         (tab   :  (k : Fin (S n)) -> Tuple (parentTypes g k) -> type g k -> Double) ->
>         (vals  :  (k : Fin (S n)) -> type g k) -> Double
> chain {n = Z}    Nil             _   _  impossible
> chain {n = Z}   (Node _  ps  g) tab val = ?duda -- tab FZ TupleNil (val FZ)
> chain {n = S m} (Node _ ps  g) tab val = 1.0 * (chain g tab' vals') where
>   tab'  :  (k : Fin (S m)) -> Tuple (parentTypes g k) -> type g k -> Double
>   tab'  =  ?lala
>   val' :  (k : Fin (S m)) -> type g k
>   val' k = val (weaken k)


...

> ||| The number of parents of a node
> np : {n : Nat} -> BayesNetwork (S n) -> Fin (S n) -> Nat
> np {n = Z}    Nil                    k impossible 
> np {n = Z}   (Node _ _ _)            k = Z
> np {n = S m} (Node {np = np'} _ _ g) k with (decEq k last)  
>   | (Yes _) = np'
>   | (No  c) = np g (strengthen k c)

> testnp : Nat
> testnp = np testBayesNetwork5 4


> ||| The type of a node
> type : {n : Nat} -> BayesNetwork (S n) -> Fin (S n) -> Type
> type {n = Z}    Nil                    k impossible 
> type {n = Z}   (Node t _ _)            _ = t
> type {n = S m} (Node t _ g) k with (decEq k last)  
>   | (Yes _) = t
>   | (No  c) = type g (strengthen k c)

> testtype : Type
> testtype = type testBayesNetwork5 4


> ||| The parents of a node
> parents : {n : Nat} -> (g : BayesNetwork (S n)) -> (k : Fin (S n)) -> Vect (np g k) (Fin n)
> parents {n = Z}    Nil        k impossible
> parents {n = Z}   (Node _ _ _) k = Nil
> parents {n = S m} (Node _ ps g) k with (decEq k last) 
>   | (Yes _) = ps
>   | (No  c) = map weaken (parents g (strengthen k c))

> testparents : Vect testnp (Fin 4)
> testparents = parents testBayesNetwork5 4 


> ||| The types of the nodes
> types : {n : Nat} -> (g : BayesNetwork (S n)) -> Vect (S n) Type
> types g = map (type g) (toVect id)


> ||| The types of the parents of a node
> parentTypes : {n : Nat} -> (g : BayesNetwork (S n)) -> (k : Fin (S n)) -> Vect (np g k) Type
> parentTypes g k = map ((type g) . weaken) (parents g k)



> ||| A tuple of values of given types
> data Tuple : {n : Nat} -> Vect n Type -> Type where
>   TupleNil  : Tuple Nil
>   TupleCons : {n : Nat} -> {t : Type} -> {ts : Vect n Type} -> t -> Tuple ts -> Tuple (t :: ts) 


> ||| The chain rule? 
> chain : {n     :  Nat} -> 
>         (g     :  BayesNetwork (S n)) ->
>         (tab   :  (k : Fin (S n)) -> Tuple (parentTypes g k) -> type g k -> Double) ->
>         (vals  : (k : Fin (S n)) -> type g k) -> Double
> chain {n = Z}    Nil             _   _  impossible
> chain {n = Z}   (Node _  _  _) tab val = tab FZ TupleNil (val FZ)
> chain {n = S Z} (Node _ ps  g) tab val = ?gugu
> chain {n = S (S m)} (Node _ ps  g) tab val = 1.0 * (chain g tab' vals') where
>   tab'  :  (k : Fin (S m)) -> Tuple (parentTypes g k) -> type g k -> Double
>   tab'  =  ?lala
>   vals' :  (k : Fin (S m)) -> type g k
>   vals' =  ?kika


> ---}
 
 
 
 
 
