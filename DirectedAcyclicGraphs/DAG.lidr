> module DirectedAcyclicGraphs.DAG

> import Data.Fin

> import Fin.Operations
> import Fin.Properties
> -- import HList.HList

> %default total
> %access public export
> %auto_implicits off


> ||| A DAG
> data DAG : Nat -> Type where
>   Nil   :  DAG Z
>   Node  :  {n : Nat} -> (dn : DAG n) -> (isP : Fin n -> Bool) -> DAG (S n)


> |||
> isParent : {n : Nat} -> (dn : DAG n) -> (k : Fin n) -> (Fin (finToNat k) -> Bool)
> isParent {n = Z}    Nil k i impossible 
> isParent {n = S m} (Node dm isP) k i with (decEq k last)
>   | (Yes p) = s3 i where
>     s0 : Fin m -> Bool
>     s0 = isP
>     s1 : m = finToNat (last {n = m})
>     s1 = sym (finToNatLastLemma {m = m})
>     s2 : m = finToNat k
>     s2 = replace {P = \ X => m = finToNat X} (sym p) s1 
>     s3 : Fin (finToNat k) -> Bool
>     s3 = replace s2 s0
>   | (No  c) = isParent dm (strengthen k {contra = c}) s2 where
>     s0 : Fin (finToNat k)
>     s0 = i 
>     s1 : Fin (finToNat (strengthen {contra = c} k)) = Fin (finToNat k)
>     s1 = cong (finToNatStrengthenLemma k c)
>     s2 : Fin (finToNat (strengthen {contra = c} k))
>     s2 = replace {P = id} (sym s1) s0


> |||
> isParentLemma1 : {n : Nat} -> 
>                  (dn : DAG n) -> (isP : Fin n -> Bool) -> 
>                  isParent (Node dn isP) last = isP
> -- this requires extensional equality or a re-implementation of |isParent|


> |||
> isParentLemma2 : {n : Nat} -> {k : Fin n} -> 
>                  (dn : DAG n) -> (isP : Fin n -> Bool) -> 
>                  isParent (Node dn isP) (weaken k) = isParent dn k
> -- this requires extensional equality or a re-implementation of |isParent|


> |||
> fold : {X : Nat -> Type} ->
>        {n : Nat} ->
>        (e : X Z) ->
>        (f : (n : Nat) -> X n -> (Fin n -> Bool) -> X (S n)) ->
>        DAG n -> X n
> fold {n = Z}   e f  Nil        = e 
> fold {n = S m} e f (Node g isP) = f m (fold e f g) isP


> {-

> data DAG : Type -> Nat -> Type where
>   Node : {A : Type} -> {n : Nat} -> {ks : List (Fin n)} -> 
>          A -> HList (map (DAG A) (map finToNat ks)) -> 
>          DAG A n -> DAG A (S n)

> lalaDAG : {A : Type} -> {n : Nat} -> {X : Nat -> Type} ->
>           (f : {B : Type} -> {m : Nat} -> {js : List (Fin m)} -> 
>                B -> HList (map X (map finToNat js)) -> X m -> X (S m)) ->
>           DAG A n -> X n
> lalaDAG {X} f (Node a dags dag) = f a xs x where
>   xs : HList (map X (map finToNat js))
>   xs = ?kika
>   x  : X m
>   x  = ?lala

> ---}


> {-

> foldDAG : {A : Type} -> {n : Nat} -> {X : Nat -> Type} ->
>           (f : {A : Type} -> {n : Nat} -> {ks : List (Fin n)} -> 
>                A -> HList (map X (map finToNat ks)) -> X (S n)) ->
>           DAG A (S n) -> X (S n)

> foldDAG {A} {n} {X} f (DAGNode {ks} a dags) = f a s2 where
>   s0 : HList (map (DAG A) (map finToNat ks))
>   s0 = dags
>   g  : HList (map (\ k => DAG A k -> X k) (map finToNat ks))
>   g  = h (map finToNat ks) where
>     h : (js : List Nat) -> HList (map (\ k => DAG A k -> X k) js)
>     h Nil = Nil
>     h (j :: js) = (foldDAG {n = j} f) :: (h js)
>   s2 : HList (map X (map finToNat ks))
>   s2 = hmap {A = Nat} {as = map finToNat ks} {Dom = DAG A} {Cod = X} g s0

> ---}


> {-


Example:

    0    1
     \  /|
      \/ |
      2  |
     /  \|
    3    4

> testDAG0 : DAG 0
> testDAG0 = Nil

> testDAG1 : DAG 1
> testDAG1 = Node Nil testDAG0

> testDAG2 : DAG 2
> testDAG2 = Node Nil testDAG1

> testDAG3 : DAG 3
> testDAG3 = Node {np = 2} [0, 1] testDAG2

> testDAG4 : DAG 4
> testDAG4 = Node {np = 1} [2] testDAG3

> testDAG5 : DAG 5
> testDAG5 = Node {np = 2} [1, 2] testDAG4

> np : {n : Nat} -> DAG (S n) -> Fin (S n) -> Nat
> np {n = Z}    Nil                   k impossible 
> np {n = Z}   (Node ps g)            k = Z
> np {n = S m} (Node {np = np'} ps g) k with (decEq k last)  
>   | (Yes _) = np'
>   | (No  c) = np g (strengthen k c)

> testnp : Nat
> testnp = np testDAG5 4

> parents : {n : Nat} -> (g : DAG (S n)) -> (k : Fin (S n)) -> Vect (np g k) (Fin n)
> parents {n = Z}    Nil        k impossible
> parents {n = Z}   (Node ps g) k = Nil
> parents {n = S m} (Node ps g) k with (decEq k last) 
>   | (Yes _) = ps
>   | (No  c) = map weaken (parents g (strengthen k c))

> testparents : Vect testnp (Fin 4)
> testparents = parents testDAG5 4 

> ---}
