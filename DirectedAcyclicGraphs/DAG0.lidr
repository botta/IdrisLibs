> module DirectedAcyclicGraphs.DAG0

> import Data.List

> import Rel.ReachableFrom

> %default total
> %access public export
> %auto_implicits off


> mutual

>   ||| A DAG as a set of edges
>   data DAG : Type -> Type where
> 
>     Nil   :  {A : Type} -> 
>              DAG A
> 
>     Node  :  {A : Type} -> 
>              (x : A) ->
>              (d : DAG A) -> 
>              {auto xne : Not (Elem x (nodes d))} ->             
>              DAG A
>            
>     Edge  :  {A : Type} -> 
>              (x : A) -> 
>              (y : A) ->
>              (d : DAG A) ->  
>              {auto xe : Elem x (nodes d)} ->             
>              {auto ye : Elem y (nodes d)} ->             
>              {auto ne : Not (x = y)} ->
>              {auto nr : Not (ReachableFrom (edges d) y x)} ->
>              DAG A

>   nodes : {A : Type} -> DAG A -> List A
>   nodes  Nil         = Nil
>   nodes (Node v d)   = v :: nodes d
>   nodes (Edge x y d) = nodes d

>   edges : {A : Type} -> DAG A -> List (A, A)
>   edges  Nil         = Nil
>   edges (Node v d)   = edges d
>   edges (Edge x y d) = (x, y) :: (edges d)


Example:

    0    1
     \  /|
      \/ |
      2  |
     /  \|
    3    4

> testDAG0 : DAG Nat
> testDAG0 = Nil

> testDAG1 : DAG Nat
> testDAG1 = Node 0 testDAG0

> {-

> testDAG2 : DAG Nat
> testDAG2 = Edge Nil testDAG1

> testDAG3 : DAG 3
> testDAG3 = Node {np = 2} [0, 1] testDAG2

> testDAG4 : DAG 4
> testDAG4 = Node {np = 1} [2] testDAG3

> testDAG5 : DAG 5
> testDAG5 = Node {np = 2} [1, 2] testDAG4

> np : {n : Nat} -> DAG (S n) -> Fin (S n) -> Nat
> np {n = Z}    Nil                   k impossible 
> np {n = Z}   (Node ps g)            k = Z
> np {n = S m} (Node {np = np'} ps g) k with (decEq k last)  
>   | (Yes _) = np'
>   | (No  c) = np g (strengthen k c)

> testnp : Nat
> testnp = np testDAG5 4

> parents : {n : Nat} -> (g : DAG (S n)) -> (k : Fin (S n)) -> Vect (np g k) (Fin n)
> parents {n = Z}    Nil        k impossible
> parents {n = Z}   (Node ps g) k = Nil
> parents {n = S m} (Node ps g) k with (decEq k last) 
>   | (Yes _) = ps
>   | (No  c) = map weaken (parents g (strengthen k c))

> testparents : Vect testnp (Fin 4)
> testparents = parents testDAG5 4 

> ---}
 
