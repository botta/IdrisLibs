> module DirectedAcyclicGraphs.DAG1

> import Data.Fin

> import Fin.Operations
> import List.HList

> %default total
> %access public export
> %auto_implicits off


> ||| A DAG
> data DAG : Nat -> Type where
>   Nil   :  DAG Z
>   Node  :  {n : Nat} -> 
>            (g   : DAG n) -> 
>            (ps  : Fin n -> Bool) ->
>            DAG (S n)

> {-

> fold : {X : Nat -> Type} ->
>        {n : Nat} ->
>        (e : X Z) ->
>        (f : (n : Nat) -> X n -> (Fin n -> Bool) -> X (S n)) ->
>        DAG n -> X n
> fold {n = Z}   e f  Nil        = e 
> fold {n = S m} e f (Node g ps) = f m (fold e f g) ps

> ---}

> {-


Example:

    0    1
     \  /|
      \/ |
      2  |
     /  \|
    3    4

> testDAG0 : DAG 0
> testDAG0 = Nil

> testDAG1 : DAG 1
> testDAG1 = Node Nil testDAG0

> testDAG2 : DAG 2
> testDAG2 = Node Nil testDAG1

> testDAG3 : DAG 3
> testDAG3 = Node {np = 2} [0, 1] testDAG2

> testDAG4 : DAG 4
> testDAG4 = Node {np = 1} [2] testDAG3

> testDAG5 : DAG 5
> testDAG5 = Node {np = 2} [1, 2] testDAG4

> np : {n : Nat} -> DAG (S n) -> Fin (S n) -> Nat
> np {n = Z}    Nil                   k impossible 
> np {n = Z}   (Node ps g)            k = Z
> np {n = S m} (Node {np = np'} ps g) k with (decEq k last)  
>   | (Yes _) = np'
>   | (No  c) = np g (strengthen k c)

> testnp : Nat
> testnp = np testDAG5 4

> parents : {n : Nat} -> (g : DAG (S n)) -> (k : Fin (S n)) -> Vect (np g k) (Fin n)
> parents {n = Z}    Nil        k impossible
> parents {n = Z}   (Node ps g) k = Nil
> parents {n = S m} (Node ps g) k with (decEq k last) 
>   | (Yes _) = ps
>   | (No  c) = map weaken (parents g (strengthen k c))

> testparents : Vect testnp (Fin 4)
> testparents = parents testDAG5 4 

> ---}
