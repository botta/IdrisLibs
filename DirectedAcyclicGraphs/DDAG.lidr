> module DirectedAcyclicGraphs.DDAG

> import Data.Fin

> import Fin.Operations

> %default total
> %access public export
> %auto_implicits off


> {-

> ||| A DDAG (a DAG with data)
> data DDAG : Nat -> Type where
>   Nil   :  DDAG Z
>   Node  :  {n  : Nat} -> 
>            (g  : DDAG n) -> 
>            (t  : Type) ->
>            (v  : t) ->
>            (ps : Fin n -> Bool) ->
>            DDAG (S n)


> fold : {X : Nat -> Type} ->
>        {n : Nat} ->
>        (e : X Z) ->
>        (f : (n : Nat) -> (x: X n) -> (t : Type) -> (v : t) -> (ps : Fin n -> Bool) -> X (S n)) ->
>        DDAG n -> X n
> fold {n = Z}   e f  Nil        = e 
> fold {n = S m} e f (Node g t v ps) = f m (fold e f g) t v ps

> ---}

> data DAG : Type -> Nat -> Type where
>   Nil   :  {t  : Type} -> DAG t Z
>   Node  :  {t  : Type} -> {n  : Nat} -> 
>            t -> (Fin n -> Bool) -> DAG t n -> DAG t (S n)

> fold : {t : Type} -> {X : Nat -> Type} ->
>        {n : Nat} ->
>        (e : X Z) ->
>        (f : {t' : Type} -> {n' : Nat} -> (v : t') -> (ps : Fin n' -> Bool) -> X n' -> X (S n')) ->
>        DAG t n -> X n
> fold {n = Z}   e f  Nil        = e 
> fold {n = S m} e f (Node v ps dag) = f v ps (fold e f dag)
