# IdrisLibs

This is a major revision and an extension of the framework for the
specification and the solution of Sequential Decision Problems (SDPs)
originally developed in
[SeqDecProbs](https://github.com/nicolabotta/SeqDecProbs). Some related
Agda code is available in
[patrikja/SeqDecProb_Agda](https://github.com/patrikja/SeqDecProb_Agda).
See also [IdrisLibs2](https://gitlab.pik-potsdam.de/botta/IdrisLibs2),
[PythonLibs](https://gitlab.pik-potsdam.de/botta/PythonLibs)

## Type checking

With 1.3.3-git:ae98085b8, you should be able to type check all the
basic libraries by entering "make libs" in the top-level repository. The
flag `--allow-capitalized-pattern-variables` is needed, see
[Makefile](Makefile).


## Research papers

### 2020: Semantic verification of dynamic programming

Authors: Nuria Brede, [Nicola Botta](https://www.pik-potsdam.de/members/botta/homepage)

* Jul. 2020: Submitted to the Journal of Functional Programming [Full text pre-print available](https://arxiv.org/abs/2008.02143)
    
* Nov. 2020: Review verdict: "Revise and resubmit"

* Feb. 2021: Re-submitted to the Journal of Functional Programming [Full text pre-print available](https://gitlab.pik-potsdam.de/botta/papers/-/tree/master/2021.On%20the%20Correctness%20of%20Monadic%20Backward%20Induction)

This is [TiPES](https://www.tipes.dk/) contribution No 37. This project
has received funding from the European Union’s Horizon 2020 research and
innovation programme under grant agreement No 820970.


### 2020: Extensional equality preservation and verified generic programming

Authors: [Nicola Botta](https://www.pik-potsdam.de/members/botta/homepage), Nuria Brede,
Patrik Jansson, Tim Richter

* Jul. 2020: Submitted to the Journal of Functional Programming [Full text pre-print available](https://arxiv.org/abs/2008.02123)

* Feb. 2021: 

This is [TiPES](https://www.tipes.dk/) contribution No 38. This project
has received funding from the European Union’s Horizon 2020 research and
innovation programme under grant agreement No 820970.


### 2018: The impact of uncertainty on optimal emission policies

Authors: [Nicola Botta](https://www.pik-potsdam.de/members/botta/homepage), Patrik Jansson, Cezar Ionescu

Published in Earth System Dynamics: https://esd.copernicus.org/articles/9/525/2018/


### 2015-2017: Contributions to a computational theory of policy advice and avoidability

Authors: [Nicola Botta](https://www.pik-potsdam.de/members/botta/homepage), Patrik Jansson, Cezar Ionescu

Paper: http://www.cse.chalmers.se/~patrikj/papers/CompTheoryPolicyAdviceAvoidability_JFP_2017_postprint.pdf

Published in Journal of Functional Programming: https://doi.org/10.1017/S0956796817000156

* 2016-01-06: Submitted to the Journal of Functional Programming (JFP) Special issue on Dependently Typed Programming. (JFP is a [RoMEO Green journal](http://www.sherpa.ac.uk/romeo/search.php?issn=0956-7968).)
    * [Full text pre-print available](http://www.cse.chalmers.se/~patrikj/papers/CompTheoryPolicyAdviceAvoidability_JFP_2016_preprint.pdf)
* 2016-07: Review verdict: "Reject and resubmit"
* 2016-11-11: Re-submitted to the Journal of Functional Programming (JFP)
    * [Full text pre-print available](http://www.cse.chalmers.se/~patrikj/papers/CompTheoryPolicyAdviceAvoidability_JFP_2016-11_preprint.pdf)
* 2017-03: Review verdict: "Revise and resubmit"
    * [Full text pre-print available](http://www.cse.chalmers.se/~patrikj/papers/CompTheoryPolicyAdviceAvoidability_JFP_2017-04_preprint.pdf)
* 2017-09: Accept!
    * [Full text post-print available](http://www.cse.chalmers.se/~patrikj/papers/CompTheoryPolicyAdviceAvoidability_JFP_2017_postprint.pdf)

The work was partially supported by the
[GRACeFUL project](https://www.graceful-project.eu/)
(640954, from the call H2020-FETPROACT-2014) and by the
[CoeGSS project](http://coegss.eu/)
(676547, H2020-EINFRA-2015-1) in the context of
Global Systems Science (GSS).


### 2014-2016: SDPs, dependent types and generic solutions

Title: Sequential decision problems, dependent types and generic solutions

Authors: [Nicola Botta](https://www.pik-potsdam.de/members/botta/homepage), Patrik Jansson, Cezar Ionescu, David R. Christiansen, Edwin Brady

Paper: https://lmcs.episciences.org/3202

Published in Logical Methods in Computer Science, March 17, 2017, Volume 13, Issue 1


### 2013: SDPs, dependently-typed solutions

Title: Sequential decision problems, dependently-typed solutions.

Authors: [Nicola Botta](https://www.pik-potsdam.de/members/botta/homepage), Cezar Ionescu, and Edwin Brady.

Paper: http://ceur-ws.org/Vol-1010/paper-06.pdf

Published in Proceedings of the Conferences on Intelligent Computer
  Mathematics (CICM 2013), "Programming Languages for Mechanized Mathematics
  Systems Workshop (PLMMS)", volume 1010 of CEUR Workshop Proceedings, 2013.








