> module HVect.HVect

> import Data.Vect

> %default total
> %access public export
> %auto_implicits off


> ||| An heterogenous vector (a vector of values of possibly different types)
> data HVect : (n : Nat) -> Vect n Type -> Type where
>   Nil  : HVect Z Nil
>   (::) : {n : Nat} -> {t : Type} -> {ts : Vect n Type} -> 
>          t -> HVect n ts -> HVect (S n) (t :: ts) 


