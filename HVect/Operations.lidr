> module HVect.Operations

> import Data.Vect

> import HVect.HVect

> %default total
> %access public export
> %auto_implicits off


* Elementary functions

> ||| The types of the compunents of a h-vect
> types : {n : Nat} -> {ts : Vect n Type} -> HVect n ts -> Vect n Type
> types {ts} _ = ts

> ||| The head of an h-vect
> head : {n : Nat} -> {t : Type} -> {ts : Vect n Type} -> 
>        HVect (S n) (t :: ts) -> t
> head (x :: xs) = x

> ||| The tail of an h-vect
> tail : {n : Nat} -> {t : Type} -> {ts : Vect n Type} -> 
>        HVect (S n) (t :: ts) -> HVect n ts
> tail (x :: xs) = xs

> ||| Append two h-vects
> (++) : {m, n : Nat} -> {ts : Vect m Type} -> {us : Vect n Type} ->
>        HVect m ts -> HVect n us -> HVect (m + n) (ts ++ us)
> (++)  Nil      ys = ys
> (++) (x :: xs) ys = x :: (xs ++ ys)


* Map

> ||| Mapping a dependently typed function on a vect makes an h-vect
> dmap : {A : Type} -> {P : A -> Type} -> {n : Nat} -> 
>        (f : (a : A) -> P a) -> (as : Vect n A) -> HVect n (map P as)
> dmap f  Nil      = Nil 
> dmap f (a :: as) = (f a) :: (dmap f as) 


* Cross

> ||| Componentwise apply a vector of functions to the elements of an h-vect
> cross : {n : Nat} -> {A : Type} -> {as : Vect n A} -> 
>         {Dom : A -> Type} -> {Cod : A -> Type} ->
>         HVect n (map (\ a => (Dom a -> Cod a)) as) -> 
>         HVect n (map Dom as) -> HVect n (map Cod as)
> cross {as = Nil} _ _ = Nil
> cross {as = a :: as'} (f :: fs') (x :: xs') = (f x) :: (cross {as = as'} fs' xs')


> ||| |vectProd|, by Matti Richter
> vectProd : {n : Nat} -> {ts : Vect n Type} -> HVect n (map List ts) -> List (HVect n ts)
> vectProd {ts = Nil}        _    = [Nil]
> vectProd {ts = (t :: ts')} yzss = [x :: yzs | x <- (head yzss), yzs <- (vectProd (tail yzss))]


> {-


> |||
> show : {n : Nat} -> {ts : Vect n Type} -> 


> ---}
 
 
