> module HVect.Properties

> import Data.Vect
> import Data.Vect.Quantifiers
> import Data.Fin
> import Control.Isomorphism

> import HVect.HVect
> import Basic.Predicates
> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties
> import Sigma.Sigma
> import Sigma.Operations

> %default total
> %access public export
> %auto_implicits on


* 

> ||| Equal h-vects have equal heads
> injective1 : {n : Nat} -> {xs, ys: HVect n ts} -> {t : Type} -> {x, y : t} -> 
>              x :: xs = y :: ys -> x = y
> injective1 Refl = Refl

> ||| Equal h-vects have equal tails
> injective2 : {n : Nat} -> {xs, ys: HVect n ts} -> {t : Type} -> {x, y : t} ->  
>              x :: xs = y :: ys -> xs = ys
> injective2 Refl = Refl


* Products of finite types

> finiteNil : Finite (HVect Z Nil) 
> finiteNil = MkSigma (S Z) iso where
>   to     : HVect Z Nil -> Fin (S Z)
>   to Nil = FZ
>   from   : Fin (S Z) -> HVect Z Nil
>   from FZ = Nil
>   toFrom : (k : Fin (S Z)) -> to (from k) = k
>   toFrom FZ = Refl
>   fromTo : (v : HVect Z Nil) -> from (to v) = v
>   fromTo Nil = Refl
>   iso    : Iso (HVect Z Nil) (Fin (S Z))
>   iso    = MkIso to from toFrom fromTo

> finiteCons : {n : Nat} -> {t : Type} -> {ts : Vect n Type} ->
>              Finite t -> Finite (HVect n ts) -> Finite (HVect (S n) (t :: ts))
> finiteCons {n} {t} {ts} ft fxyzs = s5 where
>   s0 : Iso (HVect (S n) (t :: ts)) (t, HVect n ts) 
>   s0 = MkIso to from toFrom fromTo where
>     to     : HVect (S n) (t :: ts) -> (t, HVect n ts) 
>     to Nil impossible
>     to (x :: yzs) = (x, yzs)
>     from   : (t, HVect n ts) -> HVect (S n) (t :: ts)
>     from (x, yzs) = x :: yzs
>     toFrom : (xyzs : (t, HVect n ts)) -> to (from xyzs) = xyzs
>     toFrom (x, yzs) = Refl
>     fromTo : (xyzs : HVect (S n) (t :: ts)) -> from (to xyzs) = xyzs
>     fromTo Nil impossible
>     fromTo (x :: yzs) = Refl
>   s1 : Finite (t, HVect n ts)
>   s1 = finiteProduct ft fxyzs
>   s2 : Nat
>   s2 = outl s1
>   s3 : Iso (t, HVect n ts) (Fin s2)
>   s3 = outr s1
>   s5 : Finite (HVect (S n) (t :: ts))
>   s5 = MkSigma s2 (isoTrans s0 s3)


> ||| Products of finite types are finite
> finiteLemma : {n : Nat} -> (ts : Vect n Type) -> All Finite ts -> Finite (HVect n ts)
> finiteLemma  Nil _ = finiteNil
> finiteLemma (t :: ts) (ft :: fts) = finiteCons ft (finiteLemma ts fts)


* Products of non-empty types

> notEmptyNil : Not (Empty (HVect Z Nil))
> notEmptyNil contra = contra Nil

> notEmptyCons : {n : Nat} -> {t : Type} -> {ts : Vect n Type} ->
>                Not (Empty t) -> Not (Empty (HVect n ts)) -> 
>                Not (Empty (HVect (S n) (t :: ts)))
> notEmptyCons {n} {t} {ts} contraHead contraTail contra = contraHead (contraTail . contra') where
>   contra' : t -> HVect n ts -> Void
>   contra' x yzs = contra (x :: yzs)
 
> ||| Products of non-empty types are non-empty
> notEmptyLemma : {n : Nat} -> (ts : Vect n Type) -> All (Not . Empty) ts -> Not (Empty (HVect n ts))
> notEmptyLemma  Nil       _            = notEmptyNil
> notEmptyLemma (t :: ts) (net :: nets) = notEmptyCons net (notEmptyLemma ts nets)



* Decidability of products of decidable types

> decEqLemma : {n : Nat} -> (ts : Vect n Type) -> All DecEq ts -> DecEq (HVect n ts)



* Instances

> implementation Eq (HVect Z Nil) where
>   Nil == Nil = True

> implementation (Eq t, Eq (HVect n ts)) => Eq (HVect (S n) (t::ts)) where
>   (x :: xs) == (y :: ys) = x == y && xs == ys

> implementation DecEq (HVect Z Nil) where
>   decEq Nil Nil = Yes Refl

> implementation (DecEq t, DecEq (HVect n ts)) => DecEq (HVect (S n) (t :: ts)) where
>   decEq (x :: xs) (y :: ys) with (decEq x y)
>     decEq (z :: xs) (z :: ys) | Yes Refl with (decEq xs ys)
>       decEq (z :: zs) (z :: zs) | Yes Refl | Yes Refl = Yes Refl
>       decEq (z :: xs) (z :: ys) | Yes Refl | No ctr = No (ctr . injective2)
>     decEq (x :: xs) (y :: ys) | No ctr = No (ctr . injective1)

> interface Shows (ts : Vect k Type) where
>   shows : HVect k ts -> Vect k String

> implementation Shows Nil where
>   shows Nil = Nil

> implementation (Show t, Shows ts) => Shows (t :: ts) where
>   shows (x :: xs) = show x :: shows xs

> implementation (Shows ts) => Show (HVect n ts) where
>   show xs = "[" ++ (pack . intercalate [','] . map unpack . toList $ shows xs) ++ "]"


> {-

> ---}

