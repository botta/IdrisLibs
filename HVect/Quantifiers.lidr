> module HVect.Quantifiers

> import Data.Vect

> import HVect.HVect
> import HVect.Operations
> import Vect.Properties

> %default total
> %access public export
> %auto_implicits off

> All : {n : Nat} -> {ts : Vect n Type} -> 
>       (PS : HVect n (map (\ t => t -> Type) ts)) -> HVect n ts -> Type
> All {n} {ts} PS xs = HVect n (types (cross {Cod = \ A => Type} PS (replace (sym (mapIdLemma ts)) xs)))


> {-

> ---}
