> module VeriFunctor.VeriFunctor

> import Fun.Predicates

> %default total
> %access public export
> %auto_implicits off

> interface Functor F => VeriFunctor (F : Type -> Type) where
> 
>   mapPresId    : {A : Type} -> ExtEq {A = F A} (map id) id
>   
>   mapPresComp  : {A, B, C : Type} -> (f : A -> B) -> (g : B -> C) -> 
>                  ExtEq {A = F A} (map (g . f)) (map g . map f)
>                 
>   mapPresExtEq : {A, B : Type} -> (f, g : A -> B) -> 
>                  ExtEq f g -> ExtEq {A = F A} (map f) (map g)

> {-

> ---}

