> module HList.Operations

> import Data.List
> import Data.Fin

> import HList.HList
> import Bool.Properties
> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties
> import Maybe.Properties


> %default total
> %access public export
> %auto_implicits off

> ||| The head of an h-list
> head : {t : Type} -> {ts : List Type} -> HList (t :: ts) -> t
> head {t} {ts} (x :: xs) = x


> ||| The tail of an h-list
> tail : {t : Type} -> {ts : List Type} -> HList (t :: ts) -> HList ts
> tail {t} {ts} (x :: xs) = xs


> ||| Componentwise apply a list of functions to the elements of an h-list 
> cross : {A : Type} -> {as : List A} -> {Dom : A -> Type} -> {Cod : A -> Type} ->
>         HList (map (\ a => (Dom a -> Cod a)) as) ->  HList (map Dom as) -> HList (map Cod as)
> cross {as = Nil} _ _ = Nil
> cross {as = a :: as'} (f :: fs') (x :: xs') = (f x) :: (cross {as = as'} fs' xs')


On 2019-05-14) we (Tim Richter, Nuria Brede, Matti Richter and myself,
Nicola Botta) discussed how to implement generic elementary functions
for probabilistic inference, see ProbabilisticInference/Basic.lidr.

Matti suggested to first implement

> ||| |listProd|, by Matti Richter
> listProd : {ts : List Type} -> HList (map List ts) -> List (HList ts)
> listProd {ts = Nil}        _    = [Nil]
> listProd {ts = (t :: ts')} yzss = [x :: yzs | x <- (head yzss), yzs <- (listProd (tail yzss))]
> -- listProd {ts = Nil} Nil = [Nil]
> -- listProd {ts = (t :: ts')} (xs :: yzss) = [x :: yzs | x <- xs, yzs <- (listProd yzss)]

The function takes an h-list of (homogeneous) lists of the types of
|ts|. Thus, for |ts = [Bool, Fin 42, Maybe (Fin 3)]|, an argument of
|listProd| could be

  [[T], [7, 11, 0], [Nothing]]

|listProd| generates a list of h-list with all the products compatible
with the input:

  [[T,7,Nothing], [T,11,Nothing], [T,0,Nothing]]

With |listProd| one can implement a function that takes a list of types
|ts|, a numerical type |T|, a function |f : HList ts -> T| and an h-list
of lists of the types of |ts| |l : HList (map List ts)| and adds all the
values obtrained by applying |f| to the products compatible with |l|:

> ||| |sum|, by Matti Richter
> sum : {ts : List Type} -> {T : Type} -> (Num T) => (HList ts -> T) -> HList (map List ts) -> T
> sum f l = sum (map f (listProd l)) 

For an example of how |sum| could be applied to compute marginal
probabilities, see ProbabilisticInference/Basic.lidr.


> {-

> ---}

