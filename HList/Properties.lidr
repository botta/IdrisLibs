> module HList.Properties

> import Data.List
> import Data.List.Quantifiers

> import HList.HList
> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties

> %default total
> %access public export
> %auto_implicits off


> finiteLemma : (ts : List Type) -> All Finite ts -> Finite (HList ts)



> {-

> ---}

