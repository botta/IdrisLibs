> module HList.HList

> %default total
> %access public export
> %auto_implicits off


> ||| An heterogenous list (a list of values of possibly different types)
> data HList : List Type -> Type where
>   Nil  : HList Nil
>   (::) : {t : Type} -> {ts : List Type} -> t -> HList ts -> HList (t :: ts) 

