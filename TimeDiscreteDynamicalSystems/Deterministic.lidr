> module TimeDiscreteDynamicalSystems.Deterministic

> import Data.Vect
> import Syntax.PreorderReasoning

> import Vect.Properties
> import Fun.Predicates

> %default total
> %access public export
> %auto_implicits off


Preliminaries
=============

A time discrete deterministic dynamical system (in short, a
\emph{deterministic System}) on a set |X| is a function of type |X ->
X|:

> DetSys : Type -> Type
> DetSys X = X -> X

The domain of |f : DetSys X| is often called the "state space" of |f|:

> StateSpace : {X : Type} -> DetSys X -> Type
> StateSpace = Domain


Flow
====

The flow of a deterministic system |f| over |t| steps is another
deterministic system:

> flow : {X : Type} -> Nat -> DetSys X -> DetSys X
> flow  Z    f  x = x
> flow (S n) f  x = flow n f (f x) 

Clearly, |flow t f = f^t|. The following two lemmas show that

  1) |flow 0 f = id|

  2) |flow (t + t') f = flow t' f . flow t f| 

The proofs are trivial. They are spelled out (up to a certain extent)
for readability:

> flowLemma1 : {X : Type} -> 
>              (f : DetSys X) -> (x : X) -> flow Z f x = x 
> flowLemma1 f x = Refl

> flowLemma2 : {X : Type} -> {m, n : Nat} ->
>              (f : DetSys X) -> (x : X) -> flow (m + n) f x = flow n f (flow m f x) 
> flowLemma2 {m = Z} {n} f x = ( flow (Z + n) f x )
>                            ={ Refl }=
>                              ( flow n f x )
>                            ={ Refl }=
>                              ( flow n f (flow Z f x) )
>                            QED
> flowLemma2 {m = S l} {n} f x = ( flow (S l + n) f x )
>                              ={ Refl }=
>                                ( flow (S (l + n)) f x )
>                              ={ Refl }=
>                                ( flow (l + n) f (f x) )
>                              ={ flowLemma2 f (f x) }=
>                                ( flow n f (flow l f (f x)) )
>                              ={ Refl }=
>                                ( flow n f (flow (S l) f x) )
>                              QED


Trajectories
============

For a dynamical system |f : DetSys X|, the trajectory of length |n :
Nat| starting at |x : X| is just the sequence |[x, f x, ..., f^n x]|:

> trj : {X : Type} -> (n : Nat) -> DetSys X -> X -> Vect (S n) X
> trj  Z     f  x  = x :: Nil 
> trj (S n)  f  x  = x :: trj n f (f x) 

The last element of the trajectory of length |n| of |f| starting in |x|
is just |flow n f x|:

> flowTrjLemma : {X : Type} -> 
>                (n : Nat) -> (f : DetSys X) -> (x : X) -> flow n f x = last (trj n f x)
> flowTrjLemma Z f x = ( flow Z f x )
>                    ={ Refl }=
>                      ( x )
>                    ={ Refl }=
>                      ( last (trj Z f x) )
>                    QED
> flowTrjLemma (S m) f x = ( flow (S m) f x )
>                        ={ Refl }=
>                          ( (flow m f) (f x) )
>                        ={ flowTrjLemma m f (f x) }=
>                          ( last (trj m f (f x)) )
>                        ={ sym (lastLemma x (trj m f (f x))) }=
>                          ( last (x :: trj m f (f x)) )
>                        ={ Refl }=
>                          ( last (trj (S m) f x) )
>                        QED
>                        


> {-

> ---}


