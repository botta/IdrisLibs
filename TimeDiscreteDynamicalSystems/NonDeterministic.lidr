> module TimeDiscreteDynamicalSystems.NonDeterministic

> import Data.List
> import Data.Vect
> import Syntax.PreorderReasoning

> import List.Operations
> import List.Properties
> import Fun.Predicates
> import VeriFunctor.VeriFunctor
> import VeriMonad.VeriMonad

> import TimeDiscreteDynamicalSystems.Deterministic

> %default total
> %access public export
> %auto_implicits off


Preliminaries
=============

A discrete non-deterministic dynamical system on a set |X| is a function
of type |X -> List X|:

> NonDetSys : Type -> Type
> NonDetSys X = X -> List X

The set |X| in |f : NonDetSys X| is often called the "state space" of the
system |f|:

> StateSpace : {X : Type} -> NonDetSys X -> Type
> StateSpace = Domain

Every deterministic system on |X| can be represented by a
non-deterministic systems on |X|:

> embed : {X : Type} -> DetSys X -> NonDetSys X
> embed f = ret . f 


Flow
====

The flow of a non-deterministic system |f| over |t| steps is another
non-deterministic system:

> flow : {X : Type} -> Nat -> NonDetSys X -> NonDetSys X
> flow  Z    f x = pure x
> flow (S n) f x = f x >>= flow n f -- = join (map (flow n f) (f x))

Trivially, one has |flow Z f = pure|:

> ||| flow Z f x = pure x
> flowLemma1 : {X : Type} -> 
>              (f : NonDetSys X) -> (x : X) -> flow Z f x = pure x  
> flowLemma1 f x = Refl

and also |flow (t + t') f x = flow t f x >>= flow t' f|:

> mutual

>   ||| flow (t + t') f x = flow t f x >>= flow t' f
>   flowLemma2 : {X : Type} -> {m, n : Nat} ->
>                (f : NonDetSys X) -> (x : X) -> 
>                flow (m + n) f x = flow m f x >>= flow n f
>              
>   flowLemma2 {m = Z} {n} f x = ( flow (Z + n) f x )
>                              ={ Refl }=
>                                ( flow n f x )
>                              ={ sym (leftIdentity (flow n f) x) }=
>                                ( pure x >>= flow n f )
>                              ={ Refl }=
>                                ( flow Z f x >>= flow n f )
>                              QED
>
>   flowLemma2 {m = S l} {n} f x = ( flow (S l + n) f x )
>                                ={ Refl }=
>                                  ( flow (S (l + n)) f x )
>                                ={ Refl }=
>                                  ( f x >>= flow (l + n) f )
>                                {-  
>                                ={ cong (extensionalEquality 
>                                         (flow (l + n) f)
>                                         (\ y => flow l f y >>= flow n f)
>                                         (\ y => flowLemma2 f y)) }=
>                                -}
>                                ={ flowLemma2' {m = l} {n = n} f (f x) }=
>                                  ( f x >>= (\ y => flow l f y >>= flow n f) )
>                                ={ sym (associativity (flow l f) (flow n f) (f x)) }=
>                                  ( (f x >>= flow l f) >>= flow n f )
>                                ={ Refl }=
>                                  ( flow (S l) f x >>= flow n f )
>                                QED

We can avoid postulating extensional equality of functions with an
auxiliary lemma. Notice that |flowLemma2| and |flowLemma2'| are mutually
dependent:
 
>   flowLemma2' : {X : Type} -> {m, n : Nat} ->
>                 (f : NonDetSys X) -> (xs : List X) ->
>                 xs >>= flow (m + n) f = xs >>= \ y => flow m f y >>= flow n f
>   flowLemma2' {m} {n} f  Nil      = Refl
>   flowLemma2' {m} {n} f (x :: xs) = let g = flow (m + n) f in
>                                     let h = \ y => flow m f y >>= flow n f in
>                                     ( (x :: xs) >>= flow (m + n) f )
>                                   ={ Refl }=
>                                     ( (x :: xs) >>= g )
>                                   ={ bindJoinMapSpec g (x :: xs) }=
>                                     ( join (map g (x :: xs)) )
>                                   ={ Refl }=
>                                     ( g x ++ join (map g xs) )
>                                   ={ cong (sym (bindJoinMapSpec g xs)) }=
>                                     ( g x ++ (xs >>= g) )
>                                   ={ cong (flowLemma2' f xs) }=
>                                     ( g x ++ (xs >>= h) )  
>                                   ={ cong {f = \ X => X ++ (xs >>= h)} (flowLemma2 f x) }=  
>                                     ( h x ++ (xs >>= h) )
>                                   ={ cong (bindJoinMapSpec h xs) }=
>                                     ( h x ++ join (map h xs) )
>                                   ={ Refl }=
>                                     ( join (map h (x :: xs)) )
>                                   ={ sym (bindJoinMapSpec h (x :: xs)) }=
>                                     ( (x :: xs) >>= h )
>                                   ={ Refl }=
>                                     ( (x :: xs) >>= \ y => flow m f y >>= flow n f )
>                                   QED

Every non-deterministic system |f : NonDetSys X| can be represented by
an equivalent deterministic systems on |List X|

> |||
> repr : {X : Type} -> NonDetSys X -> DetSys (List X)
> repr f xs = xs >>= f

> ||| 
> reprLemma : {X : Type} -> (n : Nat) -> (f : NonDetSys X) -> (xs : List X) -> 
>             xs >>= NonDeterministic.flow n f = Deterministic.flow n (repr f) xs
>             
> reprLemma Z f xs = ( xs >>= NonDeterministic.flow Z f )
>                  ={ Refl }=
>                    ( xs >>= (\ x => pure x) )
>                  ={ rightIdentity xs }=  
>                    ( xs )
>                  ={ Refl }=  
>                    ( Deterministic.flow Z (repr f) xs )
>                  QED
>
> reprLemma (S m) f xs = ( xs >>= NonDeterministic.flow (S m) f )
>                      ={ Refl }=
>                        ( xs >>= (\ x => f x >>= NonDeterministic.flow m f) )
>                      ={ sym (associativity f (NonDeterministic.flow m f) xs) }=
>                        ( (xs >>= f) >>= NonDeterministic.flow m f )
>                      ={ reprLemma m f (xs >>= f) }=
>                        ( (Deterministic.flow m (repr f)) (xs >>= f) )
>                      ={ Refl }=
>                        ( (Deterministic.flow m (repr f)) ((repr f) xs) )
>                      ={ Refl }=
>                        ( Deterministic.flow (S m) (repr f) xs )
>                      QED


Trajectories
============

For a dynamical system |f : NonDetSys X|, the trajectories of length |n :
Nat| starting at |x : X| are

> ||| The trajectories
> trj : {X : Type} -> (n : Nat) -> NonDetSys X -> X -> List (Vect (S n) X)
> trj  Z    f x = map (x ::) (pure Nil)
> trj (S n) f x = map (x ::) ((f x) >>= trj n f) 

> ||| map last (map (x ::) xss) = map last xss
> mapLastLemma : {X : Type} -> {n : Nat} -> 
>                (x : X) -> (xss : List (Vect (S n) X)) -> map last (map (x ::) xss) = map Data.Vect.last xss
> mapLastLemma x  Nil               = Refl                
> mapLastLemma x ((y :: ys) :: yss) = ( map last (map (x ::) ((y :: ys) :: yss)) )
>                                   ={ Refl }=
>                                     ( map last ((x :: (y :: ys)) :: (map (x ::) yss)) )
>                                   ={ Refl }=
>                                     ( last (x :: (y :: ys)) :: (map last (map (x ::) yss)) )
>                                   ={ Refl }=
>                                     ( last (y :: ys) :: (map last (map (x ::) yss)) )  
>                                   ={ cong (mapLastLemma x yss) }=
>                                     ( last (y :: ys) :: map last yss )    
>                                   ={ Refl }=
>                                     ( map last ((y :: ys) :: yss) )
>                                   QED

> mutual


>   ||| Flow and trajectory tips
>   flowTrjLemma : {X : Type} -> 
>                  (n : Nat) -> (f : NonDetSys X) -> (x : X) -> 
>                  flow n f x = map last (trj n f x)
>                
>   flowTrjLemma Z f x = ( flow Z f x )
>                      ={ Refl }=
>                        ( pure x )
>                      ={ Refl }=
>                        ( map last (map (x ::) (pure Nil)) )
>                      ={ Refl }=
>                        ( map last (trj Z f x) )
>                      QED
>                     
>   flowTrjLemma (S m) f x = ( flow (S m) f x )
>                          ={ Refl }=
>                            ( f x >>= flow m f )
>                          ={ flowTrjLemma' m f (f x) }=
>                            ( f x >>= map last . (trj m f) )
>                          ={ sym (mapBindLemma last (trj m f) (f x)) }=
>                            ( map last ((f x) >>= trj m f) )
>                          ={ sym (mapLastLemma x ((f x) >>= trj m f)) }=
>                            ( map last (map (x ::) ((f x) >>= trj m f)) )
>                          ={ Refl }=
>                            ( map last (trj (S m) f x) )
>                          QED

We can avoid postulating extensional equality of functions with an
auxiliary lemma. Notice that |flowTrjLemma| and |flowTrjLemma'| are
mutually dependent:

>   flowTrjLemma' : {X : Type} -> 
>                   (n : Nat) -> (f : NonDetSys X) -> (xs : List X) -> 
>                   xs >>= flow n f = xs >>= map last . (trj n f)

>   flowTrjLemma' n f  Nil      = Refl
>   flowTrjLemma' n f (x :: xs) = let g = flow n f in
>                                 let h = map last . (trj n f) in
>                                 ( (x :: xs) >>= flow n f )
>                               ={ Refl }=
>                                  ( (x :: xs) >>= g )
>                               ={ bindJoinMapSpec g (x :: xs) }=
>                                  ( join (map g (x :: xs)) )
>                               ={ Refl }=
>                                  ( g x ++ join (map g xs) )
>                               ={ cong (sym (bindJoinMapSpec g xs)) }=
>                                  ( g x ++ (xs >>= g) )
>                               ={ cong (flowTrjLemma' n f xs) }=
>                                  ( g x ++ (xs >>= h) )  
>                               ={ cong {f = \ X => X ++ (xs >>= h)} (flowTrjLemma n f x) }=
>                                  ( h x ++ (xs >>= h) )
>                               ={ cong (bindJoinMapSpec h xs) }=
>                                  ( h x ++ join (map h xs) )
>                               ={ Refl }=
>                                  ( join (map h (x :: xs)) )
>                               ={ sym (bindJoinMapSpec h (x :: xs)) }=
>                                 ( (x :: xs) >>= h )
>                               ={ Refl }=
>                                 ( (x :: xs) >>= map last . (trj n f) )
>                               QED


> ||| Flow and trajectories tips
> flowTrjsLemma : {X : Type} -> 
>                 (n : Nat) -> (f : NonDetSys X) -> (xs : List X) -> 
>                 xs >>= flow n f = map last (xs >>= trj n f)
>
> flowTrjsLemma n f xs = ( xs >>= flow n f )
>                      ={ flowTrjLemma' n f xs }=
>                        ( xs >>= map last . (trj n f) )
>                      ={ sym (mapBindLemma last (trj n f) xs) }=
>                        ( map last (xs >>= trj n f) )
>                      QED


> {-

Old formulation
===============

The flow of a non-deterministic system |f : NonDetSys X| over |t| steps
is a deterministic system on |List X|:

> flow : {X : Type} -> Nat -> NonDetSys X -> DetSys (List X)
> flow  Z    f xs = xs
> flow (S n) f xs = flow n f (join (map f xs)) 

The following two lemmas show that

  1) |flow 0 f = id|

  2) |flow (t + t') f = flow t' f . flow t f| 

hold as in the deterministic case:

> flowLemma1 : {X : Type} -> 
>              (f : NonDetSys X) -> (xs : List X) -> flow Z f xs = xs 
> flowLemma1 f xs = Refl

> flowLemma2 : {X : Type} -> {m, n : Nat} ->
>              (f : NonDetSys X) -> (xs : List X) -> flow (m + n) f xs = flow n f (flow m f xs) 
> flowLemma2 {m = Z} {n} f xs = ( flow (Z + n) f xs )
>                             ={ Refl }=
>                               ( flow n f xs )
>                             ={ Refl }=
>                               ( flow n f (flow Z f xs) )
>                             QED
> flowLemma2 {m = S l} {n} f xs = ( flow (S l + n) f xs )
>                               ={ Refl }=
>                                 ( flow (S (l + n)) f xs )
>                               ={ Refl }=
>                                 ( flow (l + n) f (join (map f xs)) )
>                               ={ flowLemma2 f (join (map f xs)) }=
>                                 ( flow n f (flow l f (join (map f xs))) )
>                               ={ Refl }=
>                                 ( flow n f (flow (S l) f xs) )
>                               QED

Every non-deterministic system |f : NonDetSys X| can be represented by
an equivalent deterministic systems on |List X|

> repr : {X : Type} -> NonDetSys X -> DetSys (List X)
> repr f = NonDeterministic.flow (S Z) f 

in the sense that 

> reprLemma : {X : Type} -> (n : Nat) -> (f : NonDetSys X) -> (xs : List X) -> 
>             NonDeterministic.flow n f xs = Deterministic.flow n (repr f) xs
> reprLemma Z f xs = ( NonDeterministic.flow Z f xs )
>                  ={ Refl }=
>                    ( xs )
>                  ={ Refl }=  
>                    ( Deterministic.flow Z (repr f) xs )
>                  QED
> reprLemma (S m) f xs = ( NonDeterministic.flow (S m) f xs )
>                      ={ Refl }=
>                        ( (NonDeterministic.flow m f) (join (map f xs)) )
>                      ={ Refl }=
>                        ( (NonDeterministic.flow m f) (NonDeterministic.flow (S Z) f xs) )  
>                      ={ reprLemma m f (NonDeterministic.flow (S Z) f xs) }=
>                        ( (Deterministic.flow m (repr f)) (NonDeterministic.flow (S Z) f xs) )  
>                      ={ Refl }=
>                        ( (Deterministic.flow m (repr f)) ((repr f) xs) )
>                      ={ Refl }=
>                        ( Deterministic.flow (S m) (repr f) xs )
>                      QED


* Trajectories

\TODO{Do we need as a first step the trajectories starting from a given
initial state?}

> trjs : {X : Type} -> (n : Nat) -> NonDetSys X -> X -> List (Vect (S n) X)
> trjs  Z    f  x = pure (x :: Nil) 
> trjs (S n) f  x = let xs' = f x in
>                   map (x ::) (join (map (\ x' => trjs n f x') xs'))

For a dynamical system |f : NonDetSys X|, the trajectories of length |n
: Nat| starting at |xs : List X| are:

\TODO{|trj| or |trjs|?}

> trj : {X : Type} -> (n : Nat) -> NonDetSys X -> List X -> List (Vect (S n) X)
> trj  Z    f  xs = map (:: Nil) xs 
> trj (S n) f  xs = join (map (\ x => map (x ::) (trj n f (f x))) xs)

> trjLemma : {X : Type} -> 
>            (n : Nat) -> (f : NonDetSys X) -> (xs : List X) -> 
>            trj n f (join (map f xs)) = join (map (\ x => trj n f (f x)) xs) 
> trjLemma Z f xs = ( trj Z f (join (map f xs)) )
>                 ={ Refl }=
>                   ( map (:: Nil) (join (map f xs)) )
>                 ={ mapJoinLemma {H = Vect (S Z)} (:: Nil) f xs }=
>                   ( join (map (map (:: Nil) . f) xs) )
>                 ={ Refl }=
>                   ( join (map (\ x => (map (:: Nil) (f x))) xs) )
>                 ={ Refl }=
>                   ( join (map (\ x => trj Z f (f x)) xs) )
>                 QED
> trjLemma {X} (S m) f xs = let g : (X -> List (Vect (S (S m)) X))
>                                 = (\ x => map (x ::) (trj m f (f x))) in
>                           let h = map g in
>                           ( trj (S m) f (join (map f xs)) )
>                         ={ Refl }=
>                           ( join (map (\ x => map (x ::) (trj m f (f x))) (join (map f xs))) )
>                         ={ Refl }=
>                           ( join (map g (join (map f xs))) )
>                         ={ cong (mapJoinLemma {H = List . (Vect (S (S m)))} g f xs) }=
>                           ( join (join (map (map g . f) xs)) )
>                         ={ Refl }=
>                           ( join (join (map (h . f) xs)) )
>                         ={ sym (square (map (h . f) xs)) }=
>                           ( join (map join (map (h . f) xs)) )
>                         ={ Refl }=
>                           ( join ((map join . map (h . f)) xs) )
>                         ={ cong (sym (mapPreservesComp xs (h . f) join)) }=
>                           ( join (map (join . (h . f)) xs) )
>                         ={ Refl }=
>                           ( join (map (\ y => join ((h . f) y)) xs) )
>                         ={ Refl }=
>                           ( join (map (\ y => join ((map g . f) y)) xs) )
>                         ={ Refl }=
>                           ( join (map (\ y => join (map g (f y))) xs) )
>                         ={ Refl }=
>                           ( join (map (\ y => join (map (\ x => map (x ::) (trj m f (f x))) (f y))) xs) )
>                         ={ Refl }=
>                           ( join (map (\ x => trj (S m) f (f x)) xs) )
>                         QED

The last elements of the trajectories of length |n| of |f| starting in
|xs| are the elements of |flow n f x|:

> mapLastMapConsNilLemma : {X : Type} -> (xs : List X) -> map last (map (:: Nil) xs) = xs
> mapLastMapConsNilLemma Nil = Refl
> mapLastMapConsNilLemma (x :: xs) = ( map last (map (:: Nil) (x :: xs)) )
>                                  ={ Refl }=
>                                    ( map last ((x :: Nil) :: (map (:: Nil) xs)) )
>                                  ={ Refl }=
>                                    ( x :: map last (map (:: Nil) xs) )
>                                  ={ cong (mapLastMapConsNilLemma xs) }=
>                                    ( x :: xs )
>                                  QED 

> mapLastJoinMapMapConsLemma : {X : Type} ->
>                              (n : Nat) -> (f : NonDetSys X) -> (xs : List X) -> 
>                              map Data.Vect.last (join (map (\ x => map (x ::) (trj n f (f x))) xs))
>                              =
>                              map Data.Vect.last (join (map (\ x => trj n f (f x)) xs))
>                              
> mapLastJoinMapMapConsLemma  Z    f xs = ( map Data.Vect.last (join (map (\ x => map (x ::) (trj Z f (f x))) xs)) )
>                                       ={ Refl }=
>                                         ( map Data.Vect.last (join (map (\ x => map (x ::) (map (:: Nil) (f x))) xs)) )
>                                       ={ ?kuka }=
>                                         ( map Data.Vect.last (join (map (\ x => map (:: Nil) (f x)) xs)) )
>                                       ={ Refl }=
>                                         ( map Data.Vect.last (join (map (\ x => trj Z f (f x)) xs)) )
>                                       QED
>                                       
> mapLastJoinMapMapConsLemma (S m) f xs = ?kika

< trj  Z    f  xs = map (:: Nil) xs 

> flowTrjLemma : {X : Type} -> 
>                (n : Nat) -> (f : NonDetSys X) -> (xs : List X) -> 
>                flow n f xs = map last (trj n f xs)
> flowTrjLemma Z f xs = ( flow Z f xs )
>                     ={ Refl }=
>                       ( xs )
>                     ={ sym (mapLastMapConsNilLemma xs) }=
>                       ( map last (map (\ x => x :: Nil) xs) )
>                     ={ Refl }=
>                       ( map last (trj Z f xs) )
>                     QED
> flowTrjLemma (S m) f xs = ( flow (S m) f xs )
>                         ={ Refl }=
>                           ( flow m f (join (map f xs)) )
>                         ={ flowTrjLemma m f (join (map f xs)) }=
>                           ( map last (trj m f (join (map f xs))) )
>                         ={ cong (trjLemma m f xs) }=
>                           ( map last (join (map (\ x => trj m f (f x)) xs)) )
>                         ={ sym (mapLastJoinMapMapConsLemma m f xs) }=
>                           ( map last (join (map (\ x => map (x ::) (trj m f (f x))) xs)) )
>                         ={ Refl }=
>                           ( map last (trj (S m) f xs) )
>                         QED

> ---}


 
