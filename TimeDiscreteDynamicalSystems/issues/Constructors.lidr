> import Syntax.PreorderReasoning
> import Interfaces.Verified

> %default total
> %auto_implicits off

> flow : {M : Type -> Type} -> {X : Type} -> Monad M => Nat -> (X -> M X) -> (X -> M X)
> flow  Z    f x = pure x
> flow (S n) f x = f x >>= flow n f

> flowLemma2 : {M : Type -> Type} -> {X : Type} -> {m, n : Nat} -> VerifiedMonad M => 
>              (f : X -> M X) -> (x : X) -> flow (m + n) f x = flow m f x >>= flow n f
> flowLemma2 {m = Z} {n} f x = ( flow (Z + n) f x )
>                            ={ Refl }=
>                              ( flow n f x )
>                            ={ ?lala }=
>                              ( pure x >>= flow n f )
>                            ={ Refl }=
>                              ( flow Z f x >>= flow n f )
>                            QED
> flowLemma2 {m = S l} {n} f x = ?kika



 
