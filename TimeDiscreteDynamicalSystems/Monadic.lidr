> module TimeDiscreteDynamicalSystems.Monadic

> import Data.List
> import Data.Vect
> import Syntax.PreorderReasoning

> import Fun.Predicates
> import VeriFunctor.VeriFunctor
> -- import VeriApplicative.VeriApplicative
> import VeriMonad.VeriMonad
> import Vect.Properties

> import TimeDiscreteDynamicalSystems.Deterministic

> %default total
> %access public export
> %auto_implicits off


Preliminaries
=============

A discrete monadic dynamical system on a set |X| is a function
of type |X -> M X| where |M| is a monad:

> MonadicSys : (M : Type -> Type) -> Type -> Type
> MonadicSys M X = X -> M X

The set |X| in |f : MonadicSys M X| is often called the "state space" of the
system |f|:

> StateSpace : {M : Type -> Type} -> {X : Type} -> MonadicSys M X -> Type
> StateSpace = Domain

Every deterministic system on |X| can be represented by a monadic
systems on |X|:

> embed : {M : Type -> Type} -> {X : Type} -> Monad M => DetSys X -> MonadicSys M X
> embed f = pure . f


Flow
====

The flow of a monadic system |f| over |t| steps is another monadic
system:

> flow : {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
>        Nat -> MonadicSys M X -> MonadicSys M X
> flow  Z    f x = pure x
> flow (S n) f x = f x >>= flow n f

Trivially, one has |flow Z f = pure|:

> ||| flow Z f x = pure x
> flowLemma1 : {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
>              (f : MonadicSys M X) -> ExtEq (flow Z f) pure
> flowLemma1 f x = Refl

and also |flow (t + t') f x = flow t f x >>= flow t' f|:

> ||| flow (t + t') f x = flow t f x >>= flow t' f
> flowLemma2 : {M : Type -> Type} -> {X : Type} -> {m, n : Nat} -> VeriMonad M => 
>              (f : MonadicSys M X) -> ExtEq (flow (m + n) f) (\ x => flow m f x >>= flow n f)
>
> flowLemma2 {m = Z} {n} f x = ( flow (Z + n) f x )
>                            ={ Refl }=
>                              ( flow n f x )
>                            ={ sym (leftIdentity (flow n f) x) }=
>                              ( pure x >>= flow n f )
>                            ={ Refl }=
>                              ( flow Z f x >>= flow n f )
>                            QED

> flowLemma2 {m = S l} {n} f x = ( flow (S l + n) f x )
>                              ={ Refl }=
>                                ( f x >>= flow (l + n) f )
>                              ={ bindPresExtEq (flow (l + n) f) (\ y => flow l f y >>= flow n f) (flowLemma2 f) (f x) }=
>                                ( f x >>= (\ y => flow l f y >>= flow n f) )
>                              ={ sym (associativity (flow l f) (flow n f) (f x)) }=
>                                ( (f x >>= flow l f) >>= flow n f )
>                              ={ Refl }=
>                                ( flow (S l) f x >>= flow n f )
>                              QED


Every monadic system |f : MonadicSys M X| can be represented by an
equivalent deterministic systems on |M X|

> |||
> repr : {M : Type -> Type} -> {X : Type} -> VeriMonad M => MonadicSys M X -> DetSys (M X)
> repr f xs = xs >>= f


> ||| 
> reprLemma : {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
>             (n : Nat) -> (f : MonadicSys M X) -> 
>             ExtEq {A = M X} (>>= Monadic.flow n f) (Deterministic.flow n (repr f))
>                          
> reprLemma Z f mx = ( mx >>= Monadic.flow Z f )
>                  ={ bindPresExtEq (Monadic.flow Z f) pure (flowLemma1 f) mx }=
>                    ( mx >>= pure )
>                  ={ rightIdentity mx }=  
>                    ( mx )
>                  ={ Refl }=  
>                    ( Deterministic.flow Z (repr f) mx )
>                  QED
>
> reprLemma (S m) f xs = ( xs >>= Monadic.flow (S m) f )
>                      ={ bindPresExtEq (Monadic.flow (S m) f) (\ x => f x >>= Monadic.flow m f) (\ x => Refl) xs }=
>                        ( xs >>= (\ x => f x >>= Monadic.flow m f) )
>                      ={ sym (associativity f (Monadic.flow m f) xs) }=
>                        ( (xs >>= f) >>= Monadic.flow m f )
>                      ={ reprLemma m f (xs >>= f) }=
>                        ( (Deterministic.flow m (repr f)) (xs >>= f) )
>                      ={ Refl }=
>                        ( (Deterministic.flow m (repr f)) ((repr f) xs) )
>                      ={ Refl }=
>                        ( Deterministic.flow (S m) (repr f) xs )
>                      QED


Trajectories
============

For a dynamical system |f : MonadicSys M X|, the trajectories of length |n :
Nat| starting at |x : X| are

> ||| The trajectories
> trj : {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
>       (n : Nat) -> MonadicSys M X -> X -> M (Vect (S n) X)
> trj  Z    f x = map (x ::) (pure Nil)
> trj (S n) f x = map (x ::) ((f x) >>= trj n f) 

> %hide Prelude.List.last

> ||| map last (map (x ::) xss) = map last xss
> mapLastLemma : {M : Type -> Type} -> {X : Type} -> {n : Nat} -> VeriMonad M => 
>                (x : X) -> ExtEq {A = M (Vect (S n) X)} (map last . map (x ::)) (map last)
>                
> mapLastLemma {X} {n} x mvs = ( map last (map (x ::) mvs) )
>                    ={ sym (mapPresComp {A = Vect (S n) X} (x ::) last mvs) }=
>                      ( map (last . (x ::)) mvs )
>                    ={ mapPresExtEq (last . (x ::)) last (lastLemma x) mvs }=
>                      ( map last mvs )
>                    QED



> ||| Flow and trajectory tips
> flowTrjLemma : {M : Type -> Type} -> {X : Type} -> VeriMonad M => 
>                (n : Nat) -> (f : MonadicSys M X) -> 
>                ExtEq (flow n f) (map {a = Vect (S n) X} last . (trj n f))

> flowTrjLemma {X} Z f x = ( flow Z f x )
>                    ={ Refl }=
>                      ( pure x )
>                    ={ Refl }=
>                      ( pure (last (x :: Nil)) )
>                    ={ sym (pureNatTrans last (x :: Nil)) }=
>                      ( map last (pure (x :: Nil)) )
>                    ={ cong {f = map last} (sym (pureNatTrans {A = Vect Z X} (x ::) Nil)) }=
>                      ( map last (map (x ::) (pure Nil)) )
>                    ={ Refl }=
>                      ( map last (trj Z f x) )
>                    QED
>
> flowTrjLemma (S m) f x = ( flow (S m) f x )
>                        ={ Refl }=
>                          ( f x >>= flow m f )
>                        ={ bindPresExtEq (flow m f) (map last . (trj m f)) (Monadic.flowTrjLemma m f) (f x) }=
>                          ( f x >>= map last . (trj m f) )
>                        ={ sym (mapBindLemma last (trj m f) (f x)) }=
>                          ( map last ((f x) >>= trj m f) )
>                        ={ sym (mapLastLemma x ((f x) >>= trj m f)) }=
>                          ( map last (map (x ::) ((f x) >>= trj m f)) )
>                        ={ Refl }=
>                          ( map last (trj (S m) f x) )
>                        QED

> {-

> ---}


 
