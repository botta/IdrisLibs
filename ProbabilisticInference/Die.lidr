> module ProbabilisticInference.Die

> import Data.Fin
> import Data.List
> import Control.Isomorphism

> import Basic.Predicates
> import Basic.Properties
> import Finite.Predicates
> import Finite.Properties
> import Sigma.Sigma
> import List.Predicates
> import List.Properties

> %default total
> %access public export
> %auto_implicits off


> data Die = One | Two | Three | Four | Five | Six


> implementation Eq Die where
>   (==)   One   One  =   True
>   (==)   One     _  =  False
>   (==)   Two   Two  =   True
>   (==)   Two     _  =  False
>   (==) Three Three  =   True
>   (==) Three     _  =  False
>   (==)  Four  Four  =   True
>   (==)  Four     _  =  False
>   (==)  Five  Five  =   True
>   (==)  Five     _  =  False
>   (==)   Six   Six  =   True
>   (==)   Six     _  =  False


> implementation Show Die where
>   show   One = "1"
>   show   Two = "2"
>   show Three = "3"
>   show  Four = "4"
>   show  Five = "5"
>   show   Six = "6"


> finiteDie : Finite Die
> finiteDie = MkSigma 6 (MkIso to from toFrom fromTo) where
>   to : Die -> Fin 6
>   to   One =                    FZ
>   to   Two =                 FS FZ
>   to Three =             FS (FS FZ)
>   to  Four =         FS (FS (FS FZ))
>   to  Five =     FS (FS (FS (FS FZ)))
>   to   Six = FS (FS (FS (FS (FS FZ))))
>   from : Fin 6 -> Die
>   from                     FZ      =   One
>   from                 (FS FZ)     =   Two
>   from             (FS (FS FZ))    = Three
>   from         (FS (FS (FS FZ)))   =  Four
>   from     (FS (FS (FS (FS FZ))))  =  Five
>   from (FS (FS (FS (FS (FS FZ))))) =   Six
>   toFrom : (k : Fin 6) -> to (from k) = k
>   toFrom                     FZ      = Refl
>   toFrom                 (FS FZ)     = Refl
>   toFrom             (FS (FS FZ))    = Refl
>   toFrom         (FS (FS (FS FZ)))   = Refl
>   toFrom     (FS (FS (FS (FS FZ))))  = Refl
>   toFrom (FS (FS (FS (FS (FS FZ))))) = Refl
>   fromTo : (d : Die) -> from (to d) = d
>   fromTo   One = Refl
>   fromTo   Two = Refl
>   fromTo Three = Refl
>   fromTo  Four = Refl
>   fromTo  Five = Refl
>   fromTo   Six = Refl


> notEmptyDie : Not (Empty Die)
> notEmptyDie = \ contra => contra One


Useful constants:

> finiteNotEmptyDie : (Finite Die, Not (Empty Die))
> finiteNotEmptyDie = (finiteDie, notEmptyDie)


> Die2 : Type
> Die2 = (Die, Die)

> finiteDie2 : Finite Die2
> finiteDie2 = finitePair finiteDie finiteDie

> notEmptyDie2 : Not (Empty Die2)
> notEmptyDie2 = notEmptyPair notEmptyDie notEmptyDie

> finiteNotEmptyDie2 : (Finite Die2, Not (Empty Die2))
> finiteNotEmptyDie2 = (finiteDie2, notEmptyDie2)


> hasNoDuplicatesThree : HasNoDuplicates [Three]
> hasNoDuplicatesThree = singletonHasNoDuplicates Three

> hasNoDuplicatesFour : HasNoDuplicates [Four]
> hasNoDuplicatesFour = singletonHasNoDuplicates Four

> hasNoDuplicatesSix : HasNoDuplicates [Six]
> hasNoDuplicatesSix = singletonHasNoDuplicates Six

> nElemTwoSix : Not (Elem Two [Six])
> nElemTwoSix (There later) impossible

> nElemFourSix : Not (Elem Four [Six])
> nElemFourSix (There later) impossible

> hasNoDuplicatesFourSix : HasNoDuplicates [Four, Six]
> hasNoDuplicatesFourSix = ConsHasNoDuplicates Four [Six] nElemFourSix hasNoDuplicatesSix

> nElemTwoFourSix : Not (Elem Two [Four, Six])
> nElemTwoFourSix (There later) = nElemTwoSix later

> hasNoDuplicatesTwoFourSix : HasNoDuplicates [Two, Four, Six]
> hasNoDuplicatesTwoFourSix = ConsHasNoDuplicates Two [Four, Six] nElemTwoFourSix hasNoDuplicatesFourSix


