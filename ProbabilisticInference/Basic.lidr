> module ProbabilisticInference.Basic

> import Data.Vect
> import Data.Vect.Quantifiers
> import Control.Isomorphism

> import Basic.Predicates
> import Basic.Properties
> import HVect.HVect
> import HVect.Operations
> import HVect.Properties
> import Finite.Predicates
> import Finite.Operations
> import Finite.Properties
> import Sigma.Sigma
> import Sigma.Properties
> import Sigma.Operations
> import Real.Real
> import Double.Predicates
> import Double.Postulates
> import Double.Operations
> import Double.Properties
> import Bool.Properties
> import Fin.Properties
> import Maybe.Properties
> import Num.Implementations
> import List.Predicates
> import List.Properties
> import Vect.Properties
> import Nat.Operations
> import Set.SetAsList
> import Set.Operations

> import ProbabilisticInference.FiniteNotEmpty
> import ProbabilisticInference.Die

> %default total
> %access public export
> %auto_implicits off


* Preliminaries
===============

In elementary probability, the possible outcomes of a random process are
represented by a finite, non empty set and probabilities are defined on
subsets of this set. These subsets are called \emph{events}.

In Idris, we do not have sets (and thus subsets) but we can represent
outcomes as finite, non-empty types, see FiniteNotEmpty.lidr

> Outcome : Type
> Outcome = FiniteNotEmptyType

We will make use of functions that return the type, the correspondent
finiteness and non-emptiness proofs, the cardinality and all the values
of an arbitrary finite, non-empty type. These are defined in
FiniteNotEmpty.lidr and have the following types

< type      : (O : Outcome) -> Type
< finite    : (O : Outcome) -> Finite (type O)
< notEmpty  : (O : Outcome) -> Not (Empty (type O))
< card      : (O : Outcome) -> Nat
< all       : (O : Outcome) -> List (type O)

With these premisses, we can represent events as lists of outcomes with
no duplicates

> Event : Outcome -> Type
> Event O = Sigma (List (type O)) HasNoDuplicates

Alternatively, we could represents events as Boolean functions

< Event : Outcome -> Type
< Event O = type O -> Bool

This is perhaps more appealing: with this definition, we could express
events like "the outcome of rolling two dice is 7" very naturally. As it
turns out, however, representing events as Boolean functions would be
computationally inefficient.


* Probability measures
======================

In the classical model of probability, the probability of an event |e :
Event O| is simply the number of elements in |e| (the length of the
first component of |e|) divided by the total number of possible
outcomes. This is |card O|, the number of values of type |type O|:

> prob : {O : Outcome} -> Event O -> Double
> prob {O} (MkSigma xs _) = (length xs) / (card O)

Notice that |(length os) / (card O)| can also be computed by assigning
the probability |1 / (card O)| to each possible outcome and adding up
the probabilities of the outcomes in the event.  We can make this
interpretation more explicit by rewriting |prob| as:

< prob {O} (MkSigma xs _) = sum [uniform x | x <- xs] where
<   uniform : type O -> Double
<   uniform x = 1 / (card O)  

In this implementation, |uniform| is a function that assignes to every
outcome (a value of type |type O|) the probability |1 / (card 0)|. In
probability theory, functions of type |type O -> Double| are called
\emph{probability distributions}.

Example: 

In the file Die.lidr, a die is defined by the data type

< data Die = One | Two | Three | Four | Five | Six

|Die| is obviously a finite, non-empty type as witnessed by
|finiteNotEmptyDie| also implemented in Die.lidr. We can model the
outcomes of rolling the die by

> Roll : Outcome
> Roll = MkSigma Die finiteNotEmptyDie

and consider the event in which the possible outcomes are even:

> even : Event Roll
> even = MkSigma [Two, Four, Six] hasNoDuplicatesTwoFourSix

The constant |hasNoDuplicatesTwoFourSix| is also defined in
Die.lidr. Using the classical model of probability, the probability of
|e| is the number of elements of |e| divided by the number of element of
|Die|. This is equal to 3/6 = 1/2.

> p_even : Double
> p_even = prob {O = Roll} even -- check p_even = 1/2

We can apply the same idea to model the outcome of rolling two
dice. Here the possible outcomes are pairs of type |Die2 = (Die, Die)|,
the first element representig the first die and the second element the
one of the second die.

< Roll2 : Outcome
< Roll2 = MkSigma Die2 finiteNotEmptyDie2

If we consider the event in which the first die turns even and the
second one five, we obtain

< postulate hndEvenFive : HasNoDuplicates [(Two, Five), (Four, Five), (Six, Five)]

< evenFive : Event Roll2
< evenFive = MkSigma [(Two, Five), (Four, Five), (Six, Five)] hndEvenFive

< p_evenFive : Double
< p_evenFive = prob evenFive 
< -- check p_evenFive = 3/36 = 1/12 


* Outcomes as Cartesian products of finite, non-empty types
===========================================================

In the last example, |Roll2| was the product |(Die, Die)| paired with
proofs that |(Die, Die)| is finite and non-empty.

We have obtained such proofs by applying |finitePair| and |notEmptyPair|
to the proofs |finiteDie| and |notEmptyDie| that we had already used to
define |Roll|.

The combinators |finitePair| and |notEmptyPair| are implemented in
'Finite/Properties.lidr' and in 'Basic/Properties.lidr',
respectively. Their types are

< finitePair : Finite A -> Finite B -> Finite (A, B)

and

< notEmptyPair : Not (Empty A) -> Not (Empty B) -> Not (Empty (A, B))

In many applications, the set of possible outcomes is the Cartesian
product of |n| finite, non-empty sets: |O_1 x O_2 x ... x O_n|.

In Idris, we can represent such products with tuples or with
heterogeneos vectors, see 'HVect/HVect.lidr'. The second approach is
slightly more generic.

In defining outcomes as products of finite, non-empty types, we can take
advantage of results like

< finiteLemma : (ts : Vect n Type) -> All Finite ts -> Finite (HVect n ts)

and 

< notEmptyLemma : (ts : Vect n Type) -> All (Not . Empty) ts -> Not (Empty (HVect n ts))

As an example, we re-implement the case of rolling two dice with
heterogeneous vectors

> Die2 : Vect 2 Type
> Die2 = [Die, Die]

> Roll2 : Outcome
> Roll2 = MkSigma (HVect 2 Die2) (finiteLemma Die2 [finiteDie, finiteDie], 
>                                 notEmptyLemma Die2 [notEmptyDie, notEmptyDie])

> postulate hndEvenFive : HasNoDuplicates {A = HVect 2 Die2} [[Two, Five], [Four, Five], [Six, Five]]

> evenFive : Event Roll2
> evenFive = MkSigma [[Two, Five], [Four, Five], [Six, Five]] hndEvenFive

> p_evenFive : Double
> p_evenFive = prob evenFive 
> -- check p_evenFive = 3/36 = 1/12 


* Probability distributions
===========================

The type of |uniform| in

< prob {O} (MkSigma os _) = sum [uniform x | x <- xs] where
<   uniform : type O -> Double
<   uniform x = 1 / (card O)  

is that of a \emph{probability distribution}. For a value of type |type
O -> Double| (for |O : Outcome|) to represent a valid probability
distribution, the following conditions have to be satisfied

> ||| For all |x|, |p x >= 0|
> isPD1 : {O : Outcome} -> (type O -> Double) -> Type
> isPD1 {O} pd = (x : type O) -> LTE 0 (pd x)

> ||| Sum 1 condition
> isPD2 : {O : Outcome} -> (type O -> Double) -> Type
> isPD2 {O} pd = sum [pd x | x <- all O] = 1

As we already mentined, the probability distribution that associates to
every outcome the same probability is called the \emph{uniform}
distribution:

> uniform : {O : Outcome} -> type O -> Double
> uniform {O} x = 1 / (card O)          

It encodes the classical model of probability and maximises Shannon's
information entropy

> h : {O : Outcome} -> (type O -> Double) -> Double
> h {O} pd = - sum [pd x * log (pd x) | x <- all O]

The uniform distribution is the canonical choice whenever we have no
reason to prefer one elementary outcome over any other.

We can generalize the classical model of probability by implementing a
function that computes the probability of events according to a given
probability distribution

> p : {O : Outcome} -> (type O -> Double) -> Event O -> Double
> p {O} pd (MkSigma xs _) = sum [pd x | x <- xs]

In this generalization, the classical model is obtained by taking |pd|
to be the uniform probability distribution

  p uniform = prob


* Marginal probabilities
========================

A probability distribution contains all the information needed to
compute the probability of every event.

As discussed above, in many applications the set of possible outcomes is
the Cartesian product of |n : Nat, 0 < n| finite, non-empty sets.

In these cases, probability distributions are called \emph{joint}
probabilitry distributions.

We are often interested in the probability of events in which one or
more sub-events are singleton outcomes and the other events consist of
all possible outcomes.

The probabilities associated to these special events are traditionally
called \emph{marginal}. 

Historically, this comes from the fact that a joint probability
distribution for |[type O1, type O2]| (|n = 2|) can be represented as a
table of real number |jpt : n1 x n2 -> Double| where |n1| and |n2| are
the number of possible outcomes in |O1| and |O2|, respectively. On such
a table, the probabilities of the events

  [[x, o2] | o2 <- all O2] 

  [[o1, y] | o1 <- all O1] 

for arbitrary |x : type O1| and |y : type O2| can be obtained by adding
the values of the corresponding row and colum "on the margins" of the
table.

It is easy to implement a function that takes an heterogeneous vector of
|Maybe| types corresponding to the types of a vector of outcomes and
computes a marginal event.

To this end, it is useful to implement the following \emph{deduction
schemes}:

> types : {n : Nat} -> (os : Vect n Outcome) -> Vect n Type
> types = map type

> allFiniteTypes : {n : Nat} -> (os : Vect n Outcome) -> All Finite (types os)
> allFiniteTypes  Nil      = Nil
> allFiniteTypes (o :: os) = finite o :: allFiniteTypes os

> allNotEmptyTypes : {n : Nat} -> (os : Vect n Outcome) -> All (Not . Empty) (types os)
> allNotEmptyTypes  Nil      = Nil
> allNotEmptyTypes (o :: os) = notEmpty o :: allNotEmptyTypes os

and a function that takes an heterogeneous vector of the |Maybe| types
corresponding to a vector of outcomes |os| (a so-called
\emph{descriptor}) and computes an heterogeneous vector of lists of
those types:

> allLists : {n : Nat} -> (os : Vect n Outcome) -> HVect n (map Maybe (types os)) -> HVect n (map List (types os))
> allLists  Nil       _               = Nil
> allLists (o :: os) (Nothing :: mts) = toList (finite o) :: allLists os mts
> allLists (o :: os) (Just x  :: mts) =               [x] :: allLists os mts

The idea is that |allLists| embeds a |Just x| in a singleton lists |[x]|
and uses the finiteness of the types in |os| to generate all the values
of the corresponding type for a |Nothings|.

With these helpers in place, a |marginal| function can be implemented as 

> marginal : {n : Nat} -> (os : Vect n Outcome) -> HVect n (map Maybe (types os)) -> 
>            Event 
>            (MkSigma (HVect n (types os)) 
>             (finiteLemma (types os) (allFiniteTypes os), notEmptyLemma (types os) (allNotEmptyTypes os)))
> marginal {n} os mts = MkSigma xs hndxs where
>   xs : List (HVect n (map type os))
>   xs = vectProd (allLists os mts)
>   postulate hndxs : HasNoDuplicates xs

The implementation of |marginal| crucially depends on |allLists| and on
the function |vectProd| from HVect/Operations.lidr. Its signature is

< vectProd : {n : Nat} -> {ts : Vect n Type} -> HVect n (map List ts) -> List (HVect n ts)

The idea is that |vectProd| takes an heterogeneous vector of lists and
computes a list of heterogeous vectors. This list contains all the
products of the elements of the input lists. Thus, for instance

  vectProd [[H,T],[1,2,3,4,5,6]] = [[H,1], [H,2], ... , [T,6]]

While the signature of |marginal| is admittedly ugly, applying the
function is actually very straightforward:

> e2' : Event Roll2
> e2' = marginal [Roll,Roll] [Nothing, Just Three] 
> -- check e2' = [[1,3], [2,3], [3,3], [4,3], [5,3], [6,3]]

> pe2' : Double
> pe2' = p uniform e2'
> -- check pe2' = 1/6


* Conjunctions, conditional probabilities
=========================================

The notion of \emph{conditional probability} builds upon the notion of
conjunction of events. 

Let |A,B : Event O|, for |O : Outcome| and |P(A)|, |P(B) > 0| denote the
probabilities of |A| and |B| for a fixed and unmentioned probability
distribution. 

A standard textbook definition for the \emph{conditional} probability of
|A| given |B| is

  P(A|B) = P(A,B) / P(B)     (1)

Sometimes we find the slightly different notation

  P(A|B) = P(A ∩ B) / P(B)   (2)

Notice the different meaning of the symbol |P| in (1) and in (2). The
second expression suggests that the two |P| on the right-hand side might
actually represent the same funtion: |A| and |B| represent subsets of
|O| and |A ∩ B| also represents a subset of |O|.

The |P| on the left hand side of both (1) and (2), however, cannot
represent the same function that is applied to |B| on the right-hand
side.

As pointed out in lecture 2 of
https://github.com/ionescu/Machine_Learning_2019, the vertical bar is
not a set-theoretical operation and |A|B| does not represent a subset of
|O|.

What (1) and (2) define, is a function of two arguments. This is called
the \emph{conditional probability} function

< cp : {O : Outcome} -> (type O -> Double) -> Event O -> Event O -> Maybe Double

Notice that the outcome of |cp| is a |Maybe Double|: the conditional
probability of |A : Event O| given |B : Event O| is only defined when
the probability of |B| is not zero. Alternatively, we could define |cp|
to be a partial function

< partial cp : {O : Outcome} -> (type O -> Double) -> Event O -> Event O -> Double

or we could restrict the domain of |cp| to events whose probability is
not zero:

< cp : {O : Outcome} -> (pd : type O -> Double) -> (A : Event O) -> (B : Event O) -> Not (p pd B = 0) -> Double

The implementation of |cp| depends on a binary operation that computes
the intersection of two events, see Set/Operations.lidr:

> cp : {O : Outcome} -> (Eq (type O)) => (type O -> Double) -> Event O -> Event O -> Maybe Double
> cp {O} pd A B = let pB = p pd B in 
>                 if pB == 0 
>                 then Nothing
>                 else Just ((p {O} pd (intersect A B)) / pB)


Example: 

> three : Event Roll
> three = MkSigma [Three] hasNoDuplicatesThree

> four : Event Roll
> four = MkSigma [Four] hasNoDuplicatesFour

> cpThreeEven : Maybe Double
> cpThreeEven = cp {O = Roll} (uniform {O = Roll}) three even

> cpFourEven : Maybe Double
> cpFourEven = cp {O = Roll} (uniform {O = Roll}) four even


* Independence, conditional independence
========================================

Two events |A, B : Event O| are called \emph{independent} if |P(A ∩ B) =
P(A) * P(B)|:

> Independent : {O : Outcome} -> (Eq (type O)) => (type O -> Double) -> Event O -> Event O -> Type
> Independent {O} pd A B = p pd AB = p pd A * p pd B where
>   AB : Event O
>   AB = intersect A B

> areIndependent : {O : Outcome} -> (Eq (type O)) => (type O -> Double) -> Event O -> Event O -> Bool
> areIndependent {O} pd A B = p pd AB == p pd A * p pd B where
>   AB : Event O
>   AB = intersect A B


Let |A, B, C : Event O| with |P(C) > 0|. |A| and |B| are called
\emph{conditionally independent} given |C| if |P(A ∩ B | C) = P(A | C) *
P(B | C)|. Using (2) we have

  P(A ∩ B | C) = P(A | C) * P(B | C)
    =
  P(A ∩ B ∩ C) / P(C) = P(A ∩ C) / P(C) * P(B ∩ C) / P(C)
    =
  P(A ∩ B ∩ C) = P(A ∩ C) * P(B ∩ C) / P(C)

This motivates the following implementation

> ConditionallyIndependent : {O : Outcome} -> (Eq (type O)) => 
>                            (pd : type O -> Double) -> 
>                            (A : Event O) -> (B : Event O) -> (C : Event O) -> 
>                            Not (p {O} pd C = 0) -> Type
> ConditionallyIndependent {O} pd A B C contra = p pd ABC = p pd AC * p pd BC / p pd C where
>   ABC : Event O
>   ABC = intersect (intersect A B) C
>   AC  : Event O
>   AC  = intersect A C
>   BC  : Event O
>   BC  = intersect B C




> {-

> ---}



> {-


Chain rule:

< P(A,B,C) = P(A|B,C) * P(B|C) * P(C) 

> A : Maybe Bool
> A = Just False

> B : Maybe (Fin 3)
> B = Just FZ

> C : Maybe (Maybe Bool)
> C = Just Nothing

> p3 : Double
> p3 = prob jpf is1 is2 [A, B, C]

> p3' : Maybe Double
> p3' = (condProb jpf is1 is2 alld [A, Nothing, Nothing] [Nothing, B, C])
>       *
>       (condProb jpf is1 is2 alld [Nothing, B, Nothing] [Nothing, Nothing, C])
>       *
>       (Just (prob jpf is1 is2 [Nothing, Nothing, C]))

We probably should make the proofs implicit and introduce some syntactic
sugar to shorten

< condProb jpf is1 is2 alld [Nothing, B, Nothing] [Nothing, Nothing, C]

to something like

< condProb jpf [*,B,*] [*,*,C]

> ---}

 
 
 
