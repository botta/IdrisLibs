> module ProbabilisticInference.BayesianNetwork

> import Data.Fin
> import Data.Vect
> import Control.Isomorphism

> import Basic.Predicates
> import Sigma.Sigma
> import Basic.Operations
> import Fin.Operations
> import Fin.Properties
> import Finite.Predicates
> import HVect.HVect
> import HVect.Operations
> import ProbabilisticInference.Basic
> import Bool.Properties

> import ProbabilisticInference.FiniteNotEmpty

> %default total
> %access public export
> %auto_implicits off


> mutual

>   ||| A Bayesianian network
>   data BayesianNetwork : (n : Nat) -> Vect n Outcome -> Type where
>     Nil   : BayesianNetwork Z Nil
>     Node  : {n   : Nat} ->
>             {rvs : Vect n Outcome} -> 
>             (bn  : BayesianNetwork n rvs) ->
>             (t   : Type) -> 
>             (ft  : Finite t) ->
>             (net : Not (Empty t)) ->
>             (isP : Fin n -> Bool) ->
>             (cpt : HVect (invImageSize isP True) (map (ntype bn) (invImageVect isP True)) -> t -> Double) ->
>             BayesianNetwork (S n) ((MkSigma t (ft, net)) :: rvs)

>   ||| The type of a network node
>   ntype : {n : Nat} -> {rvs : Vect n Outcome} -> 
>           BayesianNetwork n rvs -> Fin n -> Type
>   ntype {n = Z}    Nil                       k     impossible 
>   ntype {n = S m} (Node bn t ft net isP cpt)  FZ    = t
>   ntype {n = S m} (Node bn t ft net isP cpt) (FS k) = ntype bn k  


> ||| The chain rule
> chain : {n : Nat} -> {rvs : Vect n Outcome} -> 
>         (bn : BayesianNetwork n rvs) -> 
>         (f : (k : Fin n) -> ntype bn k) -> 
>         Double
> chain {n = Z}    Nil                       _ = 1
> chain {n = S m} (Node bn t ft net isP cpt) f = let f' = \ k => f (FS k) in
>                                                (cpt (dmap f' (invImageVect isP True)) (f FZ)) * (chain bn f')

Example:

    B    L
     \  /
      \/
      M
     /  
    G

> fB : Finite Bool
> fB = finiteBool

> neB : Not (Empty Bool)
> neB = notEmptyBool

> bn0 : BayesianNetwork 0 Nil
> bn0 = Nil

> rv : Outcome
> rv = MkSigma Bool (fB, neB)

> bn1 : BayesianNetwork 1 [rv]
> bn1 = Node bn0 Bool fB neB isP cpt where
>   isP : Fin 0 -> Bool
>   isP = \ k => absurd k
>   cpt : HVect (invImageSize isP True) (map (ntype bn0) (invImageVect isP True)) -> Bool -> Double
>   cpt _ False = 0.05
>   cpt _ True  = 0.95

> bn2 : BayesianNetwork 2 [rv, rv]
> bn2 = Node bn1 Bool fB neB isP cpt where
>   isP : Fin 1 -> Bool
>   isP = \ k => False
>   cpt : HVect (invImageSize isP True) (map (ntype bn1) (invImageVect isP True)) -> Bool -> Double
>   cpt _ False = 0.3
>   cpt _ True  = 0.7

> bn3 : BayesianNetwork 3 [rv, rv, rv]
> bn3 = Node bn2 Bool fB neB isP cpt where
>   isP : Fin 2 -> Bool
>   isP = \ k => True
>   cpt : HVect (invImageSize isP True) (map (ntype bn2) (invImageVect isP True)) -> Bool -> Double
>   cpt [False, False] False = 1.0
>   cpt [False, False]  True = 0.0
>   cpt [False,  True] False = 1.0
>   cpt [False,  True]  True = 0.0
>   cpt [ True, False] False = 0.95
>   cpt [ True, False]  True = 0.05
>   cpt [ True,  True] False = 0.1
>   cpt [ True,  True]  True = 0.9

> bn4 : BayesianNetwork 4 [rv, rv, rv, rv]
> bn4 = Node bn3 Bool fB neB isP cpt where
>   isP : Fin 3 -> Bool
>   isP = \ k => k == 2
>   cpt : HVect (invImageSize isP True) (map (ntype bn3) (invImageVect isP True)) -> Bool -> Double
>   cpt [False] False = 0.9
>   cpt [False]  True = 0.1
>   cpt [ True] False = 0.05
>   cpt [ True]  True = 0.95


> p0 : Double
> p0 = chain bn4 f where 
>   f : (k : Fin 4) -> ntype bn4 k 
>   f             FZ    = True
>   f         (FS FZ)   = True
>   f     (FS (FS FZ))  = True
>   f (FS (FS (FS FZ))) = True

> p1 : Double
> p1 = chain bn4 f where 
>   f : (k : Fin 4) -> ntype bn4 k 
>   f             FZ    = False
>   f         (FS FZ)   = True
>   f     (FS (FS FZ))  = True
>   f (FS (FS (FS FZ))) = True


> {-
 
> ---}

 
 
