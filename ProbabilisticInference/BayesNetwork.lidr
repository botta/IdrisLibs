> module ProbabilisticInference.BayesNetwork

> import Data.Fin
> import Data.Vect

> import Basic.Operations
> import Fin.Operations
> import Fin.Properties
> import DirectedAcyclicGraphs.DAG

> %default total
> %access public export
> %auto_implicits off


> ||| A Bayesian network of Boolean variables
> data BayesNetBool : Nat -> Type where
>   Nil   : BayesNetBool Z
>   Node  : {n : Nat} ->
>           (g   : BayesNetBool n) -> 
>           (v   : Bool) ->
>           (isP : Fin n -> Bool) ->
>           (cpt : Vect (invImageSize isP True) Bool -> Bool -> Double) ->
>           BayesNetBool (S n)


> ||| The value of a node
> val : {n : Nat} -> (g : BayesNetBool n) -> (k : Fin n) -> Bool
> val {n = Z}    Nil                 k impossible 
> val {n = S m} (Node g v _ _) k with (decEq k last)  
>   | (Yes _) = v
>   | (No  c) = val g (strengthen k {contra = c})


> ||| Is this an implementation of the chain rule? 
> chain : {n : Nat} -> (g : BayesNetBool n) -> Double
> chain  Nil                    = 1
> chain (Node g v ps cpt) = (cpt (map (val g) (invImageVect ps True)) v) * (chain g)


Example:

    B    L
     \  /
      \/
      M
     /  
    G

> bnb0 : BayesNetBool 0
> bnb0 = Nil

> bnb1 : BayesNetBool 1
> bnb1 = Node bnb0 b ps cpt where
>   b   : Bool
>   b   = True
>   ps  : Fin 0 -> Bool
>   ps  = \ k => absurd k
>   cpt : Vect (invImageSize ps True) Bool -> Bool -> Double
>   cpt _ False = 0.05
>   cpt _ True  = 0.95

> bnb2 : BayesNetBool 2
> bnb2 = Node bnb1 b ps cpt where
>   b   : Bool
>   b   = True
>   ps  : Fin 1 -> Bool
>   ps  = \ k => False
>   cpt : Vect (invImageSize ps True) Bool -> Bool -> Double
>   cpt _ False = 0.3
>   cpt _ True  = 0.7

> bnb3 : BayesNetBool 3
> bnb3 = Node bnb2 b ps cpt where
>   b   : Bool
>   b   = True
>   ps  : Fin 2 -> Bool
>   ps  = \ k => True
>   cpt : Vect (invImageSize ps True) Bool -> Bool -> Double
>   cpt [False, False] False = 1.0
>   cpt [False, False]  True = 0.0
>   cpt [False,  True] False = 1.0
>   cpt [False,  True]  True = 0.0
>   cpt [ True, False] False = 0.95
>   cpt [ True, False]  True = 0.05
>   cpt [ True,  True] False = 0.1
>   cpt [ True,  True]  True = 0.9

> bnb4 : BayesNetBool 4
> bnb4 = Node bnb3 b ps cpt where
>   b    : Bool
>   b    = True
>   ps  : Fin 3 -> Bool
>   ps  = \ k => k == 2
>   cpt : Vect (invImageSize ps True) Bool -> Bool -> Double
>   cpt [False] False = 0.9
>   cpt [False]  True = 0.1
>   cpt [ True] False = 0.05
>   cpt [ True]  True = 0.95

> test : Double
> test = chain bnb4

If |chain| turns out to be useful (and perhaps we manage to implement
other useful computations on |BayesNetBool|s), we can perhaps think of
building such networks on the basis of simpler data types and for
specific computational tasks. For instance, we could start from a plain
DAG

< ||| A DAG
< data DAG : Nat -> Type where
<   Nil   :  DAG Z
<   Node  :  {n : Nat} -> (dn  : DAG n) -> (isP : Fin n -> Bool) -> DAG (S n)

and implement a transformation


> tail' : {n : Nat} -> {A : Fin (S n) -> Type} ->
>         ((k : Fin (S n)) -> A k) -> ((j : Fin n) -> A (weaken j))
> tail' f j = f (weaken j)



> ||| DAG to BayesNetBool
> mkBayesNetBool : {n       : Nat} -> 
>                  (dn      : DAG n) -> 
>                  (nval    : (k : Fin n) -> Bool) ->
>                  (ncpt    : (k : Fin n) -> Vect (invImageSize (isParent dn k) True) Bool -> Bool -> Double) ->
>                  BayesNetBool n
 
> mkBayesNetBool {n = Z}    Nil       _ _  = Nil

> mkBayesNetBool {n = S m} (Node dm isP) nval ncpt
>   = 
>   Node bnm b isP cpt where
>     nval' : (k : Fin m) -> Bool
>     nval' = tail nval
>     ncpt' : (k : Fin m) -> Vect (invImageSize (isParent dm k) True) Bool -> Bool -> Double
>     ncpt' k = s3 where
>       s0 : Vect (invImageSize (isParent (Node dm isP) (weaken k)) True) Bool -> Bool -> Double
>       s0 = ncpt (weaken k)
>       s1 : isParent (Node dm isP) (weaken k) = isParent dm k
>       s1 = isParentLemma2 dm isP
>       s2 : invImageSize {n = finToNat (weaken k)} (isParent (Node dm isP) (weaken k)) True 
>            = 
>            invImageSize {n = finToNat k} (isParent dm k) True
>       s2 = depCong2 {alpha = Nat}
>                     {P = \ n => Fin n -> Bool}
>                     {a1 = finToNat (weaken k)}
>                     {a2 = finToNat k}
>                     {Pa1 = isParent (Node dm isP) (weaken k)}
>                     {Pa2 = isParent dm k} 
>                     {f = \ X => \ Y => invImageSize {n = X} Y True} (weakenLemma k) s1
>       s3 : Vect (invImageSize (isParent dm k) True) Bool -> Bool -> Double
>       s3 = replace {P = \ X => Vect X Bool -> Bool -> Double} s2 s0
>     bnm : BayesNetBool m
>     bnm = mkBayesNetBool dm nval' ncpt'
>     b   : Bool
>     b   = nval last
>     cpt : Vect (invImageSize isP True) Bool -> Bool -> Double
>     cpt = s3 where
>       s0 : Vect (invImageSize (isParent (Node dm isP) last) True) Bool -> Bool -> Double
>       s0 = ncpt last
>       s1 : isParent (Node dm isP) last = isP
>       s1 = isParentLemma1 dm isP
>       s2 : invImageSize {n = finToNat last} (isParent (Node dm isP) last) True 
>            = 
>            invImageSize {n = m} isP True
>       s2 = depCong2 {f = \ X => \ Y => invImageSize {n = X} Y True} finToNatLastLemma s1
>       s3 : Vect (invImageSize isP True) Bool -> Bool -> Double
>       s3 = replace {P = \ X => Vect X Bool -> Bool -> Double} s2 s0

> {-

> ---}

 
