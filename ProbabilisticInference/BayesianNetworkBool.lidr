> module ProbabilisticInference.BayesianNetworkBool

> import Data.Fin
> import Data.Vect

> import Basic.Operations
> import Fin.Operations
> import Fin.Properties

> %default total
> %access public export
> %auto_implicits off


> ||| A Bayesianian network of Boolean variables
> data BayesianNetworkBool : Nat -> Type where
>   Nil   : BayesianNetworkBool Z
>   Node  : {n : Nat} ->
>           (g   : BayesianNetworkBool n) -> 
>           (isP : Fin n -> Bool) ->
>           (cpt : Vect (invImageSize isP True) Bool -> Bool -> Double) ->
>           BayesianNetworkBool (S n)

Example:

    B    L
     \  /
      \/
      M
     /  
    G

> bnb0 : BayesianNetworkBool 0
> bnb0 = Nil

> bnb1 : BayesianNetworkBool 1
> bnb1 = Node bnb0 ps cpt where
>   ps  : Fin 0 -> Bool
>   ps  = \ k => absurd k
>   cpt : Vect (invImageSize ps True) Bool -> Bool -> Double
>   cpt _ False = 0.05
>   cpt _ True  = 0.95

> bnb2 : BayesianNetworkBool 2
> bnb2 = Node bnb1 ps cpt where
>   ps  : Fin 1 -> Bool
>   ps  = \ k => False
>   cpt : Vect (invImageSize ps True) Bool -> Bool -> Double
>   cpt _ False = 0.3
>   cpt _ True  = 0.7

> bnb3 : BayesianNetworkBool 3
> bnb3 = Node bnb2 ps cpt where
>   ps  : Fin 2 -> Bool
>   ps  = \ k => True
>   cpt : Vect (invImageSize ps True) Bool -> Bool -> Double
>   cpt [False, False] False = 1.0
>   cpt [False, False]  True = 0.0
>   cpt [False,  True] False = 1.0
>   cpt [False,  True]  True = 0.0
>   cpt [ True, False] False = 0.95
>   cpt [ True, False]  True = 0.05
>   cpt [ True,  True] False = 0.1
>   cpt [ True,  True]  True = 0.9

> bnb4 : BayesianNetworkBool 4
> bnb4 = Node bnb3 ps cpt where
>   ps  : Fin 3 -> Bool
>   ps  = \ k => k == 2
>   cpt : Vect (invImageSize ps True) Bool -> Bool -> Double
>   cpt [False] False = 0.9
>   cpt [False]  True = 0.1
>   cpt [ True] False = 0.05
>   cpt [ True]  True = 0.95


Given a Bayesian network and a function that assigns a Boolean value to
the nodes of the network, one can implement a function that computes the
probability of that assignment:

> chain : {n : Nat} -> (bn : BayesianNetworkBool n) -> (f : Fin n -> Bool) -> Double
> chain  Nil            _   = 1
> chain (Node g ps cpt) f = let f' = \ k => f (FS k) in
>                           (cpt (map f' (invImageVect ps True)) (f FZ)) * (chain g f')

for instance

> p0 : Double
> p0 = chain bnb4 (\ k => True)

> p1 : Double
> p1 = chain bnb4 (\ k => if k == FZ then False else True)


> {-
 
> ---}

 
