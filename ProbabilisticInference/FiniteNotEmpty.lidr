> module ProbabilisticInference.FiniteNotEmpty

> import Data.Fin
> import Control.Isomorphism

> import Basic.Predicates
> import Finite.Predicates
> import Finite.Operations
> import Sigma.Sigma

> %default total
> %access public export
> %auto_implicits off


> FiniteNotEmpty : Type -> Type
> FiniteNotEmpty A = (Finite A, Not (Empty A))

> FiniteNotEmptyType : Type
> FiniteNotEmptyType = Sigma Type FiniteNotEmpty

> type : FiniteNotEmptyType -> Type
> type (MkSigma A _) = A

> finite : (O : FiniteNotEmptyType) -> Finite (type O)
> finite (MkSigma A (fA, neA)) = fA

> notEmpty : (O : FiniteNotEmptyType) -> Not (Empty (type O))
> notEmpty (MkSigma A (fA, neA)) = neA

> card : (O : FiniteNotEmptyType) -> Nat
> card (MkSigma A (fA, neA)) = card fA

> all : (O : FiniteNotEmptyType) -> List (type O)
> all (MkSigma A (fA, neA)) = toList fA
