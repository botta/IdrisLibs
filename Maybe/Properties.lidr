> module Finite.Properties

> import Data.Fin
> import Control.Isomorphism

> import Finite.Predicates
> import Sigma.Sigma
> import Basic.Predicates

> %default total
> %access public export
> %auto_implicits off


> ||| If |A| is finite, |Maybe A| is finite
> finiteMaybeLemma : {A : Type} -> Finite A -> Finite (Maybe A)
> finiteMaybeLemma {A} (MkSigma n iso) = MkSigma (S n) s1 where
>   s0 : Iso (Maybe A) (Maybe (Fin n))
>   s0 = maybeCong iso
>   s1 : Iso (Maybe A) (Fin (S n))
>   s1 = isoTrans s0 maybeIsoS


> ||| For all |A: Type|, |Maybe A| is not empty
> notEmptyMaybe : {A : Type} -> Not (Empty (Maybe A))
> notEmptyMaybe = \ f => f Nothing 


> ||| If |A| has decidable equality, |Maybe A| has decidable equality
> decidableMaybeLemma : {A : Type} -> DecEq A -> DecEq (Maybe A)
> decidableMaybeLemma = %implementation
