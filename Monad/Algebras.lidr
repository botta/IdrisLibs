> module Monad.Algebras

> import Syntax.PreorderReasoning

> %default total
> %access public export
> %auto_implicits off


Condition for a function |f| to be an |M|-algebra homomorphism:

> algMorSpec  :  Monad M => {A, B : Type} -> 
>                (alpha : M A -> A) -> (beta : M B -> B) -> 
>                (f : A -> B) -> Type
> algMorSpec {A} {B} alpha beta f = (beta . map f) `ExtEq` (f . alpha)

Structure maps of |M|-algebras are left inverses of |pure|:

> algPureSpec  :  Monad M => {A : Type} -> (alpha : M A -> A) -> Type
> algPureSpec {A} alpha = alpha . pure `ExtEq` id


Structure maps of |M|-algebras are themselves |M|-algebra homomorphisms:

> algJoinSpec  :  Monad M => {A : Type} -> (alpha : M A -> A) -> Type
> algJoinSpec {A} alpha =  algMorSpec join alpha alpha  -- |(alpha . map alpha) `ExtEq` (alpha . join)|


A lemma about computation with |M|-algebras:

> algLemma  :  Monad M =>
>              {A, B, C : Type} -> (alpha: M C -> C) -> (ee : algJoinSpec alpha) ->
>              (f : B -> C ) -> (g : A -> M B) ->
>              (alpha . map (alpha . map f . g)) `ExtEq` (alpha . map f . join . map g)

> algLemma {A} {B} {C} alpha ee f g ma =
>
>   ( (alpha . map (alpha . map f . g)) ma )          ={ cong (mapPresComp g (alpha . map f) ma) }=
>
>   ( (alpha . map (alpha . map f) . map g) ma )      ={ cong (mapPresComp (map f) alpha (map g ma)) }=
>
>   ( (alpha . map alpha . map (map f) . map g) ma )  ={ ee (map (map f) (map g ma)) }=
>
>   ( (alpha . join . map (map f) . map g) ma )       ={ cong (sym (joinNatTrans f (map g ma))) }=
>
>   ( (alpha . map f . join . map g) ma )             QED

