% -*-Latex-*-
%\documentclass[a4paper,landscape]{slides}
\documentclass[colorhighlight,coloremph]{beamer}
\usepackage[mode=buildnew]{standalone}
\usetheme{boxes}
\usepackage{color,soul}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage[english]{babel}
\usepackage{ucs}
\usepackage[utf8x]{inputenc}
\usepackage{csquotes}
\usepackage{moreverb}
\usepackage{fancyvrb}
\usepackage{xcolor}


\setbeamertemplate{navigation symbols}{}%remove navigation symbols


\newcommand{\yslant}{0.5}
\newcommand{\xslant}{-1.5}

%include slides.fmt

\input{macros.TeX}

\definecolor{beamerblue}{rgb}{0.2,0.2,0.7}

\addfootbox{section}{\quad \tiny FP winter meeting, Jan. 2020}

\title{Verified dynamic programming}

\author{ Nicola Botta\inst{1,2} }
\institute{\inst{1}{Potsdam Institute for Climate Impact Research} \\
           \inst{2}{Chalmers University of Technology, Gothenburg,
             Sweden}}


\begin{document}

\date{}

\frame{\maketitle}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Outline}
%
\begin{itemize}
\vfill
\item Bellman equations
\vfill
\item Where do they come from? A sketch of DP
\vfill
\item Monads and extensional equality preservation
\vfill
\item Naive DP theories
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Bellman equations}
%
\begin{itemize}
  \vfill
  \item Wikipedia, Bellman equation in Markov decision processes:
    \begin{equation*}
      V^{\pi }(x) = R(x, \pi(x)) + \gamma \sum _{x'} P(x' \vert x, \pi (x)) * V^{\pi }(x')
    \end{equation*}
  \item Bertsekas' lecture slides on dynamic programming:
    \begin{itemize}
      \vfill  
    \item System $x_{k+1} = f_k(x_k, u_k, w_k), k = 0,...,N − 1$.
      \vfill
    \item Control constraints $u_k \in U_k(x_k)$.
      \vfill
    \item Probability distribution $P_k(\cdot \vert x_k, u_k)$ of $w_k$.
      \vfill
    \item Policies $\pi = {\mu_0, \dots, \mu_{N−1}}$, where $µ_k$ maps
      states $x_k$ into controls $u_k = µ_k(x_k)$ such that
      $µ_k(x_k) \in U_k(x_k)$ for all $x_k$.
      \vfill
    \item Expected cost of $\pi$ starting at $x_0$ is
      \begin{equation*}
        J_{\pi}(x_0)
        =
        E \left\{ g_N (x_N) + \sum_{k=0}^{N - 1} g_k(x_k, µ_k(x_k), w_k) \right\}
      \end{equation*}
    \end{itemize}
  \vfill
  \item Many more formulations \dots    
\end{itemize}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Where do they come from? A sketch of DP}
%
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Monads and extensional equality preservation}
%
\vfill
> ExtEq : {A, B : Type} -> (f, g : A -> B) -> Type
>
> ExtEq f g = (a : Domain f) -> f a = g a
\vfill
> M : Type -> Type
\vfill
> map : {A, B : Type} -> (A -> B) -> M A -> M B
\vfill
\color{red}
> mapPresExtEq  :  {A, B : Type} ->  (f, g : A -> B) ->
>
>                  ExtEq f g -> ExtEq (map f) (map g)
\color{black}
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\begin{frame}[fragile]
\frametitle{Naive DP theories}
%
\vfill
%
\end{frame}

%% -------------------------------------------------------------------

\end{document}
