> module SequentialDecisionProblems.NaiveTheoryNonDeterministic

> import Syntax.PreorderReasoning

> import Sigma.Sigma
> import List.Properties
> import Fun.Predicates
> import VeriFunctor.VeriFunctor
> import VeriMonad.VeriMonad

> %default total
> %auto_implicits off
> %access public export

> infixr 7 ##
> infixr 7 <++>

> %hide head
> %hide lteRefl


# Decision process

> X    : (t : Nat) -> Type
> Y    : (t : Nat) -> (x : X t) -> Type
> next : (t : Nat) -> (x : X t) -> (y : Y t x) -> List (X (S t))


# Decision problem

> Val    : Type
> reward : (t : Nat) -> (x : X t) -> (y : Y t x) -> (x' : X (S t)) -> Val
> (<+>)  : Val -> Val -> Val
> zero   : Val
> (<=)   : Val -> Val -> Type
> meas   : List Val -> Val

> (<++>) : {A : Type} -> (A -> Val) -> (A -> Val) -> A -> Val
> f <++> g = \ a => f a <+> g a


# Basic requirements for verified backwards induction

> lteRefl  : {a : Val} -> a <= a
> lteTrans : {a, b, c : Val} -> a <= b -> b <= c -> a <= c
> plusMon  : {a, b, c, d : Val} -> a <= b -> c <= d -> (a <+> c) <= (b <+> d)
> measMon  : {A : Type} -> (f, g : A -> Val) -> ((a : A) -> (f a) <= (g a)) -> (as : List A) -> 
>            meas (map f as) <= meas (map g as)


# Policies and policy sequences

> Policy : (t : Nat) -> Type
> Policy t = (x : X t) -> Y t x

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
>   Nil  : {t : Nat} -> PolicySeq t Z
>   (::) : {t, n : Nat} -> Policy t -> PolicySeq (S t) n -> PolicySeq t (S n)


# The value of policy sequences (measure of sums of rewards)

> val : {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Val
> val {t}  Nil      x = zero
> val {t} (p :: ps) x = let y = p x in
>                       let xs' = next t x y in
>                       meas (map (reward t x y <++> val ps) xs')


# Optimality of policy sequences

> OptPolicySeq : {t, n : Nat} -> PolicySeq t n -> Type
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> val ps' x <= val ps x


# Optimality of extensions of policy sequences

> OptExt : {t, n : Nat} -> PolicySeq (S t) n -> Policy t -> Type
> OptExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> val (p' :: ps) x <= val (p :: ps) x


# Bellman's 1957 principle of optimality

> Bellman  :  {t, n : Nat} -> 
>             (ps  :  PolicySeq (S t) n) -> OptPolicySeq ps ->
>             (p   :  Policy t)          -> OptExt ps p ->
>             OptPolicySeq (p :: ps)
>
> Bellman {t} ps ops p oep = opps where
>   opps (p' :: ps') x = 
>     let y'  =  p' x in
>     let xs' =  next t x y' in
>     let f'  =  reward t x y' <++> val ps' in
>     let f   =  reward t x y' <++> val ps in
>     let s0  =  \ x' => plusMon lteRefl (ops ps' x') in -- (x' : X (S t)) -> f' x' <= f x' 
>     let s1  =  measMon f' f s0 xs' in                  -- val (p' :: ps') x <= val (p' :: ps) x
>     let s2  =  oep p' x in                             -- val (p' :: ps)  x <= val (p :: ps)  x
>     lteTrans s1 s2


# Verified backwards induction:

First, we assess that the empty policy sequence is optimal

> nilOptPolicySeq : OptPolicySeq Nil
> nilOptPolicySeq Nil x = lteRefl

Now, provided that we can implement

> optExt     : {t, n : Nat} -> PolicySeq (S t) n -> Policy t
> optExtSpec : {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

then

> bi : (t : Nat) -> (n : Nat) -> PolicySeq t n
> bi t  Z     =  Nil
> bi t (S n)  =  let ps = bi (S t) n in 
>                optExt ps :: ps

is correct "by construction"

> biLemma : (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)
> biLemma t  Z     =  nilOptPolicySeq
> biLemma t (S n)  =  let ps  = bi (S t) n in
>                     let ops = biLemma (S t) n in
>                     let p   = optExt ps in
>                     let oep = optExtSpec ps in
>                     Bellman ps ops p oep


# Trajectories and semantic verification

We want to show that |val ps x| does indeed compute the measure (for
instance, the expected value) of the sum of the rewards obtained along
the trajectories that are obtained under the policy sequence |ps| when
starting in |x|. To this end, we start by defining sequences of
state-control pairs

> data StateCtrlSeq : (t : Nat) -> (n : Nat) -> Type where
>   Last  :  {t : Nat} -> (x : X t) -> StateCtrlSeq t (S Z)           
>   (##)  :  {t, n : Nat} -> Sigma (X t) (Y t) -> StateCtrlSeq (S t) (S n) -> StateCtrlSeq t (S (S n))

and a function |val'| that adds the the rewards along a state-control
sequence:

> head : {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
> head (Last x)             = x
> head (MkSigma x y ## xys) = x  

> val' : {t, n : Nat} -> StateCtrlSeq t n -> Val
> val' {t} {n = S Z}     (Last x)             = zero
> val' {t} {n = S (S m)} (MkSigma x y ## xys) = reward t x y (head xys) <+> val' xys

Next, we implement a function that computes all the trajectories that
are obtained under a policy sequence |ps| when starting in |x|:

> trj : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> List (StateCtrlSeq t (S n))
> trj {t}  Nil      x  =  pure (Last x)
> trj {t} (p :: ps) x  =  let y   = p x in
>                         let xs' = next t x y in                            
>                         map ((MkSigma x y) ##) (xs' >>= trj ps)

Now we can formulate the property that ensures that |val ps x| does
compute the measure of the sum of the rewards obtained along the
trajectories that are obtained under the policy sequence |ps| when
starting in |x| rigorously:

< valVal'Th : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
<             val ps x = meas (map val' (trj ps x)) 

We can actually prove the |val|-|val'| theorem if |meas| fulfill the
natural conditions:

> measPlusLemma  : {A : Type} -> (f : A -> Val) -> (g : A -> Val) -> (as : List A) -> 
>                  meas (map (f <++> g) as) = meas (map f as) <+> meas (map g as)

> measJoinLemma  : (vss : List (List Val)) -> meas (join vss) = meas (map meas vss)

> measConstLemma : {A : Type} -> (v : Val) -> (as : List A) -> meas (map (const v) as) = v

To show this, we first put forward a few auxiliary lemmas:

> mapConstLemma    : {A, B, C : Type} -> (c : C) -> (as : List A) -> (f : A -> B) ->
>                    map (const c) as = map (const c) (map f as)

> mapConstLemma'   : {A, B, C : Type} -> (b : B) -> (as : List A) -> (f : B -> C) ->
>                    map f (map (const b) as) = map (const (f b)) as
                 
> measRetLemma     : (v : Val) -> meas (pure v) = v

> mapHeadLemma     : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
>                     map head (trj ps x) = map (const x) (trj ps x) 

> measMapHeadLemma : {t, n : Nat} -> (ps : PolicySeq t n) -> (f : X t -> Val) -> (x : X t) -> 
>                    (meas . (map (f . head) . (trj ps))) x = f x            

> measMapHeadLemma' : {t, n : Nat} -> 
>                     (ps : PolicySeq t n) -> (mx : List (X t)) -> (f : X t -> Val) ->
>                     meas (map (f . head) (mx >>= trj ps)) = meas (map f mx)

> val'Lemma        : {t, m : Nat} -> 
>                    (x : X t) -> (y : Y t x) -> (xyss : List (StateCtrlSeq (S t) (S m))) -> 
>                    map val' (map ((MkSigma x y) ##) xyss)
>                    = 
>                    map (((reward t x y) . head) <++> val') xyss            

Some of these lemmas depend on |measPlusLemma|, |measJoinLemma| and
|measConstLemma|. They are all implemented after the main result. 

We prove the main result by induction of the policy sequence |ps|,
starting from the induction step:

> valVal'Th : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
>             val ps x = meas (map val' (trj ps x)) 

> valVal'Th {t} (p :: ps) x = 
>  let y    = p x in
>  let xs'  = next t x y in
>  let r    = reward t x y in
>  let h    = trj ps in
>  let lhs  = meas (map r xs') in  
>  let lhs' = meas (map (r . head) (xs' >>= h)) in
>  let rhs  = meas (map meas (map (map val') (map h xs'))) in
>    ( val (p :: ps) x )
>  ={ Refl }=
>    ( meas (map (r <++> val ps) xs') )
>  ={ measPlusLemma r (val ps) xs' }=
>    ( meas (map r xs') <+> meas (map (val ps) xs') )
>    {- Lifted induction pattern: we are about to prove 
>           val ps x = meas (map val' (trj ps x)) 
>       and 
>           meas (map val' (trj ps x)) = ((meas . map val') . h) x
>       by definition of h and composition. This implies that
>           map (val ps) xs' = map ((meas . map val') . h) xs'
>       We use this pattern to rewrite the rhs: -}
>  ={ cong {f = \ α => lhs <+> meas α} 
>          (mapPresExtEq (val ps) ((meas . map val') . h) (valVal'Th ps) xs') }= 
>    ( lhs <+> meas (map ((meas . map val') . h) xs') )
>  ={ cong {f = \ α => lhs <+> meas α} 
>          (mapPresComp h (meas . map val') xs') }=
>    ( lhs <+> meas (map (meas . (map val')) (map h xs')) )      
>  ={ cong {f = \ α => lhs <+> meas α} (mapPresComp (map val') meas (map h xs')) }=
>    ( lhs <+> meas (map meas (map (map val') (map h xs'))) )
>  ={ Refl }=  
>    ( meas (map r xs') <+> rhs )    
>    -- measMapHeadLemma': meas (map (f . head) (xs >>= trj ps)) = meas (map f xs)
>  ={ cong {f = \ α => α <+> rhs} (sym (measMapHeadLemma' ps xs' r)) }=
>    ( meas (map (r . head) (xs' >>= h)) <+> rhs )
>  ={ Refl }=  
>    ( meas (map (r . head) (xs' >>= h)) <+> meas (map meas (map (map val') (map h xs'))) )
>    -- measJoinLemma: meas (join vss) = meas (map meas vss)
>  ={ cong {f = \ α => lhs' <+> α} (sym (measJoinLemma (map (map val') (map h xs'))))  }=    
>    ( lhs' <+> meas (join (map (map val') (map h xs'))) )
>  ={ cong {f = \ α => lhs' <+> meas (join α)} (sym (mapPresComp h (map val') xs')) }=
>    ( lhs' <+> meas (join (map (map val' . h) xs')) )  
>  ={ cong {f = \ α => lhs' <+> meas α} (sym (mapJoinLemma val' h xs')) }= 
>    ( lhs' <+> meas (map val' (join (map h xs'))) )
>  ={ cong {f = \ α => lhs' <+> meas (map val' α)} (sym (bindJoinMapSpec h xs')) }=
>    ( lhs' <+> meas (map val' (xs' >>= h)) )  
>  ={ Refl }=
>    ( meas (map (r . head) (xs' >>= h)) <+> meas (map val' (xs' >>= h)) )    
>  ={ sym (measPlusLemma (r . head) val' (xs' >>= h)) }=
>    ( meas (map ((r . head) <++> val') (xs' >>= h)) )  
>    -- val'Lemma: map val' (map ((MkSigma x y) ##) xyss) = map ((r . head) <++> val') xyss
>  ={ cong (sym (val'Lemma x y (xs' >>= h))) }= 
>    ( meas (map val' (map ((MkSigma x y) ##) (xs' >>= trj ps))) )
>  ={ Refl }= 
>    ( meas (map val' (trj (p :: ps) x)) )
>  QED

and then the base case:

> valVal'Th {t} Nil x = ( val Nil x )
>                 ={ Refl }=
>                   ( zero )
>                 ={ sym (measRetLemma zero) }=
>                   ( meas (pure zero) )
>                 ={ Refl }=
>                   ( meas (pure (val' (Last x))) )
>                 ={ cong {f = meas} (sym (pureNatTrans val' (Last x))) }=
>                   ( meas (map val' (pure (Last x))) )
>                 ={ Refl }=
>                   ( meas (map val' (trj Nil x)) )
>                 QED


# Auxiliary results

> -- map (const c) as = map (const c) (map f as)
> mapConstLemma c ma f = ( map (const c) ma )
>                      ={ Refl }=
>                        ( map ((const c) . f) ma )
>                      ={ mapPresComp f (const c) ma }=
>                        ( map (const c) (map f ma) )
>                      QED

> -- map f (map (const b) as) = map (const (f b)) as
> mapConstLemma' b ma f = ( map f (map (const b) ma) )
>                       ={ sym (mapPresComp (const b) f ma) }=  
>                         ( map (f . (const b)) ma )
>                       ={ Refl }=   
>                         ( map (const (f b)) ma )
>                       QED

< pureNatTrans : {X, Y : Type} -> (f : X -> Y) -> ExtEq {B = List Y} (map f . pure) (pure . f)

> -- (v : Val) -> meas (pure v) = v
> measRetLemma v = ( meas (pure v) )
>                ={ Refl }=
>                  ( meas (pure ((const v) v)) )
>                ={ cong {f = meas} (sym (pureNatTrans (const v) v)) }=
>                  ( meas (map (const v) (pure v)) )
>                ={ measConstLemma v (pure v) }=
>                  ( v )
>                QED

> -- map head (trj ps x) = map (const x) (trj ps x) 
> mapHeadLemma {t}      Nil      x = Refl
> mapHeadLemma {t} {n = S m} (p :: ps) x =
>   let y     =  p x in
>   let xy    =  MkSigma x y in
>   let xs'   =  next t x y in
>   let xyss  =  (xs' >>= trj ps) in 
>     ( map head (trj (p :: ps) x) )
>   ={ Refl }=
>     ( map head (map (xy ##) xyss) )
>   ={ sym (mapPresComp {A = StateCtrlSeq (S t) (S m)} (xy ##) head xyss) }=
>     ( map (head . (xy ##)) xyss )
>   ={ Refl }=
>     ( map (const x) xyss )
>   ={ mapConstLemma x xyss (xy ##) }=
>     ( map (const x) (map (xy ##) xyss) )
>   ={ Refl }=
>     ( map (const x) (trj (p :: ps) x) )
>   QED                                
                                                                         
> -- (meas . (map (f . head) . (trj ps))) x = f x            
> measMapHeadLemma {t} {n} ps f x 
>   = ( (meas . (map (f . head) . (trj ps))) x )
>   ={ Refl }=
>     ( meas (map (f . head) (trj ps x)) )
>   ={ cong (mapPresComp head f (trj ps x)) }=
>     ( meas (map f (map head (trj ps x))) )
>   ={ cong {f = \ α => meas (map f α)} (mapHeadLemma ps x) }=
>     ( meas (map f (map (const x) (trj ps x))) )
>   ={ cong (sym (mapPresComp (const x) f (trj ps x))) }=
>     ( meas (map (f . (const x)) (trj ps x)) )
>   ={ Refl }=
>     ( meas (map (const (f x)) (trj ps x)) )
>   ={ measConstLemma (f x) (trj ps x) }=
>     ( f x )
>   QED

> -- meas (map (f . head) (mx >>= trj ps)) = meas (map f mx)
> measMapHeadLemma' {t} {n} ps mx f
>   = let g  = trj ps in
>     ( meas (map (f . head) (mx >>= g)) )
>   ={ cong {f = \ α => meas (map (f . head) α)} (bindJoinMapSpec g mx) }= 
>     ( meas (map (f . head) (join (map g mx))) )
>   ={ cong (mapJoinLemma (f . head) g mx) }= 
>     ( meas (join (map (map (f . head) . g) mx)) )
>   ={ measJoinLemma (map (map (f . head) . g) mx) }= 
>     ( meas (map meas (map (map (f . head) . g) mx)) )  
>   ={ cong (sym (mapPresComp (map (f . head) . g) meas mx) ) }= 
>     ( meas (map (meas . (map (f . head) . g)) mx) )  
>   ={ cong (mapPresExtEq (meas . (map (f . head) . (trj ps))) f (measMapHeadLemma ps f) mx) }= 
>     ( meas (map f mx) )
>   QED

> -- map val' (map ((MkSigma x y) ##) xyss) = map (((reward t x y) . head) <++> val') xyss
> val'Lemma {t} {m} x y xyss = ( map val' (map ((MkSigma x y) ##) xyss) )
>                        ={ sym (mapPresComp {A = StateCtrlSeq (S t) (S m)} ((MkSigma x y) ##) val' xyss) }=
>                          ( map (val' . ((MkSigma x y) ##)) xyss )
>                        ={ Refl }=
>                          ( map (((reward t x y) . head) <++> val') xyss )
>                        QED
                       
> {-

---}
