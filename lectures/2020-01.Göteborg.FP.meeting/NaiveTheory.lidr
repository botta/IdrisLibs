> module NaiveTheory

> import Syntax.PreorderReasoning

> import Sigma.Sigma
> import Fun.Predicates

> %default total
> %auto_implicits off
> %access public export

> infixr 7 ##
> infixr 7 <++>

> %hide map
> %hide pure
> %hide join
> %hide (>>=)
> %hide head
> %hide lteRefl


# Decision process

> M    : Type -> Type

> X    : (t : Nat) -> Type
> Y    : (t : Nat) -> (x : X t) -> Type
> next : (t : Nat) -> (x : X t) -> (y : Y t x) -> M (X (S t))


# Decision problem

> Val    : Type
> reward : (t : Nat) -> (x : X t) -> (y : Y t x) -> (x' : X (S t)) -> Val

> (<+>)  : Val -> Val -> Val
> zero   : Val
> (<=)   : Val -> Val -> Type

> meas   : M Val -> Val


# Basic requirements for verified backwards induction

> map      : {A, B : Type} -> (A -> B) -> M A -> M B

> lteRefl  : {a : Val} -> a <= a
> lteTrans : {a, b, c : Val} -> a <= b -> b <= c -> a <= c
> plusMon  : {a, b, c, d : Val} -> a <= b -> c <= d -> (a <+> c) <= (b <+> d)

> measMon  : {A : Type} -> (f, g : A -> Val) -> ((a : A) -> (f a) <= (g a)) -> 
>            (ma : M A) -> meas (map f ma) <= meas (map g ma)


# Helper functions

> (<++>) : {A : Type} -> (A -> Val) -> (A -> Val) -> A -> Val
> f <++> g = \ a => f a <+> g a


# Policies and policy sequences

> Policy : (t : Nat) -> Type
> Policy t = (x : X t) -> Y t x

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
>   Nil  : {t : Nat} -> PolicySeq t Z
>   (::) : {t, n : Nat} -> Policy t -> PolicySeq (S t) n -> PolicySeq t (S n)


# The value of policy sequences (Bellman equation)

> val : {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Val
> val {t}  Nil      x = zero
> val {t} (p :: ps) x = let y = p x in
>                       let mx' = next t x y in
>                       meas (map (reward t x y <++> val ps) mx')


# Optimality of policy sequences

> OptPolicySeq : {t, n : Nat} -> PolicySeq t n -> Type
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> 
>                             val ps' x <= val ps x


# Optimality of extensions of policy sequences

> OptExt : {t, n : Nat} -> PolicySeq (S t) n -> Policy t -> Type
> OptExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> 
>                     val (p' :: ps) x <= val (p :: ps) x


# Bellman's 1957 principle of optimality

> Bellman  :  {t, n : Nat} -> 
>             (ps  :  PolicySeq (S t) n) -> OptPolicySeq ps ->
>             (p   :  Policy t)          -> OptExt ps p ->
>             OptPolicySeq (p :: ps)
>
> Bellman {t} ps ops p oep (p' :: ps') x = 
>   let y'  =  p' x in
>   let mx' =  next t x y' in
>   let f'  =  reward t x y' <++> val ps' in
>   let f   =  reward t x y' <++> val ps in
>   let s0  =  \ x' => plusMon lteRefl (ops ps' x') in 
>   let s1  =  measMon f' f s0 mx' in  -- val (p' :: ps') x <= val (p' :: ps) x
>   let s2  =  oep p' x in             -- val (p' :: ps)  x <= val (p :: ps)  x
>   lteTrans s1 s2


# Verified backwards induction:

First, we assess that the empty policy sequence is optimal

> nilOptPolicySeq : OptPolicySeq Nil
> nilOptPolicySeq Nil x = lteRefl

Now, provided that we can implement

> optExt     : {t, n : Nat} -> PolicySeq (S t) n -> Policy t
> optExtSpec : {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

then

> bi : (t : Nat) -> (n : Nat) -> PolicySeq t n
> bi t  Z     =  Nil
> bi t (S n)  =  let ps = bi (S t) n in 
>                optExt ps :: ps

is correct "by construction"

> biLemma : (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)
> biLemma t  Z     =  nilOptPolicySeq
> biLemma t (S n)  =  let ps  = bi (S t) n in
>                     let ops = biLemma (S t) n in
>                     let p   = optExt ps in
>                     let oep = optExtSpec ps in
>                     Bellman ps ops p oep


# Trajectories and consistency of |val|

We want to prove that |val ps x| does indeed compute the measure (for
instance, the expected value) of the sum of the rewards along the
trajectories that are obtained under the policy sequence |ps| when
starting in |x|. To this end, we start by defining sequences of
state-control pairs

> data StateCtrlSeq : (t : Nat) -> (n : Nat) -> Type where
>   Last  :  {t : Nat} -> (x : X t) -> StateCtrlSeq t (S Z)           
>   (##)  :  {t, n : Nat} -> 
>            Sigma (X t) (Y t) -> StateCtrlSeq (S t) (S n) -> StateCtrlSeq t (S (S n))

and a function |sumR| that computes the sum of the rewards of a
state-control sequence:

> head : {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
> head (Last x)             = x
> head (MkSigma x y ## xys) = x  

> sumR : {t, n : Nat} -> StateCtrlSeq t n -> Val
> sumR {t} (Last x)             = zero
> sumR {t} (MkSigma x y ## xys) = reward t x y (head xys) <+> sumR xys


Next, we implement a function that computes all the trajectories that
are obtained under a policy sequence |ps| when starting in |x|. For
this, |M| has to be equipped with a return operation 

> pure     : {A : Type} -> A -> M A

It is also useful to introduce bind and join operations

> (>>=)    : {A, B : Type} -> M A -> (A -> M B) -> M B

> join     : {A : Type} -> M (M A) -> M A

that fulfill the monadic specification

> bindJoinLemma : {A, B : Type} -> (ma : M A) -> (f : A -> M B) -> ma >>= f = join (map f ma)

With |pure| and |>>=|, we can express the computation of the
trajectories as

> trj : {t, n : Nat} -> 
>       (ps : PolicySeq t n) -> (x : X t) -> M (StateCtrlSeq t (S n))
> trj {t}  Nil      x  =  pure (Last x)                            
> trj {t} (p :: ps) x  =  let y   = p x in                         
>                         let mx' = next t x y in                            
>                         map ((MkSigma x y) ##) (mx' >>= trj ps)

Finally, we compute the measure of the sum of the rewards obtained along
the trajectories that are obtained under the policy sequence |ps| when
starting in |x|

> val' : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> Val
> val' ps x = meas (map sumR (trj ps x)) 

Now we can formulate the property that |val ps| does indeed compute the
measure of the sum of the rewards obtained along the trajectories
obtained under the policy sequence |ps|:

> valVal'Th : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
>             val ps x = val' ps x 

We can prove the |val|-|val'| theorem if |map| preserves composition

> mapPresComp : {A, B, C : Type} ->
>               (ma : M A) -> (f : A -> B) -> (g : B -> C) -> 
>               map (g . f) ma = (map g . map f) ma

, |pure| and |join| are natural transformations

> pureNatTrans     : {A, B : Type} -> (a : A) -> (f : A -> B) -> 
>                    map f (pure a) = pure (f a)

> joinNatTrans     : {A, B : Type} -> (mma : M (M A)) -> (f : A -> B) -> 
>                    map f (join mma) = join (map (map f) mma)

and the measure fulfills three natural conditions:

> measPlusLemma  : {A : Type} -> 
>                  (f : A -> Val) -> (g : A -> Val) -> (ma : M A) -> 
>                  meas (map (f <++> g) ma) = meas (map f ma) <+> meas (map g ma)

> measJoinLemma  : (vss : M (M Val)) -> meas (join vss) = meas (map meas vss)

> measConstLemma : {A : Type} -> (v : Val) -> (ma : M A) -> 
>                  meas (map (const v) ma) = v

To prove the |val|-|val'| equivalence, we first put forward a few
auxiliary lemmas:

> mapConstLemma    : {A, B, C : Type} -> (c : C) -> (ma : M A) -> (f : A -> B) ->
>                    map (const c) ma = map (const c) (map f ma)

> measRetLemma     : (v : Val) -> meas (pure v) = v

> mapJoinLemma     : {A, B, C : Type} -> 
>                    (f : B -> C) -> (g : A -> M B) -> (ma : M A) -> 
>                    map f (join (map g ma)) = join (map (map f . g) ma)

> mapHeadLemma     : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
>                     map head (trj ps x) = map (const x) (trj ps x) 

> measMapHeadLemma : {t, n : Nat} -> (ps : PolicySeq t n) -> 
>                    (f : X t -> Val) -> (x : X t) -> 
>                    (meas . (map (f . head) . (trj ps))) x = f x            

> measMapHeadLemma' : {t, n : Nat} -> 
>                    (ps : PolicySeq t n) -> (mx : M (X t)) -> (f : X t -> Val) ->
>                    meas (map (f . head) (mx >>= trj ps)) = meas (map f mx)

> sumRLemma        : {t, m : Nat} -> 
>                    (x : X t) -> (y : Y t x) -> 
>                    (xyss : M (StateCtrlSeq (S t) (S m))) -> 
>                    map sumR (map ((MkSigma x y) ##) xyss)
>                    = 
>                    map (((reward t x y) . head) <++> sumR) xyss            

These auxiliary results are implemented after the main result. With
these results in place, we can derive the equivalence of |val| and
|val'| by induction on |ps|. But we need one more axiom:

> mapPresExtEq : {A, B : Type} ->  
>                (f, g : A -> B) -> ExtEq f g -> ExtEq (map f) (map g)

!

> valVal'Th Nil x = ( val Nil x )
>                 ={ Refl }=
>                   ( zero )
>                 ={ sym (measRetLemma zero) }=
>                   ( meas (pure zero) )
>                 ={ Refl }=
>                   ( meas (pure (sumR (Last x))) )
>                 ={ cong (sym (pureNatTrans (Last x) sumR)) }=
>                   ( meas (map sumR (pure (Last x))) )
>                 ={ Refl }=
>                   ( meas (map sumR (trj Nil x)) )
>                 ={ Refl }=
>                   ( val' Nil x )
>                 QED

> valVal'Th {t} {n = S m} (p :: ps) x = 
>  let y    = p x in
>  let mx'  = next t x y in
>  let r    = reward t x y in
>  let h    = trj ps in
>  let lhs  = meas (map r mx') in  
>  let lhs' = meas (map (r . head) (mx' >>= h)) in
>  let rhs  = meas (map meas (map (map sumR) (map h mx'))) in
>    ( val (p :: ps) x )
>  ={ Refl }=
>    ( meas (map (r <++> val ps) mx') )
>  ={ measPlusLemma r (val ps) mx' }=
>    ( meas (map r mx') <+> meas (map (val ps) mx') )
>    {-
>      val ps x = val' ps x 
>        => 
>      map (val ps) mx' = map (val' ps) mx' 
>    -}
>  ={ cong {f = \ α => lhs <+> meas α} 
>          (mapPresExtEq (val ps) (val' ps) (valVal'Th ps) mx') }= 
>    ( lhs <+> meas (map (val' ps) mx') )
>    {- 
>       val' ps x = ((meas . map sumR) . h) x 
>         => 
>       map (val' ps) mx' = map ((meas . map sumR) . h) mx' 
>    -}
>  ={ cong {f = \ α => lhs <+> meas α} 
>          (mapPresExtEq (val' ps) 
>                        ((meas . map {A = StateCtrlSeq (S t) (S m)} sumR) . h) 
>                        (\ x => Refl) mx') }= 
>    ( lhs <+> meas (map ((meas . map sumR) . h) mx') )
>  ={ cong {f = \ α => lhs <+> meas α} 
>          (mapPresComp mx' h (meas . map sumR)) }=
>    ( lhs <+> meas (map (meas . (map sumR)) (map h mx')) )      
>  ={ cong {f = \ α => lhs <+> meas α} 
>          (mapPresComp (map h mx') (map sumR) meas) }=
>    ( lhs <+> meas (map meas (map (map sumR) (map h mx'))) )
>  ={ Refl }=  
>    ( meas (map r mx') <+> rhs )    
>    -- measMapHeadLemma': meas (map (f . head) (xs >>= trj ps)) = meas (map f xs)
>  ={ cong {f = \ α => α <+> rhs} (sym (measMapHeadLemma' ps mx' r)) }=
>    ( meas (map (r . head) (mx' >>= h)) <+> rhs )
>  ={ Refl }=  
>    ( meas (map (r . head) (mx' >>= h)) 
>      <+> 
>      meas (map meas (map (map sumR) (map h mx'))) )
>    -- measJoinLemma: meas (join vss) = meas (map meas vss)
>  ={ cong {f = \ α => lhs' <+> α} 
>          (sym (measJoinLemma (map (map sumR) (map h mx'))))  }= 
>    ( lhs' <+> meas (join (map (map sumR) (map h mx'))) )
>  ={ cong {f = \ α => lhs' <+> meas (join α)} 
>          (sym (mapPresComp mx' h (map sumR))) }=
>    ( lhs' <+> meas (join {A = Val} (map (map sumR . h) mx')) )  
>  ={ cong {f = \ α => lhs' <+> meas α} (sym (mapJoinLemma sumR h mx')) }= 
>    ( lhs' <+> meas (map sumR (join (map h mx'))) )
>  ={ cong {f = \ α => lhs' <+> meas (map sumR α)} (sym (bindJoinLemma mx' h)) }=
>    ( lhs' <+> meas (map sumR (mx' >>= h)) )  
>  ={ Refl }=
>    ( meas (map (r . head) (mx' >>= h)) <+> meas (map sumR (mx' >>= h)) )    
>  ={ sym (measPlusLemma (r . head) sumR (mx' >>= h)) }=
>    ( meas (map ((r . head) <++> sumR) (mx' >>= h)) )  
>    -- sumRLemma: map sumR (map ((MkSigma x y) ##) xyss) 
>    --              = 
>    --            map ((r . head) <++> sumR) xyss
>  ={ cong (sym (sumRLemma {m = m} x y (mx' >>= h))) }= 
>  -- ={ ?kika }=  
>    ( meas (map sumR (map {A = StateCtrlSeq (S t) (S m)} ((MkSigma x y) ##) 
>           (mx' >>= trj ps))) )
>  ={ Refl }=
>    ( meas (map sumR (trj (p :: ps) x)) )
>  ={ Refl }=
>    ( val' (p :: ps) x )
>  QED


# Auxiliary results

> -- map (const c) ma = map (const c) (map f ma)
> mapConstLemma c ma f = ( map (const c) ma )
>                      ={ Refl }=
>                        ( map ((const c) . f) ma )
>                      ={ mapPresComp ma f (const c) }=
>                        ( map (const c) (map f ma) )
>                      QED

> -- (v : Val) -> meas (pure v) = v
> measRetLemma v = ( meas (pure v) )
>                ={ cong (sym (pureNatTrans v (const v))) }=
>                  ( meas (map (const v) (pure v)) )
>                ={ measConstLemma v (pure v) }=
>                  ( v )
>                QED

> -- map f (join (map g ma)) = join (map (map f . g) ma)
> mapJoinLemma f g ma = ( map f (join (map g ma)) )
>                     ={ joinNatTrans (map g ma) f }=
>                       ( join (map (map f) (map g ma)) )
>                     ={ Refl }=
>                       ( join ((map (map f) . (map g)) ma) )
>                     ={ cong (sym (mapPresComp ma g (map f))) }=
>                       ( join (map (map f . g) ma) )
>                     QED

> -- map head (trj ps x) = map (const x) (trj ps x) 
> mapHeadLemma {t} {n = Z} Nil x 
>   = ( map head (trj Nil x) )
>   ={ Refl }=
>     ( map head (pure (Last x)) )
>   ={ pureNatTrans (Last x) head }=  
>     ( pure (head (Last x)) )
>   ={ Refl }=
>     ( pure x )
>   ={ sym (pureNatTrans {A = StateCtrlSeq t (S Z)} (Last x) (const x)) }=
>     ( map (const x) (pure (Last x)) )    
>   ={ Refl }=
>     ( map (const x) (trj Nil x) )
>   QED
> mapHeadLemma {t} {n = S m} (p :: ps) x 
>   = let y     =  p x in
>     let xy    =  MkSigma x y in
>     let mx'   =  next t x y in
>     let xyss  =  (mx' >>= trj ps) in 
>     ( map head (trj (p :: ps) x) )
>   ={ Refl }=
>     ( map head (map {B = StateCtrlSeq t (S (S m))} (xy ##) xyss) )
>   ={ sym (mapPresComp {B = StateCtrlSeq t (S (S m))} xyss (xy ##) head) }=
>     ( map (head . (xy ##)) xyss )
>   ={ Refl }=
>     ( map (const x) xyss )
>   ={ mapConstLemma {B = StateCtrlSeq t (S (S m))} x xyss (xy ##) }=
>     ( map (const x) (map {B = StateCtrlSeq t (S (S m))} (xy ##) xyss) )
>   ={ Refl }=
>     ( map (const x) (trj (p :: ps) x) )
>   QED                                

> -- (meas . (map (f . head) . (trj ps))) x = f x            
> measMapHeadLemma {t} {n} ps f x 
>   = ( (meas . (map (f . head) . (trj ps))) x )
>   ={ Refl }=
>     ( meas (map (f . head) (trj ps x)) )
>   ={ cong (mapPresComp (trj ps x) head f) }=
>     ( meas (map f (map head (trj ps x))) )
>   ={ cong {f = \ α => meas (map f α)} (mapHeadLemma ps x) }=
>     ( meas (map f (map (const x) (trj ps x))) )
>   ={ cong (sym (mapPresComp (trj ps x) (const x) f)) }=
>     ( meas (map (f . (const x)) (trj ps x)) )
>   ={ Refl }=
>     ( meas (map (const (f x)) (trj ps x)) )
>   ={ measConstLemma (f x) (trj ps x) }=
>     ( f x )
>   QED

> -- meas (map (f . head) (mx >>= trj ps)) = meas (map f mx)
> measMapHeadLemma' {t} {n} ps mx f
>   = let g  = trj ps in
>     ( meas (map (f . head) (mx >>= g)) )
>   ={ cong {f = \ α => meas (map (f . head) α)} (bindJoinLemma mx g) }= 
>     ( meas (map (f . head) (join (map g mx))) )
>   ={ cong (mapJoinLemma (f . head) g mx) }= 
>     ( meas (join (map (map (f . head) . g) mx)) )
>   ={ measJoinLemma (map (map (f . head) . g) mx) }= 
>     ( meas (map meas (map (map (f . head) . g) mx)) )  
>   ={ cong (sym (mapPresComp mx (map (f . head) . g) meas) ) }= 
>     ( meas (map (meas . (map (f . head) . g)) mx) )  
>   ={ cong (mapPresExtEq (meas . (map (f . head) . (trj ps))) f (measMapHeadLemma ps f) mx) }= 
>     ( meas (map f mx) )
>   QED

> -- map sumR (map ((MkSigma x y) ##) xyss) = map (((reward t x y) . head) <++> sumR) xyss
> sumRLemma {t} {m} x y xyss 
>   = ( map sumR (map {A = StateCtrlSeq (S t) (S m)} ((MkSigma x y) ##) xyss) )
>   ={ sym (mapPresComp {A = StateCtrlSeq (S t) (S m)} xyss ((MkSigma x y) ##) sumR) }=
>     ( map {A = StateCtrlSeq (S t) (S m)} (sumR . ((MkSigma x y) ##)) xyss )
>   ={ Refl }=
>     ( map (((reward t x y) . head) <++> sumR) xyss )
>   QED
