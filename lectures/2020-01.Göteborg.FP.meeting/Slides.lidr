> module Slides

> %default total
> %auto_implicits off
> %access public export

> Domain : {A, B : Type} -> (f : A -> B) -> Type
> Domain {A} f = A

> ExtEq      :  {A, B : Type} -> (f, g : A -> B) -> Type
> ExtEq f g  =  (a : Domain f) -> f a = g a

> M  :  Type -> Type

> map  :  {A, B : Type} -> (A -> B) -> M A -> M B

> mapPresExtEq  :  {A, B : Type} ->  (f, g : A -> B) ->
>                  ExtEq f g -> ExtEq (map f) (map g)
