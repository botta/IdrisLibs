> module SequentialDecisionProblems.NaiveTheoryDeterministic

> import Syntax.PreorderReasoning

> import Sigma.Sigma
> import List.Properties
> import Fun.Predicates

> %default total
> %auto_implicits off
> %access public export

> infixr 7 ##
> infixr 7 <++>

> %hide head
> %hide lteRefl


# Decision process

> X    : (t : Nat) -> Type
> Y    : (t : Nat) -> (x : X t) -> Type
> next : (t : Nat) -> (x : X t) -> (y : Y t x) -> X (S t)


# Decision problem

> Val    : Type
> reward : (t : Nat) -> (x : X t) -> (y : Y t x) -> (x' : X (S t)) -> Val
> (<+>)  : Val -> Val -> Val
> zero   : Val
> (<=)   : Val -> Val -> Type


# Basic requirements for verified backwards induction

> lteRefl  : {a : Val} -> a <= a
> lteTrans : {a, b, c : Val} -> a <= b -> b <= c -> a <= c
> plusMon  : {a, b, c, d : Val} -> a <= b -> c <= d -> (a <+> c) <= (b <+> d)


# Helper functions

> (<++>) : {A : Type} -> (A -> Val) -> (A -> Val) -> A -> Val
> f <++> g = \ a => f a <+> g a


# Policies and policy sequences

> Policy : (t : Nat) -> Type
> Policy t = (x : X t) -> Y t x

> data PolicySeq : (t : Nat) -> (n : Nat) -> Type where
>   Nil  : {t : Nat} -> PolicySeq t Z
>   (::) : {t, n : Nat} -> Policy t -> PolicySeq (S t) n -> PolicySeq t (S n)


# The value of policy sequences (Bellman equation)

> val : {t, n : Nat} -> PolicySeq t n -> (x : X t) -> Val
> val {t}  Nil      x = zero
> val {t} (p :: ps) x = let y = p x in
>                       let x' = next t x y in
>                       (reward t x y <++> val ps) x'


# Optimality of policy sequences

> OptPolicySeq : {t, n : Nat} -> PolicySeq t n -> Type
> OptPolicySeq {t} {n} ps  =  (ps' : PolicySeq t n) -> (x : X t) -> val ps' x <= val ps x


# Optimality of extensions of policy sequences

> OptExt : {t, n : Nat} -> PolicySeq (S t) n -> Policy t -> Type
> OptExt {t} ps p  =  (p' : Policy t) -> (x : X t) -> val (p' :: ps) x <= val (p :: ps) x


# Bellman's 1957 principle of optimality

> Bellman  :  {t, n : Nat} -> 
>             (ps  :  PolicySeq (S t) n) -> OptPolicySeq ps ->
>             (p   :  Policy t)          -> OptExt ps p ->
>             OptPolicySeq (p :: ps)
>
> Bellman {t} ps ops p oep = opps where
>   opps (p' :: ps') x = 
>     let y'  =  p' x in
>     let x'  =  next t x y' in
>     let s1  =  plusMon lteRefl (ops ps' x') in -- val (p' :: ps') x <= val (p' :: ps) x
>     let s2  =  oep p' x in                     -- val (p' :: ps)  x <= val (p :: ps)  x
>     lteTrans s1 s2


# Verified backwards induction:

First, we assess that the empty policy sequence is optimal

> nilOptPolicySeq : OptPolicySeq Nil
> nilOptPolicySeq Nil x = lteRefl

Now, provided that we can implement

> optExt     : {t, n : Nat} -> PolicySeq (S t) n -> Policy t
> optExtSpec : {t, n : Nat} -> (ps : PolicySeq (S t) n) -> OptExt ps (optExt ps)

then

> bi : (t : Nat) -> (n : Nat) -> PolicySeq t n
> bi t  Z     =  Nil
> bi t (S n)  =  let ps = bi (S t) n in 
>                optExt ps :: ps

is correct "by construction"

> biLemma : (t : Nat) -> (n : Nat) -> OptPolicySeq (bi t n)
> biLemma t  Z     =  nilOptPolicySeq
> biLemma t (S n)  =  let ps  = bi (S t) n in
>                     let ops = biLemma (S t) n in
>                     let p   = optExt ps in
>                     let oep = optExtSpec ps in
>                     Bellman ps ops p oep


# Trajectories and consistency of |val|

We want to show that |val ps x| does indeed compute the sum of the
rewards obtained along the trajectory that is obtained under the policy
sequence |ps| when starting in |x|. To this end, we start by defining
sequences of state-control pairs

> data StateCtrlSeq : (t : Nat) -> (n : Nat) -> Type where
>   Last  :  {t : Nat} -> (x : X t) -> StateCtrlSeq t (S Z)           
>   (##)  :  {t, n : Nat} -> Sigma (X t) (Y t) -> StateCtrlSeq (S t) n -> StateCtrlSeq t (S n)

and a function |sumR| that computes the sum of the rewards of a
state-control sequence:

> head : {t, n : Nat} -> StateCtrlSeq t (S n) -> X t
> head (Last x)             = x
> head (MkSigma x y ## xys) = x  

> sumR : {t, n : Nat} -> StateCtrlSeq t n -> Val
> sumR {t} {n = S Z}     (Last x)             = zero
> sumR {t} {n = S (S m)} (MkSigma x y ## xys) = reward t x y (head xys) <+> sumR xys

Next, we implement a function that computes the trajectory that is
obtained under a policy sequence |ps| when starting in |x|:

> trj : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> StateCtrlSeq t (S n)
> trj {t}  Nil      x  =  Last x
> trj {t} (p :: ps) x  =  let y  = p x in
>                         let x' = next t x y in                            
>                         (MkSigma x y) ## trj ps x'

Finally, we compute the measure of the sum of the rewards obtained along
the trajectory that is obtained under the policy sequence |ps| when
starting in |x|

> val' : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> Val
> val' ps x = sumR (trj ps x)

Now we can formulate the property that |val ps| does indeed compute the
measure of the sum of the rewards obtained along the trajectories
obtained under the policy sequence |ps|:

< valVal'Th : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
<             val ps x = val' ps x 

With 

> headLemma : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
>             head (trj ps x) = x
> headLemma  Nil      x = Refl
> headLemma (p :: ps) x = Refl

we can prove the |val|-|val'| theorem by induction on |ps|:

> valVal'Th : {t, n : Nat} -> (ps : PolicySeq t n) -> (x : X t) -> 
>             val ps x = val' ps x 
> valVal'Th      Nil      x = Refl
> valVal'Th {t} (p :: ps) x = 
>   let y = p x in
>   let x' = next t x y in
>     ( val (p :: ps) x )
>   ={ Refl }=
>     ( reward t x y x' <+> val ps x' )
>   ={ cong (valVal'Th ps x') }=
>     ( reward t x y x' <+> val' ps x' )
>   ={ cong {f = \ α => reward t x y α <+> val' ps x'} (sym (headLemma ps x')) }=
>     ( reward t x y (head (trj ps x')) <+> val' ps x' )
>   ={ Refl }=
>     ( sumR ((MkSigma x y) ## trj ps x') )
>   ={ Refl }=
>     ( val' (p :: ps) x )
>   QED


> {-

> ---}
