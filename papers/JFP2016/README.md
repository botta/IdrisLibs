This repository contains the Idris code presented in [1]. The code has
been derived from
[SequentialDecisionProblems](https://gitlab.pik-potsdam.de/botta/IdrisLibs/tree/master/SequentialDecisionProblems).
It has been type checked (make all) with Idris 0.12.2-git:PRE (ff776da).

This work has been partially supported by [GRACeFUL
   project](https://www.graceful-project.eu/) (640954, from the call
   H2020-FETPROACT-2014). [CoeGSS project](http://coegss.eu/) (676547,
   H2020-EINFRA-2015-1) in the context of Global Systems Science (GSS).

---

[1] Contributions to a computational theory of policy advice and
    avoidability. Nicola Botta, Patrik Jansson, Cezar Ionescu.

* 2016-01-06: Submitted to the Journal of Functional Programming (JFP)
  Special issue on Dependently Typed Programming. (JFP is a [RoMEO Green
  journal](http://www.sherpa.ac.uk/romeo/search.php?issn=0956-7968).)

* [Full text pre-print available](http://www.cse.chalmers.se/~patrikj/papers/CompTheoryPolicyAdviceAvoidability_JFP_2016_preprint.pdf)

* 2016-07: Review verdict: "Reject and resubmit"

* 2016-11: Revised manuscript [1] resubmitted


