> module Real.Real


> %default total
> %access public export


* Reals

> ||| Real numbers
> Real : Type 
> Real = Double
