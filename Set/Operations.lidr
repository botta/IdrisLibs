> module Set.Operations

> import Data.List

> import Set.SetAsList
> import Sigma.Sigma
> import Sigma.Operations
> import List.Predicates
> import List.Operations
> import List.Properties

> %default total
> %access public export
> %auto_implicits off


> ||| Equality test for sets as lists without duplicates
> eqeq : {A : Type} -> (DecEq A) => SetAsList A -> SetAsList A -> Bool
> eqeq (MkSigma  Nil      _) (MkSigma  Nil      _) =  True 
> eqeq (MkSigma  Nil      _) (MkSigma (y :: ys) _) = False
> eqeq (MkSigma (x :: xs) _) (MkSigma  Nil      _) = False
> eqeq (MkSigma (x :: xs) p) (MkSigma (y :: ys) q) = 
>   if (elem x (y :: ys) && elem y (x :: xs))
>   then assert_total (eqeq (MkSigma xs (tailHasNoDuplicates x xs p)) (MkSigma ys (tailHasNoDuplicates y ys q)))
>   else False

> -- implementation (DecEq A) => Eq (SetAsList A) where
> --   (==) = eqeq

We should probably introduce an equivalence relation at the type level,
prove its decidability and then implement an equality test consistent
with the type-level equivalence.

An equivalence relation would also be helpful for formulating (and
perhaps also implementing) obvious properties of set-theoretical
operations on sets as lists.

> ||| Computes the intersection of two lists without duplicates on the basis 
> ||| of |intersect| for lists. At some point, we should probable implement 
> ||| a version with full garantees.
> intersect : {A : Type} -> (Eq A) => SetAsList A -> SetAsList A -> SetAsList A
> intersect {A} X Y = MkSigma xs hndxs where
>   xs : List A
>   xs = intersect (outl X) (outl Y)
>   postulate hndxs : HasNoDuplicates xs


