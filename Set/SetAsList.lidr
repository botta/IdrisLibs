> module Set.SetAsList

> import Data.List

> import Sigma.Sigma
> import List.Predicates

> %default total
> %access public export
> %auto_implicits off


> SetAsList : Type -> Type
> SetAsList A = Sigma (List A) HasNoDuplicates


