> module ParameterUncertaintyDiscussion

> %default total
> %access public export
> %auto_implicits off

> Identity : Type -> Type
> Identity X = X
> Prob : Type -> Type
> mapProb : {A, B : Type} -> (A -> B) -> Prob A -> Prob B

Assume that we have a deterministic model that represents the time
discrete, autonomous evolution of a physical (economical, social, ...)
system under certain controls and parameters

 Climate = CO₂Air × CO₂U × CO₂L × AirT × OceT × IceVol 

 Sources = CO₂Emi × SO₂Inj

 Ctrl    = ΔCO₂Emi × SO₂Inj

 State   = Sources × Climate

 next    : State × Ctrl -> State

> V : Type  -- V = State

> C : Type  -- C = Ctrl

> P : Type  -- P = ...

> f : V -> C -> P -> V

> v0 : V

Conceptually, the values that the parameter can take at a given step
depend on the state of the system (and, perhaps, also on the control
chosen) at that step:

> p : V -> C -> P

In practice, we do not know |p| and we assume that, at every step, all
what we know about our parameter is given by a fixed probability
distribution:

> pd : Prob P

We have discussed *three* ways of accounting for the uncertainty encoded
by |pd| in a sequential decision process (SDP)

> M : Type -> Type

> X : Nat -> Type

> Y : (t : Nat) -> (x : X t) -> Type

> next : (t : Nat) -> (x : X t) -> Y t x -> M (X (S t))

where the transition function |next| is defined in terms of |f| and the
initial state is

> x0 : X Z

> {-

0) The simple minded way
========================

This is from Cezar's file:

  f : V × C × P → V

  p ∈ P ⇒ f p : V × C → V

  v₀, c₀   ⇒ v₁
  v₁, c₁   ⇒ v₂
  ...

  p is "uncertain" pd ∈ Prob P

  mapProb f pd : Prob (V × C → V)

This is the translation for our |f| which is in curried form. Perhaps
|h| should be better called |f'|:

> h : P -> (V -> C -> V)
> h p v c = f v c p

Now we can define |M|, |X|, etc.

> M = Prob

> X t = V

> Y t x = C

With |mapProb h pd| we obtain a probability distribution on functions of
type |V -> C -> V|. In order to compute a new state, we have to apply
these functions to the current state-control pair:

> next t v c = mapProb (\ g => g v c) (mapProb h pd) 

We see that |next t v c| is defined as in the standard approach (below) 

  next t v c
    ={ def. next }=
  mapProb (\ g => g v c) (mapProb h pd) 
    ={ mapProb pres. composition }=
  mapProb ((\ g => g v c) . h) pd
    ={ mapProb pres. ee + see below }=
  mapProb (f v c) pd 

because the functions |(\ g => g v c) . h| and |f v c| are extensionally
equal and |mapProb| preserves extensional equality:

  ((\ g => g v c) . h) p
    ={ def. . }= 
  (\ g => g v c) (h p)
    ={ eta }= 
  h p v c
    ={ def. of h }=
  f v c p

The initial state is also the same as in the standard approach. 

> x0 = v0 

Thus, if this computation is correct, we still have only three ways to
apply the current framework. 

But let's see what Marina and Nuria have implemented, perhaps we are
missing a fourth way!

> -}

1) The standard way
===================

> M = Prob

> X t = V

> Y t x = C

> standard     : next t v c = mapProb (f v c) pd : V -> Prob V 

> simple minded: next t v c : Prob (V, P) -> Prob (V, P) 


  pd = [(p1, a), (p2, 1 - a)]

  next t v c = [(f v c p1, a), (f v c p2, 1 - a)]

  f v c p1 is the solution of BEAM with initial value v, with constant parameter p1 and with input c

  [(v0, 1)], c0 -> [(f1 v0 c0, a), (f2 v0 c0, 1 - a)] 

                    f1 (f1 v0 c0) c1

                    f2 (f1 v0 c0) c1




> x0 = v0 


> {-

* Analysis of |next t|:

    next t : V -> C -> Prob V

    flip (next t) c : V -> Prob V

    sys      :      V -> Prob V

    repr sys : Prob V -> Prob V

    (repr sys) pv = bind pv sys   

    bind : Prob A -> (A -> Prob B) -> Prob B

    bind pa f = ...

    prob (b, bind pa f) = sum_{a ∈ pa} [prob (a, pa) * prob (b, f a)]

          P(b)          = sum_{a ∈ pa} [   P(a)      *    P(b | a)  ]

    prob (v', repr (flip (next t) c) pv) 
      =
    prob (v', bind pv (flip (next t) c)) 
      = 
    sum_{v ∈ pv} [prob (v, pv) * prob (v', flip (next t) c v)]
      = 
    sum_{v ∈ pv} [prob (v, pv) * prob (v', next t v c)]

  * Role of advisor and def. of |X| ans |Y|

  * Role of climate scientist (economist, etc.) and def. of |next|

  * The meaning of |pd|

  * |pd| as a function 


2) The way started by Nuria and Marina (perhaps)
================================================

< miracle : P

< M = Identity

< X Z  = V

< X (S t) = (V, P)

< Y Z x = Unit

< Y (S t) x = C

< next  Z     v     () = (v, miracle) 

< next (S t) (v, p)  c = (f v c p, p)

< x0 = v0 

This corresponds to a *deterministic* SDP in which the "true" value of
the parameter is revealed after the first (empty) decision step by a
"miracle". At each subsequent step, the decision maker observes a state
of the physical system and the revealed parameter value and has to
select a control.

The uncertainty about the unknown parameter of |f| is resolved by the
miracle:

< next (S t) : (V, P) -> C -> (V, P)

It seems to me that this approach is not very useful for practical
decision making:

* Who is supposed to bring about the miracle?

* Why do we carry a constant parameter in the "prognostic" variables?

* What can we say about the decisions obtained with optimal policy
  sequences for this problem? Their value will in general very much
  depend on the outcome of the miracle!


3) The way that (perhaps) corresponds to Michel's notion of optimal policy sequence?
====================================================================================

> M = Identity

> X t = Prob (V, P)

> Y t x = C

> next t pdvp c = mapProb g pdvp where
>   g : (V, P) -> (V, P)
>   g (v, p) = (f v c p, p)

> x0 = mapProb h pd where
>   h : P -> (V, P)
>   h p = (v0, p)

This corresponds to a *deterministic* SDP in which, at each decision
step, the decision maker observes a probability distribution on pairs
consisting of the state of the physical system and the parameter and has
to select a control.

This approach too is not very useful for practical decision making:

* The state space does not seem to represent plausible observations for
  a decision maker.

* We have a huge state space, most of which is unreachable.

Ciao,
Nicola

> ---}


Thu 24.9:
=========

 f p : E -> Prob E

 f p e = [(e + delta, p), (e, 1 - p)]
  
 parameter η ∈ {0, 1} = ETA (or η ∈ [0,1] = ETA)

 pd : Prob ETA

 f ETA : E -> E

 f η e = e + η * delta


Fri 25.9:
=========

 X : Nat -> Type

 Y : (t : Nat) -> (x : X t) -> Type

 next₁, next₂ : (t : Nat) -> (x : X t) -> Y t x -> X (S t)

 val₁ ps x = val next₁ ps x

 val₂ ps x = val next₂ ps x

 ps optimal₁ = ∀ x, ps'  val₁ ps' x <= val₁ ps x

 ps optimal₂ = ∀ x, ps'  val₂ ps' x <= val₂ ps x

---

Michel's (and perhaps Marina's) notion of optimality:

 α ∈ [0, 1], ps α-optimal 
  = 
 ∀ x, ps' (val₁ ps' x) * α + (val₂ ps' x) * (1 - α)  =  measVal α ps' x
          <=
          (val₁ ps  x) * α + (val₂ ps  x) * (1 - α)

