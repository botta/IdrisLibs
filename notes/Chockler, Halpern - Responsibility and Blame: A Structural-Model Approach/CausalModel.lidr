> module CausalModel

> import Data.Fin
> import Syntax.PreorderReasoning

> import Rel.ReachableFrom

> %default total
> %access public export
> %auto_implicits off

* Preliminaries

The notion of causal model introduced in section 2 relies on being able
to represent finite sets of variables where each variable is associated
with a non-empty set of possible values.

In Idris we can represent finite set of variables with vectors of
elements of type |Type|:

> Var : Nat -> Type
> Var n = Fin n -> Type

We will need to be able to require that the variables of a specific set
are associated with non-empty types

> Empty : Type -> Type
> Empty A = A -> Void

> NonEmptyVar : {n : Nat} -> Var n -> Type
> NonEmptyVar {n} v = (k : Fin n) -> Not (Empty (v k))

A possible assignment for |v : Var n| is then just a vector of possible
values, one for each variable in |v|:

> Val : (n : Nat) -> Var n -> Type
> Val n v = (k : Fin n) -> v k

In order to formalize the notion of causal model, we also need to
compare assignments for subsets of certain sets of variables. To this
end, it is useful to consider

> toUnit : {n : Nat} -> Var n -> List (Fin n) -> Var n
> toUnit v Nil = v 
> toUnit v (i :: is)  = toUnit (\ j => if j == i then Unit else v j) is 

This function takes a vector of |n| variables |v : Var n| and a list of
indexes. It computes a new vector of variables where all the types
corresponding to the elements of the list are set to the singleton type.

Thus, if |v1| and |v2| are assignments for |toUnit v is| that is

< v1, v2 : Val n (toUnit v is) 

we know for certain that |v1 i = v2 i| for |i <- is|. It is also useful
to define an "equal up to" relation between assignments

> eqUpTo : (n : Nat) -> (v : Var n) -> Fin n -> Val n v -> Val n v -> Type
> eqUpTo n v k x y = (j : Fin n) -> Not (j = k) -> x j = y j
 

* Causal model

A \emph{signature} is just a finite set of non-empty \emph{exogenous}
variables |U = Var m| and a finite set of non-empty \emph{endoogenous}
variables |V = Var n|:

> Signature : Nat -> Nat -> Type
> Signature m n = ((u : Var m ** NonEmptyVar u), (v : Var n ** NonEmptyVar v))

A \emph{causal model} over a signature |s| is just |s| together with a
function of type |F m n s|:

> F : (m : Nat) -> (n : Nat) -> Signature m n -> Type
> F m n ((u ** neu), (v ** nev)) = (k : Fin n) -> (x : Val m u) -> (y : Val n (toUnit v [k])) -> v k

> CausalModel : (m : Nat) -> (n : Nat) -> Type
> CausalModel m n = (s : Signature m n ** F m n s)

The idea is that, in a causal model |(s ** f)|, |f| defines how the
value of the |k|-th endogenous variable |v k| depends on the values of
the exogenous variables |x| and on the values of the endogenous
variables |v 0| ... |v (k - 1)| and |v (k + 1)| ... |v (n - 1)| for
arbitrary |k : Fin n|, |x : Val m u| and |y : Val n (toUnit v [k])|.

One can describe (some salient features of) a causal model in terms of a
\emph{causal network}. Let |FinRel m n| be the type of relations from
|Fin m| to |Fin n|

> FinRel : Nat -> Nat -> Type
> FinRel m n = Fin m -> Fin n -> Type

For a causal model |sf : CausalModel m n|, a value of type |DepOn m n
sf| is an endo-relation on |Fin n|:

> DepOn : (m : Nat) -> (n : Nat) -> CausalModel m n -> FinRel n n
> DepOn m n (((u ** neu), (v ** nev)) ** f)  =  \ i => \ j =>
>                                               (x : Val m u) ->
>                                               (y1 : Val n (toUnit v [j])) -> 
>                                               (y2 : Val n (toUnit v [j])) -> 
>                                               eqUpTo n (toUnit v [j]) i y1 y2 -> 
>                                               Not (y1 i = y2 i) -> 
>                                               Not (f j x y1 = f j x y2)

In other words, for |r : DepOn m n (((u ** neu), (v ** nev)) ** f)|, a
value of type |r i j| represents a proof that the endogenous variable |v
j| depends on the endogenous variable |v i| in the sanse that for any
(should instead be exists?) exogenous assigment |x| and for any
endogenous assignments |y1| and |y2| with unit values at |j|, it is
enough for |y1| and |y2| to differ in their |i|-th component for |f j x
y1| and |f j x y2| to differ.

This seems to be what is introduced in the first half of page 92 at the
beginning of section 2. Further, they restrict the attention to what
they call \emph{recursive} (causal) models. These are (causal) models
whose associate causal network is a \emph{directed acyclic graph}:


> mutual

>   data DAG : Type -> Type where
> 
>     Nil   :  {t : Type} -> 
>              DAG t
>            
>     Edge  :  {t : Type} -> 
>              (a : t) -> (b : t) -> (g : DAG t) -> 
>              Not (a = b) ->
>              Not (ReachableFrom g b a) ->
>              DAG t

>   graph : {t : Type} -> DAG t -> List (t, t)
>   graph  Nil            = Nil
>   graph (Edge a b g ne nr) = (a, b) :: graph g

>   ReachableFrom : {t : Type} -> DAG t -> t -> t -> Type
>   ReachableFrom g x y = RF (graph g) x y




> {-

The idea is that, for an empty |DAG|, |ReachableFrom| is the empty
relation. In a |DAG| that consists of an edge |(a, b)| together with a
|g : DAG|, |y| is reachable from |x| if one of the following conditions
occurs:

0) |y| is reachable from |x| in |g|,

1) |x = a| and |y = b|,

2) |x = a| and |y| is reachable from |b| in |g|,

3) |y = b| and |a| is reachable from |x| in |g| or

4) |a| is reachable from |x| in |g| and |y| is reachable from |b| in |g|:

>   ReachableFrom : {t : Type} -> DAG t -> t -> t -> Type
>   ReachableFrom  Nil               x y = Void
>   ReachableFrom (Edge a b g ne nr) x y =
>     Either (ReachableFrom g x y)
>            (Either (x = a, y = b)
>                    (Either (x = a, ReachableFrom g b y) 
>                            (Either (y = b, ReachableFrom g x a)
>                                    (ReachableFrom g b y, ReachableFrom g x a)
>                            )
>                    )
>            )

|ReachableFrom| is decidable:

> ||| If |P| and |Q| are decidable, |(P , Q)| is decidable
> decPair : {P, Q : Type} -> Dec P -> Dec Q -> Dec (P , Q)
> decPair (Yes p) (Yes q) = Yes (p , q)
> decPair (Yes p) (No nq) = No (\ pq => nq (snd pq))
> decPair (No np) (Yes q) = No (\ pq => np (fst pq))
> decPair (No np) (No nq) = No (\ pq => np (fst pq))

> decReachableFrom : {t : Type} -> (DecEq t) =>
>                    (g : DAG t) -> (x : t) -> (y : t) -> Dec (ReachableFrom g x y)
> decReachableFrom  Nil               x y = No absurd
> decReachableFrom (Edge a b g ne nr) x y with (decReachableFrom g x y)
>   | (Yes p0) = Yes (Left p0)
>   | (No  c0) with (decPair (decEq x a) (decEq y b))
>       | (Yes p1) = Yes (Right (Left p1))
>       | (No  c1) with (decPair (decEq x a) (decReachableFrom g b y))
>           | (Yes p2) = Yes (Right (Right (Left p2)))
>           | (No  c2) with (decPair (decEq y b) (decReachableFrom g x a))
>               | (Yes p3) = Yes (Right (Right (Right (Left p3))))
>               | (No  c3) with (decPair (decReachableFrom g b y) (decReachableFrom g x a))
>                   | (Yes p4) = Yes (Right (Right (Right (Right p4))))
>                   | (No  c4) = No contra where
>                       contra : ReachableFrom (Edge a b g ne nr) x y -> Void
>                       contra (Left p0) = c0 p0
>                       contra (Right (Left p1)) = c1 p1
>                       contra (Right (Right (Left p2))) = c2 p2
>                       contra (Right (Right (Right (Left p3)))) = c3 p3
>                       contra (Right (Right (Right (Right p4)))) = c4 p4

> {-
> decReachableFrom : {t : Type} -> (DecEq t) =>
>                    (g : DAG t) -> (x : t) -> (y : t) -> Dec (ReachableFrom g x y)
> decReachableFrom  Nil               x y = No absurd
> decReachableFrom (Edge a b g ne nr) x y with (decReachableFrom g x y)
>   | (Yes p0) = Yes (Case0 p0)
>   | (No  c0) with (decPair (decEq x a) (decEq y b))
>       | (Yes p1) = (Yes s2) where
>           s0 : ReachableFrom (Edge a b g ne nr) a b
>           s0 = Case1
>           s2 : ReachableFrom (Edge a b g ne nr) x y
>           s2 = ?lala
>       | (No  c1) = ?kika
> -}

and

> ReachableFromLemma : {t : Type} -> 
>                      (a : t) -> (b : t) -> (g : DAG t) -> 
>                      (ne : Not (a = b)) ->
>                      (nr : Not (ReachableFrom g b a)) ->
>                      (x : t) -> (y : t) -> 
>                      ReachableFrom g x y -> ReachableFrom (Edge a b g ne nr) x y 
> ReachableFromLemma a b g ne nr x y prf = Left prf

> NotReachableFromNilLemma : {t : Type} -> (DecEq t) => 
>                            (a : t) -> (b : t) -> Not (ReachableFrom Nil a b)
> NotReachableFromNilLemma a b = id
 
> mkEmpty : {t : Type} -> DAG t
> mkEmpty = Nil

> addEdge : {t : Type} -> (DecEq t) =>
>           (a : t) -> (b : t) -> (g : Maybe (DAG t)) -> Maybe (DAG t)
> addEdge a b  Nothing = Nothing
> addEdge a b (Just g) with (decEq a b)
>   | (Yes _) = Nothing
>   | (No ne) with (decReachableFrom g b a)
>       | (Yes _)     = Nothing
>       | (No nr) = Just (Edge a b g ne nr)

> char : {t : Type} -> (DecEq t) => DAG t -> t -> t -> Bool
> char Nil x y = False
> char (Edge a b g ne nr) x y with (decPair (decEq x a) (decEq y b))
>   | (Yes _) = True
>   | (No  _) = char g x y

> graph : {t : Type} -> DAG t -> List (t, t)
> graph  Nil            = Nil
> graph (Edge a b g ne nr) = (a, b) :: graph g

> mutual

>   conv : {t : Type} -> DAG t -> DAG t
>   conv Nil = Nil
>   conv (Edge a b g ne nr) = Edge b a (conv g) (ne . sym) (convLemma g b a nr)

>   convConvLemma : {t : Type} -> (g : DAG t) -> conv (conv g) = g
>   convConvLemma Nil = Refl
>   convConvLemma (Edge a b g ne nr) = ( conv (conv (Edge a b g ne nr)) )
>                                    ={ Refl }=
>                                      ( conv (Edge b a (conv g) (ne . sym) (convLemma g b a nr)) )
>                                    ={ Refl }=
>                                      ( Edge a b (conv (conv g)) ((ne . sym) . sym) (convLemma (conv g) a b (convLemma g b a nr)) )
>                                    ={ ?lala }=
>                                      ( (Edge a b g ne nr) )
>                                    QED

>   convLemma1 : {t : Type} -> 
>                (g : DAG t) -> (x : t) -> (y : t) -> 
>                ReachableFrom g x y -> ReachableFrom (conv g) y x 
>   convLemma1  Nil               x y r impossible
>   convLemma1 (Edge a b g ne nr) x y (Left p) = s2 where
>     s0 : ReachableFrom g x y
>     s0 = p
>     s1 : ReachableFrom (conv g) y x
>     s1 = convLemma1 g x y s0
>     s2 : ReachableFrom (conv (Edge a b g ne nr)) y x
>     s2 = ReachableFromLemma b a (conv g) (ne . sym) (convLemma g b a nr) y x s1
>   convLemma1 (Edge a b g ne nr) x y (Right (Left p1))                  = ?convLemma1h1
>   convLemma1 (Edge a b g ne nr) x y (Right (Right (Left p2)))          = ?convLemma1h2
>   convLemma1 (Edge a b g ne nr) x y (Right (Right (Right (Left p3))))  = ?convLemma1h3
>   convLemma1 (Edge a b g ne nr) x y (Right (Right (Right (Right p4)))) = ?convLemma1h4


>   convLemma : {t : Type} -> 
>               (g : DAG t) -> (x : t) -> (y : t) -> 
>               Not (ReachableFrom g x y) -> Not (ReachableFrom (conv g) y x) 
>   convLemma  Nil               x y ynrx = id
>   convLemma (Edge a b g ne nr) x y ynrx = contra where
>     ne'    : Not (b = a)
>     ne'    = \ p => ne (sym p)
>     nr'    : Not (ReachableFrom (conv g) a b)
>     nr'    = convLemma g b a nr
>     contra : Not (ReachableFrom (Edge b a (conv g) ne' nr') y x)
>     contra (Left p0) = ynrx s2 where
>       s0 : ReachableFrom (conv g) y x
>       s0 = p0
>       s1 : ReachableFrom g x y
>       s1 = ?kika
>       s2 : ReachableFrom (Edge a b g ne nr) x y
>       s2 = ReachableFromLemma a b g ne nr x y s1
>     contra (Right (Left p1)) = ?convLemmah1
>     contra (Right (Right (Left p2))) = ?convLemmah2
>     contra (Right (Right (Right (Left p3)))) = ?convLemmah3
>     contra (Right (Right (Right (Right p4)))) = ?convLemmah4




> showDAG : {t : Type} -> (Show t) => DAG t -> String
> showDAG = show . graph 

> using (t : Type)
>   implementation (Show t) => Show (DAG t) where
>     show = showDAG



> g0 : DAG Nat
> g0 = Nil

> g1 : DAG Nat
> g1 = Edge 0 1 Nil uninhabited (NotReachableFromNilLemma 0 1)

> g2 : Maybe (DAG Nat)
> g2 = addEdge 1 0 (addEdge 0 1 (Just Nil))

> g3 : Maybe (DAG Nat)
> g3 = addEdge 1 2 (addEdge 0 1 (Just Nil))

> g4 : Maybe (DAG Nat)
> g4 = addEdge 2 1 (addEdge 2 0 (addEdge 0 1 (Just Nil)))

> g5 : Maybe (DAG Nat)
> g5 = addEdge 1 2 (addEdge 2 0 (addEdge 0 1 (Just Nil)))

> g6 : Maybe (DAG Nat)
> g6 = addEdge 5 0 (addEdge 4 5 (addEdge 3 4 (addEdge 1 2 (addEdge 0 1 (Just Nil)))))

> g7 : Maybe (DAG Nat)
> g7 = addEdge 2 3 (addEdge 5 0 (addEdge 4 5 (addEdge 3 4 (addEdge 1 2 (addEdge 0 1 (Just Nil))))))

> ---}
 
