> module DynamicalSystems

> import Data.Vect

> %default total
> %access public export
> %auto_implicits off

> flow : {X : Type} -> (X -> X) -> Nat -> (X -> X)
> flow f   Z     =  id
> flow f  (S m)  =  f . flow f m

> trj : {X : Type} -> (X -> X) -> (n : Nat) -> X -> Vect (S n) X
> trj f  Z    x  =  x :: Nil
> -- trj f (S m) x  =  reverse ( f (head (trj f m x )) :: trj f m x )
> trj f (S m) x  =  x :: (trj f m . f) x

---

> joinList : {A : Type} -> List (List A) -> List A
> joinList  Nil         =  Nil
> joinList (xs :: xss)  =  xs ++ joinList xss

> sys : Nat -> List Nat
> sys n = [n, n + 1, n + 3]

> flowlist : (Nat -> List Nat) -> Nat -> Nat -> List Nat
> flowlist f n Z = [n]
> flowlist f nat (S n) =  joinList (map  f (flowlist f nat n)  )

> flowList : {X : Type} -> (X -> List X) -> Nat -> (X -> List X)
> flowList f  Z     =  pure
> flowList f (S n)  =  join . map  f . flowList f n  

> flowM : {X : Type} -> {M : Type -> Type} -> Monad M =>
>         (X -> M X) -> Nat -> (X -> M X)
> flowM f  Z     =  pure
> flowM f (S n)  =  join . map  f . flowM f n  

> infixr 1 >=>
> infixr 1 <=<

> infixr 1 #

> (>=>) : {A, B, C : Type} -> {M : Type -> Type} -> Monad M => 
>         (A -> M B) -> (B -> M C) -> A -> M C
> f >=> g = join . map g . f

> (<=<) : {A, B, C : Type} -> {M : Type -> Type} -> Monad M => 
>         (B -> M C) -> (A -> M B) -> A -> M C
> g <=< f = join . map g . f

> (#) : {A, B, C : Type} -> {M : Type -> Type} -> Monad M => 
>       (B -> M C) -> (A -> M B) -> A -> M C
> g # f = join . map g . f
