> module Bush

> -- import Data.Fin
> import Syntax.PreorderReasoning


> %default total
> %access public export
> %auto_implicits off


> map : {A, B : Type} -> (A -> B) -> List A -> List B
> map f  Nil      = Nil
> map f (a :: as) = f a :: map f as


> sum : List Nat -> Nat
> sum Nil = Z
> sum (a :: as) = 1 + sum as


> fold : {A : Type} -> {B : Type} -> 
>        (base : B) -> 
>        (step : A -> B -> B) -> 
>        (as : List A) -> B
> fold base step  Nil      = base
> fold base step (a :: as) = step a (fold base step as)


> map' : {A, B : Type} -> (A -> B) -> List A -> List B
> map' f = fold Nil (\ a => \ bs => f a :: bs)


> sum' : List Nat -> Nat
> sum' = fold Z (\ a => \ b => a + b)


> mapLemma : {A, B : Type} -> 
>            (f : A -> B) -> (as : List A) -> map' f as = map f as
> mapLemma  f  Nil = ( map' f Nil )
>                  ={ Refl }=
>                    ( fold Nil (\ a => \ bs => f a :: bs) Nil )
>                  ={ Refl }=
>                    ( Nil )
>                  ={ Refl }=
>                    ( map f Nil )
>                  QED
> mapLemma  f (a :: as) = ( map' f (a :: as) )
>                       ={ Refl }=
>                         ( fold Nil (\ a => \ bs => f a :: bs) (a :: as) )
>                       ={ Refl }=
>                         ( (\ a => \ bs => f a :: bs) a (fold Nil (\ a => \ bs => f a :: bs) as) )
>                       ={ Refl }=
>                         ( f a :: fold Nil (\ a => \ bs => f a :: bs) as )
>                       ={ Refl }=
>                         ( f a :: map' f as )
>                       ={ cong (mapLemma f as) }=
>                         ( f a :: map f as )
>                       ={ Refl }=
>                         ( map f (a :: as) )
>                       QED


> induction : {A : Type} -> {P : List A -> Type} ->
>             (base : P Nil) -> 
>             (step : (a : A) -> {as : List A} -> P as -> P (a :: as)) -> 
>             (as : List A) -> P as
> induction base step  Nil      = base
> induction base step (a :: as) = step a (induction base step as)


> mapLemma' : {A, B : Type} -> 
>             (f : A -> B) -> (as : List A) -> map' f as = map f as
> mapLemma' {A} f  as = induction {A = A} {P = \ as => map' f as = map f as} base step as where
>   base : map' f Nil = map f Nil
>   base = Refl
>   step : (a : A) -> {as : List A} -> map' f as = map f as -> map' f (a :: as) = map f (a :: as)
>   step a ih = cong ih

...

> data Bush : Type -> Type where
>   NilBush  : {A : Type} -> Bush A
>   ConsBush : {A : Type} -> A -> Bush (Bush A) -> Bush A


> mapBush : {A, B : Type} -> (A -> B) -> Bush A -> Bush B
> mapBush f  NilBush         = NilBush
> mapBush f (ConsBush a bas) = ConsBush (f a) (mapBush (mapBush f) bas)


> foldBush : {A : Type} -> {B : Type} -> 
>            (base : B) -> 
>            (step : A -> Bush B -> B) -> 
>            (as : Bush A) -> B
> foldBush base step  NilBush         = base
> foldBush base step (ConsBush a bas) = step a (mapBush (foldBush base step) bas)


> hfoldBush : {A : Type} -> {B : Type -> Type} -> 
>             (base : {T : Type} -> B T) -> 
>             (step : {T : Type} -> T -> B (B T) -> B T) -> 
>             (as : Bush A) -> B A
> hfoldBush base step  NilBush         = base
> hfoldBush base step (ConsBush a bas) = 
>   step a (hfoldBush base step (mapBush (hfoldBush base step) bas))





> {-

> ---}




