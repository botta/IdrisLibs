> module NaturalTransformation.Predicates

> import Syntax.PreorderReasoning

> import Fun.Predicates

> %default total
> %auto_implicits off
> %access public export


In category theory, a natural transformation η between two functors F
and G between categories 𝔸 and 𝔹is a family of arrows in 𝔹indexed by
objects in 𝔸such that

       F f   
  F X -----> F Y
   |          |
   | η X      | η Y
   v          v
  G X -----> G Y
       G f
   
commutes:

  (1)  (G f) . (η X) = (η Y) . (F f)

In Idris, we can express the naturality condition between two endo
functors with

> NatTrans : {F, G : Type -> Type} -> (Functor F, Functor G) => ({T : Type} -> F T -> G T) -> Type
> NatTrans eta = {X, Y : Type} -> (f : X -> Y) -> ExtEq (map f . eta) (eta . map f)

In the above definition |ExtEq f g| posits that |f| and |g| are
extensionally equal, see "IdrisLibs/Fun/Predicates.list". We can use
|NatTrans| to posit and prove that |listToMaybe| as defined in
"Prelude/List.idr"

< ||| Either return the head of a list, or `Nothing` if it is empty.
< listToMaybe : List a -> Maybe a
< listToMaybe []      = Nothing
< listToMaybe (x::xs) = Just x 

is a natural transformation between |List| and |Maybe|:

< listToMaybeIsNatTrans : NatTrans {F = List} {G = Maybe} listToMaybe
< listToMaybeIsNatTrans f Nil = Refl
< listToMaybeIsNatTrans f (x :: xs) = Refl

In this exaple, we have given the implicit arguments |F| and |G| of
|NatTrans| explicitly for the sake of clarity. But

> listToMaybeIsNatTrans : NatTrans listToMaybe
> listToMaybeIsNatTrans f Nil = Refl
> listToMaybeIsNatTrans f (x :: xs) = Refl

would have worked as well. Unfortunately, we cannot use |NatTrans| to
prove (or even posit) the naturality of arbitrary transformations. We
can convince the type checker that if |F| is a functor then |F . F| is
also a functor using named implementations:

> using (F : Type -> Type)
>   implementation [FunctorComposition] Functor F => Functor (F . F) where
>     map = map . map  

But trying to express that |join| for lists is a natural transformation 

< using implementation FunctorComposition
<   joinIsNatTrans : NatTrans {F = List . List} {G = List} join

fails to type check, see github.com/idris-lang/Idris-dev/issues/4793. An
alternative approach would be to define functors via records. This is
the infamous |Unctor| pattern:

> record Unctor where
>   constructor MkUnctor
>   omap : Type -> Type  
>   fmap : {X, Y : Type} -> (X -> Y) -> omap X -> omap Y

< NatTrans' : (F, G : Unctor) -> ({T : Type} -> omap F T -> omap G T) -> Type
< NatTrans' F G eta = {X, Y : Type} -> (f : X -> Y) -> ExtEq (fmap G f . eta) (eta . fmap F f)

In |NatTrans'|, we can make |F| and |G| implicit as in |NatTrans|:

> NatTrans' : {F, G : Unctor} -> ({T : Type} -> omap F T -> omap G T) -> Type
> NatTrans' {F} {G} eta = {X, Y : Type} -> (f : X -> Y) -> ExtEq (fmap G f . eta) (eta . fmap F f)

but we still need to give explicit arguments to the projections |fmap|
and |omap|. This is perhaps actually better than the original 

< NatTrans : {F, G : Type -> Type} -> (Functor F, Functor G) => ({T : Type} -> F T -> G T) -> Type
< NatTrans eta = {X, Y : Type} -> (f : X -> Y) -> ExtEq (map f . eta) (eta . map f)

and suggests that perhaps |Functor|, |Applicative|, |Monad|, etc. should
be better implemented as records rather than interfaces. With records,
we can seamlessly show that |listTomaybe| is a natural transformation

> listToMaybeIsNatTrans' : NatTrans' {F = MkUnctor List map} {G = MkUnctor Maybe map} listToMaybe
> listToMaybeIsNatTrans' f  Nil      = Refl
> listToMaybeIsNatTrans' f (x :: xs) = Refl

> joinIsNatTrans' : NatTrans' {F = MkUnctor (List . List) (map . map)} {G = MkUnctor List map} join
> joinIsNatTrans' f Nil = Refl
> joinIsNatTrans' f (xs :: xss) = ( (map f . join) (xs :: xss) )
>                               ={ Refl }=
>                                 ( map f (xs ++ (join xss)) ) 
>                               ={ mapAppendDistributive f xs (join xss) }= 
>                                 ( map f xs ++ map f (join xss) )
>                               ={ cong (joinIsNatTrans' f xss) }=
>                                 ( map f xs ++ join (map (map f) xss) )
>                               ={ Refl }=
>                                 ( (join . map (map f)) (xs :: xss) )
>                               QED

Notice that, both in |listToMaybeIsNatTrans'| and in |joinIsNatTrans'| (and
in contrast to |listToMaybeIsNatTrans|), the implicit arguments |F| and |G|

> {-

> ---}
