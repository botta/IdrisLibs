> module Basic.Predicates

> %default total
> %access public export
> %auto_implicits off


> Empty : Type -> Type
> Empty A = A -> Void
