> module Basic.Thoughts

> import Syntax.PreorderReasoning

> import Fun.Predicates

> %default total
> %access public export
> %auto_implicits off

> infix 6 <=>


In pure functional programming languages, function application preserves
intensional (propositional, syntactical, etc.) equality:

  (1)  ∀ h : X -> Y, ∀ x, x' : X, x = x' ⇒ h x = h x'

we can encode this property for an arbitrary function |h|
through a |presEq h| type:

> presEq : {X, Y : Type} -> (h : X -> Y) -> Type
> presEq h = (x, x' : Domain h) -> x = x' -> h x = h x'

With this notation, (1) can be formulated as

> anyPresEq :  {X, Y : Type} -> (h : X -> Y) -> presEq h
> anyPresEq h x x Refl = Refl

In Idris, |anyPresEq| is called congruence. It is made available in
Prelude.Basics via a function |cong|

< cong : (x = x') -> h x = h x'

where |x| and |x'| and the types of |x|, |x'| and |h| are
implicit. Because any function preserves intensional equality, any
higher order function preserves intensional equality. Thus, if |g, g' :
A -> B| are intensionally equal and |h : (A -> B) -> Y|, |h g| and |h
g'| are intensionally equal.

In particular, if |Y = C -> D|, |h| maps intensionally equal functions
to intensionally equal function. What about extensional equality

  (2)  g ≡ g' = ∀ a ∈ Dom g, g a = g' a

? We can encode extensional equality as a property of function pairs

> (<=>) : {A, B : Type} -> (g, g' : A -> B) -> Type
> g <=> g' = (a : Domain g) -> g a = g' a

In intensional type theories and thus in Idris, intensional equality
implies extensional equality

> EqEEq : {A, B : Type} -> (g, g' : A -> B) -> g = g' -> g <=> g' 
> EqEEq g g Refl a = Refl

but not the other way round. Therefore, if |g, g' : A -> B| are
intensionally equal they are also equal extensionally and for every |h :
(A -> B) -> (C -> D)|, |h g| and |h g'| are equal both intensionally and
extensionally.

What if |g, g' : A -> B| are extensionally equal but not necessarily
intensionally equal? Does any function |h : (A -> B) -> (C -> D)|
preserve extensional equality? Or perhaps even obtain intensional
equality? Does

  (1')  ∀ h : (A -> B) -> (C -> D), ∀ g, g' : A -> B, g ≡ g' ⇒ h g ≡ h g'

hold in Idris? We can formulate the question precisely by formalizing
the notion of preservation of extensional equality:

> presEEq : {A, B, C, D : Type} -> (h : (A -> B) -> (C -> D)) -> Type
> presEEq h = (g, g' : Domain h) -> g <=> g' -> h g <=> h g'

We can give a few examples of functions that preserve extensional
equality. Trivially, the identity function and the constant function do
so. Pre-composition also preserves extensional equality:

> preComp : {A, B, C : Type} -> (f : B -> C) -> (g : A -> B) -> A -> C
> preComp f g = f . g 

> preCompPresEEq : {A, B, C : Type} -> (f : B -> C) -> presEEq (preComp f)
> preCompPresEEq f g g' p a = ( ((preComp f) g) a )
>                           ={ Refl }=
>                             ( f (g a) )
>                           ={ anyPresEq f (g a) (g' a) (p a) }=
>                             ( f (g' a) )
>                           ={ Refl }=
>                             ( ((preComp f) g') a )
>                           QED

And, symmetrically, post-composition:

> postComp : {A, B, C : Type} -> (g : A -> B) -> (f : B -> C) -> A -> C
> postComp = flip preComp 

> postCompPresEEq : {A, B, C : Type} -> (g : A -> B) -> presEEq (postComp g)
> postCompPresEEq g f f' p a = ( ((postComp g) f) a )
>                            ={ Refl }=
>                              ( f (g a) )
>                            ={ p (g a) }=
>                              ( f' (g a) )
>                            ={ Refl }=
>                              ( ((postComp g) f') a )
>                            QED

Perhaps not surprisingly, preservation of extensional equality also
holds for the |map| method of functors like |Identity|, |Maybe|, |List|,
|Prob|. 

Are there functions that do not preserve extensional equality?

Let |h g| with |g : A -> B| and |h : (A -> B) -> (C -> D)| be total. In
this case one would expect that, for a fixed |c : C| the computation of
|(h g) c| requires evaluating |g a| for a finite number of |a : A|. 

If we know that |g a = g' a| for any |a : A|, it seems plausible that |h
g| and |h g'|, if total, are extensionally equal. In fact one would
expect |h g| and |h g'| to be intensionally equal if we assume that all
what |h| can do with an argument |g| is to evaluate it for a finite
number of |a : Domain g|. Still, something like |anyPresEq| for the
extensional case

> anyPresEEq : {A, B, C, D : Type} -> (h : (A -> B) -> (C -> D)) -> presEEq h

does not seem to be implementable in Idris.

-- Tim 18.01.2020:

Let's look at the functor ( X -> _) for some type X 

> Repr : Type -> Type -> Type
> Repr X Y = X -> Y

Its |map| maps f to postComposition with f 

> mapRepr : (X : Type) -> {Y, Z : Type} -> (Y -> Z) -> (Repr X Y -> Repr X Z)
> mapRepr X f = \ h => f . h 

Can we show |presEEq (mapRepr X)| ?
 
< presEEq h = (g, g' : Domain h) -> g <=> g' -> h g <=> h g'

so

< presEEq (mapRepr X) = (g, g' : Domain (mapRepr X)) -> g <=> g' -> (mapRepr X) g <=> (mapRepr X) g'
<                     = (g, g' : Y -> Z) -> g <=> g' -> (\ f => g . f) <=> (\ f => g' . f)
<                     = (g, g' : Y -> Z) -> g <=> g' -> (f : X -> Y) -> g . f = g' . f

As shown in | postCompPresEEq | above, we can prove

<                     = (g, g' : Y -> Z) -> g <=> g' -> (f : X -> Y) -> g . f <=> g' . f

but unfortunately, this is not enough...

-- End Tim

Very nice! What is problem with |mapRepr X| not preserving extensional
equality?

Assuming that |mapRepr X| does preserve identity and composition, we
have a nice counterexample (for the conjecture that every functor also
preserves extensional equality) and a strong motivation for proposing a
refinement of the (|Veri|) |Functor|, |Applicative|, |Monad|,
etc. hierarchy that distinguishes between those functors whose |map|
preserves extensional equality (and for which we can implement generic
proofs of non-trivial properties) and those for which this is not the
case!

Given that we have at least three good examples of how to put such a
refinement at work, this would be a valuable contribution to a CS
journal, I think.

Any idea for how to call functors (and then applicative, monads, etc.)
that preserve extensional equality?

Ciao,
Nicola

> {-

> ---}
 
