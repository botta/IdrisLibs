> module VeriMonad.NotEmpty
  
> import VeriMonad.VeriMonad

> %default total
> %access public export
> %auto_implicits off

> using (M : Type -> Type)
> NotEmpty : VeriMonad M => {A : Type} -> M A -> Type

