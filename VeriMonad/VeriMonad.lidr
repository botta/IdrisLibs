> module VeriMonad.VeriMonad

> import Syntax.PreorderReasoning

> import Fun.Predicates
> import VeriFunctor.VeriFunctor

> %default total
> %access public export
> %auto_implicits off

> infixl 1 >>=


A Monad is an endo-functor M on a category ℂ together with two natural
transformations η and μ such that

       η (M X)             M (η X)
  M X ---------> M (M X) <--------- M X 
                    |
                    |
                    | μ X
                    |
                    |
                   M X

   (4)  (μ X) . (η (M X)) = id

   (5)  (μ X) . (M (η X)) = id

and

                  M (μ X)
     M (M (M X)) ---------> M (M X)
          |                  |
          |                  |
  μ (M X) |                  | μ X
          |                  |
          |                  |
       M (M X) -----------> M X
                  μ X

  (6)  (μ X) . (M (μ X)) = (μ X) . (μ (M X))

The naturality conditions for η and μ can be expressed as

  (2)  (M f) . (η X) = (η Y) . f --- (map f) . ret = ret . f

and

  (3)  (M f) . (μ X) = (μ (M X)) . (M (M f)) --- (map f) . join = join . (map (map f))

In Idris, η and μ are called |pure| and |join|, respectively 

  pure : {M : Type -> Type} -> {X : Type} -> Monad M => X -> M X 

  join : {M : Type -> Type} -> {X : Type} -> Monad M => M (M X) -> M X 

The Idris |Monad| interface from "Prelude/Monad.idr" provides default
implementations of bind (|>>=|) and |join| that fulfill the
specification

  (1)  >>= f = join . map f 

and the conditions (1-6) can be expressed through a |VeriMonad|
refinement of |VeriFunctor| and of |Monad|:

> ||| 
> interface (VeriFunctor M, Monad M) => VeriMonad (M : Type -> Type) where
> 
>   bindJoinMapSpec : {A, B : Type} -> (f : A -> M B) -> ExtEq (>>= f) (join . map f)
>
>   pureNatTrans    : {A, B : Type} -> (f : A -> B) -> ExtEq {B = M B} (map f . pure) (pure . f)
> 
>   joinNatTrans    : {A, B : Type} -> (f : A -> B) -> ExtEq {A = M (M A)} (map f . join) (join . map (map f))              
>
>   triangleLeft    : {A : Type} -> ExtEq {A = M A} (join . pure) id
>
>   triangleRight   : {A : Type} -> ExtEq {A = M A} (join . map pure) id         
>
>   square          : {A : Type} -> ExtEq {A = M (M (M A))} (join . map join) (join . join)         

For a |VeriMonad|, one can show that |>>=| preserves extensional equality

> ||| ∀ f, ∀ g, (∀ a, f a = g a) => (∀ ma, map f ma = map g ma)
> bindPresExtEq : {M : Type -> Type} -> {A, B : Type} -> (VeriMonad M) => 
>                 (f, g : A -> M B) -> ExtEq f g -> ExtEq (>>= f) (>>= g) 
>                 
> bindPresExtEq f g eeqfg ma = ( ma >>= f ) 
>                            ={ bindJoinMapSpec f ma }=
>                              ( join (map f ma) )
>                            ={ cong (mapPresExtEq f g eeqfg ma) }=
>                              ( join (map g ma) )   
>                            ={ sym (bindJoinMapSpec g ma) }=
>                              ( ma >>= g ) 
>                            QED

, the monadic laws

> ||| ∀ f, ∀ a,  (pure a) >>= f = f a
> leftIdentity : {M : Type -> Type} -> {A, B : Type} -> (VeriMonad M) => 
>                (f : A -> M B) -> ExtEq (\ a => (pure a) >>= f) f
>                
> leftIdentity f a = ( (pure a) >>= f )
>                  ={ bindJoinMapSpec f (pure a) }=
>                    ( join (map f (pure a)) )
>                  ={ cong {f = join} (pureNatTrans f a) }=
>                    ( join (pure (f a)) )
>                  ={ triangleLeft (f a) }=
>                    ( f a )
>                  QED


> ||| ∀ ma,  ma >>= pure = ma
> rightIdentity : {M : Type -> Type} -> {A : Type} -> (VeriMonad M) => 
>                 ExtEq {A = M A} (>>= pure) id
>                 
> rightIdentity ma = ( ma >>= pure )
>                  ={ bindJoinMapSpec pure ma }=
>                    ( join (map pure ma) )
>                  ={ triangleRight ma }=
>                    ( ma )
>                  QED            


> ||| ∀ f, ∀ g, ∀ ma,  (ma >>= f) >>= g = ma >>= (\ a => (f a) >>= g)
> associativity : {M : Type -> Type} -> {A, B, C : Type} -> (VeriMonad M) => 
>                 (f : A -> M B) -> (g : B -> M C) -> 
>                 ExtEq (\ ma => (ma >>= f) >>= g) (>>= (\ x => (f x) >>= g))
>                 
> associativity f g ma = ( (ma >>= f) >>= g )
>                      ={ bindJoinMapSpec g (ma >>= f) }=
>                        ( join (map g (ma >>= f)) )
>                      ={ cong {f = \ X => join (map g X)} (bindJoinMapSpec f ma) }=
>                        ( join (map g (join (map f ma))) )
>                      ={ cong (joinNatTrans g (map f ma)) }=
>                        ( join (join (map (map g) (map f ma))) )
>                      ={ sym (square (map (map g) (map f ma))) }=
>                        ( join (map join (map (map g) (map f ma))) )
>                      ={ Refl }=
>                        ( join (map join ((map (map g) . (map f)) ma)) )
>                      ={ cong {f = \ X => join (map join X)} (sym (mapPresComp f (map g) ma)) }=
>                        ( join (map join (map ((map g) . f) ma)) )
>                      ={ Refl }=
>                        ( join (((map join) . (map ((map g) . f))) ma) )
>                      ={ cong (sym (mapPresComp ((map g) . f) join ma)) }=
>                        ( join (map (join . ((map g) . f)) ma) ) 
>                      ={ Refl }=
>                        ( join (map (\ a => join (map g (f a))) ma) ) 
>                      ={ cong (mapPresExtEq (\ a => join (map g (f a))) 
>                                            (\ a => (f a) >>= g)
>                                            (\ a => sym (bindJoinMapSpec g (f a))) 
>                                            ma) }=
>                        ( join (map (\ a => (f a) >>= g) ma) )
>                      ={ sym (bindJoinMapSpec (\ a => (f a) >>= g) ma) }=
>                        ( ma >>= (\ a => (f a) >>= g) )
>                      QED

and some more generic results that turn out to be very usful in
applications, see for instance
"IdrisLibs/TimeDiscreteDynamicalSystems/Monadic.lidr":

> ||| ∀ f, ∀ g, ∀ ma,  map f (join (map g ma)) = join (map (map f . g) ma)
> mapJoinLemma : {M : Type -> Type} -> {A, B, C : Type} -> (VeriMonad M) => 
>                (f : B -> C) -> (g : A -> M B) -> 
>                ExtEq {A = M A} {B = M C} (map f . (join . map g)) (join . map (map f . g))
>
> mapJoinLemma f g ma = ( map f (join (map g ma)) )
>                     ={ joinNatTrans f (map g ma) }=
>                       ( join (map (map f) (map g ma)) )
>                     ={ Refl }=
>                       ( join ((map (map f) . (map g)) ma) )
>                     ={ cong (sym (mapPresComp g (map f) ma)) }=
>                       ( join (map (map f . g) ma) )
>                     QED


> ||| ∀ f, ∀ g, ∀ ma,  map f (ma >>= g) = ma >>= map f . g
> mapBindLemma : {M : Type -> Type} -> {A, B, C : Type} -> (VeriMonad M) => 
>                (f : B -> C) -> (g : A -> M B) -> 
>                ExtEq {A = M A} (\ ma => map f (ma >>= g)) (>>= map f . g)
>
> mapBindLemma f g ma = ( map f (ma >>= g) )
>                     ={ cong (bindJoinMapSpec g ma) }=
>                       ( map f (join (map g ma)) )
>                     ={ mapJoinLemma f g ma }=
>                       ( join (map (map f . g) ma) ) 
>                     ={ sym (bindJoinMapSpec (map f . g) ma) }=
>                       ( ma >>= map f . g )
>                     QED

> ||| ∀ f, ∀ g, ∀ ma, join (map (map f . g) ma  =  ma >>= map f . g 
> mapJoinMapBindLemma : {M : Type -> Type} -> {A, B, C : Type} -> (VeriMonad M) => 
>                       (f : B -> C) -> (g : A -> M B) -> 
>                       ExtEq (join . map (map f . g)) (>>= map f . g)
> mapJoinMapBindLemma f g ma = ( join (map (map f . g) ma) )
>                                ={ sym (mapJoinLemma f g ma) }=
>                              ( map f (join (map g ma)) )
>                                ={ cong (sym (bindJoinMapSpec g ma)) }=
>                              ( map f (ma >>= g) )
>                                ={ mapBindLemma f g ma }=
>                              ( ma >>= map f . g )
>                                QED


> {-               
                                                
> ---}

 
