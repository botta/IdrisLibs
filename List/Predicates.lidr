> module List.Predicates

> import Data.List

> %default total
> %access public export
> %auto_implicits off


> data HasNoDuplicates : {A : Type} -> List A -> Type where
>   NilHasNoDuplicates  : HasNoDuplicates Nil
>   ConsHasNoDuplicates : {A : Type} -> (a : A) -> (as : List A) -> Not (Elem a as) -> HasNoDuplicates as  -> HasNoDuplicates (a :: as)


> {-

> ---}
