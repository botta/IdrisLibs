> module Rel.ReachableFrom

> import Decidable.Properties

> %default total
> %access public export
> %auto_implicits off


> ||| |RF es x y| is an evidence that |y| is reachable from |x| in the 
> ||| graph (list of edges) |es|  
> data RF : {t : Type} -> List (t, t) -> t -> t -> Type where
> 
>   Case0 : {t : Type} -> {e : (t, t)} -> {es : List (t, t)} ->
>           {x : t} -> {y : t} -> 
>           {p : fst e = x} -> {q : snd e = y} -> 
>           RF (e :: es) x y
>
>   Case1 : {t : Type} -> {e : (t, t)} -> {es : List (t, t)} ->
>           {x : t} -> {y : t} ->
>           {p : fst e = x} -> {q : RF es (snd e) y} -> 
>           RF (e :: es) x y
>           
>   Case2 : {t : Type} -> {e : (t, t)} -> {es : List (t, t)} ->
>           {x : t} ->  {y : t} ->
>           {p : RF es x (fst e)} -> {q : snd e = y} -> 
>           RF (e :: es) x y
>          
>   Case3 : {t : Type} -> {e : (t, t)} -> {es : List (t, t)} ->
>           {x : t} -> {y : t} -> 
>           {p : RF es x (fst e)} -> {q : RF es (snd e) y} -> 
>           RF (e :: es) x y
>           
>   Case4 : {t : Type} -> {e : (t, t)} -> {es : List (t, t)} ->
>           {x : t} -> {y : t} -> 
>           {p : RF es x y} -> 
>           RF (e :: es) x y


> |||
> ReachableFrom : {t : Type} -> List (t, t) -> t -> t -> Type
> ReachableFrom = RF  


> using (t : Type, x : t, y : t)
>   implementation Uninhabited (RF {t} Nil x y) where
>     uninhabited prf impossible


> ||| |RF| is decidable
> decRF : {t : Type} -> (DecEq t) =>
>         (es: List (t, t)) -> (x : t) -> (y : t) -> Dec (RF es x y) 
> decRF  Nil      x y = No absurd
> decRF (e :: es) x y with (decPair (decEq (fst e) x) (decEq (snd e) y))
>   | (Yes p0) = Yes (Case0 {p = fst p0} {q = snd p0})
>   | (No  c0) with (decPair (decEq (fst e) x) (decRF es (snd e) y))
>       | (Yes p1) = Yes (Case1 {p = fst p1} {q = snd p1})
>       | (No  c1) with (decPair (decRF es x (fst e)) (decEq (snd e) y))
>           | (Yes p2) = Yes (Case2 {p = fst p2} {q = snd p2})
>           | (No  c2) with (decPair (decRF es x (fst e)) (decRF es (snd e) y))
>               | (Yes p3) = Yes (Case3 {p = fst p3} {q = snd p3})
>               | (No  c3) with (decRF es x y)
>                   | (Yes p4) = Yes (Case4 {p = p4})
>                   | (No  c4) = No contra where
>                       contra : RF (e :: es) x y -> Void
>                       contra (Case0 {p} {q}) = c0 (p, q)  
>                       contra (Case1 {p} {q}) = c1 (p, q)  
>                       contra (Case2 {p} {q}) = c2 (p, q)  
>                       contra (Case3 {p} {q}) = c3 (p, q)  
>                       contra (Case4 {p})     = c4 p


> |||
> decReachableFrom : {t : Type} -> (DecEq t) =>
>                    (es: List (t, t)) -> (x : t) -> (y : t) -> Dec (RF es x y) 
> decReachableFrom = decRF  


> {-

> ---}
