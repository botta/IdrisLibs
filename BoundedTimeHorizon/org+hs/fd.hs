import Data.Array

swap (a, b)      =  (b, a)
pair (f, g) x    =  (f x, g x)
cross (f, g)     =  pair (f . fst, g . snd)

fromMaybe (Just a) = a

maxBy gr [x]             =  x
maxBy gr (x1 : x2 : xs)  =  if gr(x1, x2) then maxBy gr (x1 : xs) else maxBy gr (x2 : xs)

update_fun f (x0, y0) x  =  if x == x0 then y0 else f x
updates_fun f xs  =  foldl update_fun f xs

g = [[Just 0, Nothing, Just 2, Nothing],
     [Just 10, Just 0, Just 5, Nothing],
     [Just 3, Nothing, Just 0, Just 2],
     [Just 10, Just 1, Nothing, Just 0]]

-- (g !! s) !! s' is just the cost of going from s to s' (or nothing)

newtype State = S Int deriving (Eq, Ord, Show, Ix)
newtype Ctrl  = C Int deriving (Eq, Ord, Show)
type    Val   = Int

instance Bounded State where
  minBound = S 0
  maxBound = S ((length g) - 1)

instance Enum State where
  succ (S x)      =  if S x < maxBound then S (x+1) else error "maxBound exceeded"
  pred (S x)      =  if S x > minBound then S (x-1) else error "minBound exceeded"
  toEnum n        =  if toInt minBound <= n && n <= toInt maxBound then S n else error (show n ++ " out of range")
  fromEnum (S x)  =  x

instance Bounded Ctrl where
  minBound = C 0
  maxBound = C ((length g) - 1)

instance Enum Ctrl where
  succ (C x)      =  if C x < maxBound then C (x+1) else error "maxBound exceeded"
  pred (C x)      =  if C x > minBound then C (x-1) else error "minBound exceeded"
  toEnum n        =  if toInt minBound <= n && n <= toInt maxBound then C n else error (show n ++ " out of range")
  fromEnum (C x)  =  x


toInt :: State -> Int
toInt (S x) = x

ctrls :: State -> [Ctrl]         -- allowed controls
ctrls (S s) = [c | c <- [minBound .. maxBound],
                   (g !! s) !! (fromEnum c) /= Nothing]

next :: State -> Ctrl -> State
next _ (C b)  =  (S b)

reward :: State -> Ctrl -> State -> Val
reward (S s) (C c) _ = fromMaybe ((g !! s) !! c)

(<+>) :: Maybe Val -> Val -> Maybe Val -- how to add rewards
Nothing <+> v   =  Nothing
(Just v) <+> w  =  Just (v + w)

-- Compare value table entries, problem specific
-- gr should be read as "strictly better entry" for the value table
gr_vt :: (Maybe Val, Maybe Val) -> Bool
gr_vt (Nothing, Nothing) =  False  -- Nothing is not strictly better than Nothing
gr_vt (Nothing, Just v)  =  False  -- Nothing is not strictly better than a value
gr_vt (Just v, Nothing)  =  True   -- A value is strictly better than Nothing
gr_vt (Just x, Just y)   =  x < y  -- problem specific: here we minimize costs, so smaller is better


-- Key function, that returns a change in the
-- policy and value tables for a given state

type Val_Pol_Change = ([(State, Maybe Val)], [(State, Maybe Ctrl)])

improve_fun :: (State -> Maybe Val, State -> Maybe Ctrl) -> State ->
               Val_Pol_Change
improve_fun (val, pol) s0 = val_pol_changes
  where
  cs = ctrls s0                       :: [Ctrl] -- allowed controls
  nexts = map (next s0) cs            :: [State] -- possible next states
  rs = map (\ (s, c) -> reward s0 c s) (zip nexts cs) :: [Val] -- rewards for possible transitions
  vs = map (\ (s, r) -> (val s) <+> r) (zip nexts rs) :: [Maybe Val] -- value of possible transitions
  (bestv, bestc) = maxBy (gr_vt . cross(fst, fst))
                   (zip vs cs) :: (Maybe Val, Ctrl) -- best value
  val_pol_changes = if gr_vt(bestv, val s0)
    then ([(s0, bestv)], [(s0, Just bestc)])
    else ([], []) :: Val_Pol_Change -- updates to value and policy tables

-- Table-based implementation

pointupdate :: s -> v -> c -> ([(s,v)],[(s,Maybe c)])
pointupdate s v c = ([(s,v)],[(s,Just c)])
noupdate = ([],[])

improve (vt, pt) s0 = val_pol_changes
  where
  cs = ctrls s0                                           :: [Ctrl] -- allowed controls
  nexts = map (next s0) cs                                :: [State] -- possible next states
  rs = map (\ (s, c) -> reward s0 c s) (zip nexts cs)     :: [Val] -- rewards for possible transitions
  vs = map (\ (s, r) -> vt ! s <+> r) (zip nexts rs)      :: [Maybe Val] -- value of possible transitions
  (bestv, bestc) = maxBy (gr_vt . cross(fst, fst))  (zip vs cs) :: (Maybe Val, Ctrl) -- best value
  val_pol_changes = if gr_vt(bestv, vt ! s0)
                       then pointupdate s0 bestv bestc
                       else noupdate                     -- updates to value and policy tables


improve_states :: (Table Val, Table Ctrl) -> [State] -> Val_Pol_Change
improve_states (vt, pt) ss  =  (vt_changes, pt_changes)
  where
  (vt_changes, pt_changes)  =  cross(concat, concat) $
                               unzip $ map (improve (vt, pt)) ss

update_tables (vt, pt) (vt_changes, pt_changes) = (vt', pt')
  where
  vt' = vt // vt_changes
  pt' = pt // pt_changes

------------------------------------------------------------

goals :: [State] -- list of starting points for backward induction
goals  = [S 0]


-- bellman_ford algorithm: best vt and pt that can be obtained allowing n steps

bf (vt, pt) n
  | n == 0                   =  (vt, pt)
  | n  > 0                   =  bf (vt', pt') (n-1)
  where
  (vt_changes, pt_changes)  =  improve_states (vt, pt) [minBound .. maxBound]
  (vt', pt') = update_tables (vt, pt) (vt_changes, pt_changes)

-- bellman_ford algorithm with indeterminate stop

bfi (vt, pt) = if vt == vt' && pt == pt' then (vt, pt) else bfi (vt', pt')
  where
  (vt_changes, pt_changes)  =  improve_states (vt, pt) [minBound .. maxBound]
  (vt', pt') = update_tables (vt, pt) (vt_changes, pt_changes)

-- backwards induction:
--   states are either done, being explored, or neither
--   goal states are done
--   a non-goal state is done if it has been explored and all its successors have been explored
--   initial states have no predecessors (that we care about)
--   at each step: for each state in the todo list
--                     improve the state
--                     add the predecessors to the new todo list
--                     if the state is done, add it to the done list
--   if the todo list is empty, we're done

inits = [S 3]

prevs :: State -> [State]
prevs s = if elem s inits
            then []
            else [s' | s' <- [minBound .. maxBound], not (null (filter (\ c -> next s' c == s) (ctrls s')))] -- deliberately general

succs s = map (next s) (ctrls s) -- deliberately general

alldone done s = if elem s goals then True else all (\ s' -> elem s' done) (filter (/= s) (succs s))

type Table a = Array State (Maybe a)
type Endo a = a -> a
step :: Endo ((Table Val, Table Ctrl), [State])
step ((vt, pt), todo) = ((vt', pt'), todo')
  where
  (vt_changes, pt_changes) = improve_states (vt, pt) todo
  (vt', pt') = update_tables (vt, pt) (vt_changes, pt_changes)
  todo' = concat (map prevs todo)
  -- done' = done ++ filter (alldone (done ++ visited)) todo
  -- todo' = filter (\ s -> not (alldone (done' ++ visited) s)) (concat (map prevs todo))
  -- visited' = todo ++ visited

step_done :: ((Table Val, Table Ctrl), [State]) -> Bool
step_done ((vt, pt), todo) = null todo

bi :: Endo ((Table Val, Table Ctrl), [State])
bi = until step_done step

bi' :: Goals -> (Table Val, Table Ctrl)
bi' = fst . bi . starting_point

type Goals = [State]
starting_point :: Goals -> ((Table Val, Table Ctrl), [State])
starting_point gs = ((vt0,pt0),gs)
  where  pt0 :: Array State (Maybe Ctrl)
         pt0 = array (minBound, maxBound) [(i, Nothing) | i <- [minBound .. maxBound]]

         vt0' :: Array State (Maybe Val)
         vt0' = array (minBound, maxBound) [(i, Nothing) | i <- [minBound .. maxBound]]

         vt0 = vt0' // [(s, Just 0) | s <- goals]

-----------------------------------------
-- "Online" algorithms: without ~succs~ and ~prevs~; ~next~ executes a state transition.
-- Idea: ~pt ! s~ is a probability distribution over ~ctrls s~ skewed towards the better controls, together with the number of times it has been updated so far

{-
adjust_v (v_avg, v, n) = (n * v_avg + v) / (n+1) -- just for illustration purposes
adjust_p (c, pcs, v_avg, v, n) = [(c', p + sign c' c * delta) | (c, p) <- pcs]
  where
  delta = if n == 0 then 0 else (v_avg - v) / ((n+1) * v)
  sign c' c = if c' == c then signum (-delta) else signum delta

improve_approx (vt, pt) s0 = val_pol_changes
  where
  (pcs, n) = pt ! s0
  c = pick pcs -- unsafe "function"!
  s = next s0 c
  r = reward s0 c s
  v = vt ! s <+> r
  new_v = adjust_v (vt ! s0, v, n)  -- adjust ~s0~ value
  new_p = adjust_p (c, ps, vt ! s0, v, n) -- adjust probability distribution
  vt_pt_changes = ([(s0, vt_change)], [s0, pt_change])

-}
