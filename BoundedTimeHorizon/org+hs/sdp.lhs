Santa's little helpers:

> pair (f, g) x    =  (f x, g x)
> cross (f, g)     =  pair (f . fst, g . snd)

> update f (x, v)  =  \ x' -> if x == x' then v else f x'

> maxBy le [x]     =  x
> maxBy le (x1 : x2 : xs)  =  if le (x1, x2) then maxBy le (x1 : xs) else maxBy le (x2: xs)

SDP as a record type:

> data SDP states ctrls val m_next m_reward m_vt m_pol = SDP {
>   ctrls   ::  states   -> [ctrls],
>   next    ::  states   -> ctrls -> m_next states,
>   reward  ::  states   -> ctrls -> states -> m_reward val,
>   vt      ::  states   -> m_vt val,
>   pol     ::  states   -> m_pol ctrls,
>   plus    ::  m_vt val -> m_next (m_reward val) -> m_vt val,
>   le      ::  (m_vt val, m_vt val) -> Bool
>   }


> improve :: Functor m_next => SDP states ctrls val m_next m_reward m_vt m_pol -> states -> (states -> m_pol ctrls, states -> m_vt val)
> improve (SDP ctrls next reward vt pol plus le) s0 = (pol', vt')
>   where
>   cs             =  ctrls s0      -- control candidates                     --      :: [ctrls]
>   mss            =  map (next s0) cs  -- possible uncertain next states     --      :: [m_next states]
>   ps             =  map (\ (ms, c) -> fmap (reward s0 c) ms) (zip mss cs)   --       :: [m_next (m_reward val)]
>   vts            =  map (plus (vt s0)) ps
>   (bestv, bestc) =  maxBy (le . cross(fst, fst)) vts
>   (pol', vt')    =  if le (bestv, vt s0) then (update pol (s0, bestc), update vt (s0, bestv)) else (pol, vt)

